package com.example.pedram.myapplication.models_get;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pedram on 4/21/2018.
 */

public class Product {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("team_id")
    @Expose
    private Integer teamId;
    @SerializedName("team2_id")
    @Expose
    private Integer team2Id;
    @SerializedName("M1")
    @Expose
    private Integer m1;
    @SerializedName("M2")
    @Expose
    private Integer m2;
    @SerializedName("M3")
    @Expose
    private Integer m3;
    @SerializedName("M4")
    @Expose
    private Integer m4;
    @SerializedName("M5")
    @Expose
    private Integer m5;
    @SerializedName("M6")
    @Expose
    private Integer m6;
    @SerializedName("factor_M1")
    @Expose
    private Integer factorM1;
    @SerializedName("factor_M2")
    @Expose
    private Integer factorM2;
    @SerializedName("factor_M3")
    @Expose
    private Integer factorM3;
    @SerializedName("factor_M4")
    @Expose
    private Integer factorM4;
    @SerializedName("factor_M5")
    @Expose
    private Integer factorM5;
    @SerializedName("factor_M6")
    @Expose
    private Integer factorM6;
    @SerializedName("virtual_sum_M1")
    @Expose
    private Integer virtualSumM1;
    @SerializedName("virtual_sum_M2")
    @Expose
    private Integer virtualSumM2;
    @SerializedName("virtual_sum_M3")
    @Expose
    private Integer virtualSumM3;
    @SerializedName("virtual_sum_M4")
    @Expose
    private Integer virtualSumM4;
    @SerializedName("virtual_sum_M5")
    @Expose
    private Integer virtualSumM5;
    @SerializedName("virtual_sum_M6")
    @Expose
    private Integer virtualSumM6;
    @SerializedName("winner")
    @Expose
    private String winner;
    @SerializedName("cup")
    @Expose
    private String cup;
    @SerializedName("group")
    @Expose
    private String group;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public Integer getTeam2Id() {
        return team2Id;
    }

    public void setTeam2Id(Integer team2Id) {
        this.team2Id = team2Id;
    }

    public Integer getM1() {
        return m1;
    }

    public void setM1(Integer m1) {
        this.m1 = m1;
    }

    public Integer getM2() {
        return m2;
    }

    public void setM2(Integer m2) {
        this.m2 = m2;
    }

    public Integer getM3() {
        return m3;
    }

    public void setM3(Integer m3) {
        this.m3 = m3;
    }

    public Integer getM4() {
        return m4;
    }

    public void setM4(Integer m4) {
        this.m4 = m4;
    }

    public Integer getM5() {
        return m5;
    }

    public void setM5(Integer m5) {
        this.m5 = m5;
    }

    public Integer getM6() {
        return m6;
    }

    public void setM6(Integer m6) {
        this.m6 = m6;
    }

    public Integer getFactorM1() {
        return factorM1;
    }

    public void setFactorM1(Integer factorM1) {
        this.factorM1 = factorM1;
    }

    public Integer getFactorM2() {
        return factorM2;
    }

    public void setFactorM2(Integer factorM2) {
        this.factorM2 = factorM2;
    }

    public Integer getFactorM3() {
        return factorM3;
    }

    public void setFactorM3(Integer factorM3) {
        this.factorM3 = factorM3;
    }

    public Integer getFactorM4() {
        return factorM4;
    }

    public void setFactorM4(Integer factorM4) {
        this.factorM4 = factorM4;
    }

    public Integer getFactorM5() {
        return factorM5;
    }

    public void setFactorM5(Integer factorM5) {
        this.factorM5 = factorM5;
    }

    public Integer getFactorM6() {
        return factorM6;
    }

    public void setFactorM6(Integer factorM6) {
        this.factorM6 = factorM6;
    }

    public Integer getVirtualSumM1() {
        return virtualSumM1;
    }

    public void setVirtualSumM1(Integer virtualSumM1) {
        this.virtualSumM1 = virtualSumM1;
    }

    public Integer getVirtualSumM2() {
        return virtualSumM2;
    }

    public void setVirtualSumM2(Integer virtualSumM2) {
        this.virtualSumM2 = virtualSumM2;
    }

    public Integer getVirtualSumM3() {
        return virtualSumM3;
    }

    public void setVirtualSumM3(Integer virtualSumM3) {
        this.virtualSumM3 = virtualSumM3;
    }

    public Integer getVirtualSumM4() {
        return virtualSumM4;
    }

    public void setVirtualSumM4(Integer virtualSumM4) {
        this.virtualSumM4 = virtualSumM4;
    }

    public Integer getVirtualSumM5() {
        return virtualSumM5;
    }

    public void setVirtualSumM5(Integer virtualSumM5) {
        this.virtualSumM5 = virtualSumM5;
    }

    public Integer getVirtualSumM6() {
        return virtualSumM6;
    }

    public void setVirtualSumM6(Integer virtualSumM6) {
        this.virtualSumM6 = virtualSumM6;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getCup() {
        return cup;
    }

    public void setCup(String cup) {
        this.cup = cup;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
