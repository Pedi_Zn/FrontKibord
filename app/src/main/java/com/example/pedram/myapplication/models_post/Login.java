package com.example.pedram.myapplication.models_post;

/**
 * Created by pedram on 3/25/2018.
 */

public class Login {

    private String mobile_number;
    private String password;

    public String getMobilenumber() {
        return mobile_number;
    }

    public void setMobilenumber(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
