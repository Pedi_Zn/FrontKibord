package com.example.pedram.myapplication.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pedram.myapplication.Fragments.PrivatePanel;
import com.example.pedram.myapplication.Panels.CupPanel;
import com.example.pedram.myapplication.Panels.GozareshPanel;
import com.example.pedram.myapplication.Panels.LoginPanel;
import com.example.pedram.myapplication.Panels.MatchPanel;
import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.Panels.UserPanel;
import com.example.pedram.myapplication.Panels.UserPanel2;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.Cups;
import com.example.pedram.myapplication.models_get.DbUser;
import com.example.pedram.myapplication.models_get.FpUser;
import com.example.pedram.myapplication.models_get.Guide;
import com.example.pedram.myapplication.models_get.JSONResponse;
import com.example.pedram.myapplication.models_get.JSONResponse2;
import com.example.pedram.myapplication.models_get.JSONResponse3;
import com.example.pedram.myapplication.models_get.JSONResponse4;
import com.example.pedram.myapplication.models_get.Matches;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_get.RgUser;
import com.example.pedram.myapplication.models_get.UserMatch;
import com.example.pedram.myapplication.models_get.teamList;
import com.example.pedram.myapplication.models_post.ForgotPassword;
import com.example.pedram.myapplication.models_post.Login;
import com.example.pedram.myapplication.models_post.NewPassword;
import com.example.pedram.myapplication.models_post.OptionAnswer;
import com.example.pedram.myapplication.models_post.Options;
import com.example.pedram.myapplication.models_post.OptionsValue;
import com.example.pedram.myapplication.models_post.Register;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Path;

public class DataAdapter3 extends RecyclerView.Adapter<DataAdapter3.ViewHolder> {
    private Context context;
    private ArrayList<Cups> android;
    public static int CUPID;
    public static int team1_ID, team2_ID;
    public static String team1_name, team2_name;

    public DataAdapter3(Context context, ArrayList<Cups> android) {
        this.android = android;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_row, viewGroup, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        final Api request = retrofit.create(Api.class);
        final LoginPanel loginPanel = new LoginPanel();

        Call<JSONResponse3> call = request.getCups(loginPanel.getAccess());

        call.enqueue(new Callback<JSONResponse3>() {
            @Override
            public void onResponse(Call<JSONResponse3> call, Response<JSONResponse3> response) {
                if (response.isSuccessful()) {
                    viewHolder.mid.setVisibility(View.VISIBLE);
                    viewHolder.tv_api_level.setVisibility(View.GONE);
                    viewHolder.tv_name.setVisibility(View.GONE);
                    viewHolder.tv_version.setVisibility(View.GONE);
                    viewHolder.mid.setText(android.get(i).getName());
                }
                else
                    Toast.makeText(context, "خطا در ارسال اطلاعات", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<JSONResponse3> call, Throwable t) {
                t.printStackTrace();
                Log.d("tag", "Ped, message is : " + t.getMessage());
                Log.d("tag", "Ped, call is : " + call.toString());
                Toast.makeText(context, "خطا در اتصال به سرور", Toast.LENGTH_SHORT).show();
            }
        });

        Call<ResponseBody> call2 = request.getCupImage(android.get(i).getId(), loginPanel.getAccess());

        call2.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    boolean FileDownloaded = DownloadImage(response.body());
                }
            }

            private boolean DownloadImage(ResponseBody body) {
                try {
                    Log.d("DownloadImage", "Reading and writing file");
                    InputStream in = null;
                    FileOutputStream out = null;

                    try {
                        in = body.byteStream();
                        out = new FileOutputStream(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                        int c;

                        while ((c = in.read()) != -1) {
                            out.write(c);
                        }
                    }
                    catch (IOException e) {
                        Log.d("DownloadImage",e.toString());
                        return false;
                    }
                    finally {
                        if (in != null) {
                            in.close();
                        }
                        if (out != null) {
                            out.close();
                        }
                    }

                    int width, height;

                    Bitmap bMap = BitmapFactory.decodeFile(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                    width = 2*bMap.getWidth();
                    height = 3*bMap.getHeight();
                    Bitmap bMap2 = Bitmap.createScaledBitmap(bMap, width, height, false);
                    viewHolder.iv_team1.setImageBitmap(bMap2);
                    viewHolder.iv_team2.setImageBitmap(bMap2);

                    return true;

                } catch (IOException e) {
                    Log.d("DownloadImage",e.toString());
                    return false;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                Log.d("tag", "Ped, message is : " + t.getMessage());
                Log.d("tag", "Ped, call is : " + call.toString());
                Toast.makeText(context, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
            }
        });

        viewHolder.setItemClickListener(new Api() {
            @Override
            public Call<DbUser> tokenAccess(@Body Login login) {
                return null;
            }

            @Override
            public Call<RgUser> registerUsers(@Body Register register) {
                return null;
            }

            @Override
            public Call<FpUser> forgotUsers(@Body ForgotPassword forgotPassword) {
                return null;
            }

            @Override
            public Call<NwPass> newPassword(@Body NewPassword newPassword, @Path("id") Integer id) {
                return null;
            }

            @Override
            public Call<RgUser> getUser(@Path("id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<JSONResponse> getJSON(@Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<JSONResponse3> getCups(@Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<JSONResponse4> getBills(@Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<Guide> getGuides() {
                return null;
            }

            @Override
            public Call<teamList> getTeam(@Path("id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<Matches> getMatch(@Path("id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> setOption(@Body Options options, @Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> sendCash(@Body Options options, @Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> addValue1(@Body Options options, @Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> addValue(@Body Options options, @Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> setValue(@Body OptionsValue optionsValue, @Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> setAnswer(@Body OptionAnswer optionAnswer, @Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<UserMatch> getOption(@Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<ResponseBody> getCupImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<ResponseBody> getTeamImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<ResponseBody> getLeftTeamImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<ResponseBody> getRightTeamImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<JSONResponse2> getPreMatches(@Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> getSuccess() {
                return null;
            }

            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if (isLongClick) {
//                    Toast.makeText(context, "#" + android.get(i).getId() + " - "  + " (Long click)", Toast.LENGTH_SHORT).show();
                    CUPID = android.get(i).getId();
//                    Toast.makeText(context, android.get(i).getId().toString(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, UserPanel2.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {
                    CUPID = android.get(i).getId();
//                    Toast.makeText(context, android.get(i).getId().toString(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, UserPanel2.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
//                    Toast.makeText(context, "#" + android.get(i).getId() + " - " , Toast.LENGTH_SHORT).show();
                }
            }

        });



    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView tv_name, tv_version, mid;
        private EditText tv_api_level;
        private ImageView iv_team1, iv_team2;
        private Api requestInterface;
        private RelativeLayout relativeLayout;

        public ViewHolder(View view) {
            super(view);

            tv_name = (TextView)view.findViewById(R.id.tv_name);
            tv_version = (TextView)view.findViewById(R.id.tv_version);
            tv_api_level = (EditText) view.findViewById(R.id.tv_api_level);
            mid = (TextView) view.findViewById(R.id.mid);

            iv_team2 = (ImageView)view.findViewById(R.id.IV_team1);
            iv_team1 = (ImageView)view.findViewById(R.id.IV_team2);

            view.setTag(view);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);

        }

        public void setItemClickListener (Api requestInterface) {
            this.requestInterface = requestInterface;
        }

        @Override
        public void onClick(View view) {
            requestInterface.onClick(view, getPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            requestInterface.onClick(view, getPosition(), true);

            return true;
        }

    }

}