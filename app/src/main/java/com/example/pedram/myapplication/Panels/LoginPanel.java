package com.example.pedram.myapplication.Panels;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.droidbyme.dialoglib.AnimUtils;
import com.droidbyme.dialoglib.DroidDialog;
import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.Guide;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_post.ForgotPassword;
import com.example.pedram.myapplication.models_get.FpUser;
import com.example.pedram.myapplication.models_get.DbUser;
import com.example.pedram.myapplication.models_post.Login;
import com.example.pedram.myapplication.models_post.NewPassword;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import dmax.dialog.SpotsDialog;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginPanel extends AppCompatActivity {

    Typeface iransans_font;
    Dialog myDialog;
    TextView forgotpass, createaccount, app_name, version;
    EditText phonenumber, password;
    Button login;
    ImageView telegram, calllog;
    SpotsDialog dialog;
    public static int id;
    private Api api;
    public static String access_token;


    @Override
    public void onBackPressed() {
        finish();
        System.exit(0);
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_panel);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        //progressBar.getProgressDrawable().setColorFilter(getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_IN);

//        Toast.makeText(LoginPanel.this, StartupPanel.IP, Toast.LENGTH_LONG).show();

        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        api = retrofit.create(Api.class);

        forgotpass = (TextView) findViewById(R.id.TV_forgotpassword);
        createaccount = (TextView) findViewById(R.id.TV_createaccount);
        app_name = (TextView) findViewById(R.id.TV_appname);
        version = (TextView) findViewById(R.id.version);

        phonenumber = (EditText) findViewById(R.id.ET_phonenumber);
        password = (EditText) findViewById(R.id.ET_password);

        login = (Button) findViewById(R.id.BTN_login);

//        telegram = (ImageView) findViewById(R.id.IV_telegram);
//         calllog = (ImageView) findViewById(R.id.IV_calllog);

        iransans_font = Typeface.createFromAsset(getAssets(), "iransans.ttf");

        forgotpass.setTypeface(iransans_font);
        createaccount.setTypeface(iransans_font);
        phonenumber.setTypeface(iransans_font);
        password.setTypeface(iransans_font);
        login.setTypeface(iransans_font);

        myDialog = new Dialog(this);

        //getUser();


        Call<Guide> call = api.getGuides();
        call.enqueue(new Callback<Guide>() {
            @Override
            public void onResponse(Call<Guide> call, final Response<Guide> response) {
                if (response.isSuccessful()) {

                    if (!(response.body().getVersion().equals("V 2.0"))) {
                        new DroidDialog.Builder(LoginPanel.this)
                                .icon(R.drawable.ic_stat_name)
                                .title("بروز رسانی                                                                                                            ")
                                .content("هم اکنون آپدیت برنامه موجود می باشد برای دریافت نسخه " + response.body().getVersion() + " به کانال تلگرام ما مراجعه کنید                                    ")
                                .cancelable(true, true)
                                .positiveButton("تلگرام", new DroidDialog.onPositiveListener() {
                                    @Override
                                    public void onPositive(Dialog droidDialog) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/joinchat/I2myUUaebUh9Wc2gfdBr4w"));
                                        startActivity(intent);
                                        droidDialog.dismiss();
                                    }
                                })
                                .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                                    @Override
                                    public void onNeutral(Dialog droidDialog) {
                                        droidDialog.dismiss();
                                    }
                                })
                                .animation(AnimUtils.AnimFadeInOut)
                                .color(ContextCompat.getColor(LoginPanel.this, R.color.blue), ContextCompat.getColor(LoginPanel.this, R.color.white),
                                        ContextCompat.getColor(LoginPanel.this, R.color.blue))
                                .divider(true, ContextCompat.getColor(LoginPanel.this, R.color.white))
                                .show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Guide> call, Throwable t) {
                t.printStackTrace();
                Log.d("tag", "Ped, message is : " + t.getMessage());
                Log.d("tag", "Ped, call is : " + call.toString());
                Toast.makeText(LoginPanel.this, "خطا در اتصال به سروررررر", Toast.LENGTH_LONG).show();
            }
        });




        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /**/
                String login_phonenumber = phonenumber.getText().toString();

                if (login_phonenumber.equals("") || !(trueNumber(login_phonenumber, 11))) {
                    Toast.makeText(getApplicationContext(), "شماره موبایل نامعتبر می باشد", Toast.LENGTH_LONG).show();
                }
                else {
                    if (password.getText().toString().equals(""))
                        Toast.makeText(getApplicationContext(), "رمز عبور نامعتبر می باشد", Toast.LENGTH_LONG).show();
                    else {
                        if (!isConnected(LoginPanel.this)) {
                            buildDialog(LoginPanel.this).show();
                        } else {

                            //progressBar.setVisibility(View.GONE);

                            Login login = new Login();
                            login.setMobilenumber(phonenumber.getText().toString());
                            login.setPassword(password.getText().toString());

                            Call<DbUser> call = api.tokenAccess(login);

                            call.enqueue(new Callback<DbUser>() {
                                @Override
                                public void onResponse(Call<DbUser> call, final Response<DbUser> response) {
                                    if (response.isSuccessful()) {

                                        new CountDownTimer(4000, 4000) {

                                            @Override
                                            public void onTick(long l) {
                                                dialog = new SpotsDialog(LoginPanel.this, R.style.Custom);
                                                dialog.show();
                                            }

                                            @Override
                                            public void onFinish() {
//                                                dialog.dismiss();
                                                id = response.body().getId();
                                                setId(id);
                                                access_token = response.body().getAccessToken();
                                                setAccess(access_token);
                                                Toast.makeText(LoginPanel.this, "خوش آمدید", Toast.LENGTH_LONG).show();
                                                finish();
                                                Intent intent = new Intent(LoginPanel.this, UserPanel.class);
                                                startActivity(intent);
                                            }
                                        }.start();


                                    } else
                                        Toast.makeText(LoginPanel.this, getString(R.string.Login_Error), Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onFailure(Call<DbUser> call, Throwable t) {
                                    t.printStackTrace();
                                    Log.d("tag", "Ped, message is : " + t.getMessage());
                                    Log.d("tag", "Ped, call is : " + call.toString());
                                    Toast.makeText(LoginPanel.this, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                }
            }
        });

        forgotpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //progressBar.setVisibility(View.VISIBLE);
                String login_phonenumber = phonenumber.getText().toString();

                if (login_phonenumber.equals("") || !(trueNumber(login_phonenumber, 11))) {
                    Toast.makeText(getApplicationContext(), "شماره موبایل نامعتبر می باشد", Toast.LENGTH_LONG).show();
                }
                else {
                    if(!isConnected(LoginPanel.this)) {
                        buildDialog(LoginPanel.this).show();
                    }
                    else {
                        ForgotPassword forgotPassword = new ForgotPassword();
                        forgotPassword.setMobilenumber(phonenumber.getText().toString());

                        Call<FpUser> call = api.forgotUsers(forgotPassword);

                        call.enqueue(new Callback<FpUser>() {
                            @Override
                            public void onResponse(Call<FpUser> call, final Response<FpUser> response) {
                                if (response.isSuccessful()) {
                                    final TextView txtclose, question;
                                    final Button sabt;
                                    final String answerr;

                                    myDialog.setContentView(R.layout.popup);
                                    question = myDialog.findViewById(R.id.TV_question);
                                    txtclose = myDialog.findViewById(R.id.textclose);

                                    sabt = myDialog.findViewById(R.id.BTN_sabt);
                                    myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    myDialog.show();

                                    txtclose.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            myDialog.dismiss();
                                        }
                                    });

                                    question.setText(response.body().getPrivateQuestion());
                                    answerr = response.body().getPrivateAnswer();

                                    sabt.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            final EditText answer;
                                            answer =  myDialog.findViewById(R.id.ET_answer);
                                            if (question.getText().equals(""))
                                                Toast.makeText(LoginPanel.this, "سوال خصوصی تعریف نشده", Toast.LENGTH_LONG).show();
                                            else
                                                if (answer.getText().toString().equals(""))
                                                    Toast.makeText(LoginPanel.this, "لطفا پاسخ خود را وارد کنید", Toast.LENGTH_LONG).show();
                                                else
                                                    if (answerr.equals(answer.getText().toString())) {
                                                        myDialog.dismiss();
                                                        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                                        myDialog.show();

                                                        question.setText("رمزعبور جدید خود را وارد کنید");
                                                        answer.setText("");

                                                        sabt.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {

                                                                if (answer.getText().toString().equals("")) {
                                                                    Toast.makeText(getApplicationContext(), "لطفا رمز عبور خود را وارد کنید", Toast.LENGTH_LONG).show();
                                                                }
                                                                else {
                                                                    if (!(truePass(answer.getText().toString()))) {
                                                                        Toast.makeText(getApplicationContext(), "رمز عبور باید شامل حداقل 8 کاراکتر، یک رقم و یک حرف بزرگ باشد", Toast.LENGTH_LONG).show();
                                                                    }
                                                                    else {
                                                                        NewPassword newPassword = new NewPassword();
                                                                        newPassword.setPassword(answer.getText().toString());

                                                                        Call<NwPass> call = api.newPassword(newPassword, response.body().getId());

                                                                        call.enqueue(new Callback<NwPass>() {
                                                                            @Override
                                                                            public void onResponse(Call<NwPass> call, Response<NwPass> response) {
                                                                                if (response.isSuccessful()) {
                                                                                    Toast.makeText(LoginPanel.this, "تغییرات با موفقیت انجام شد", Toast.LENGTH_LONG).show();
                                                                                    myDialog.dismiss();

                                                                                } else
                                                                                    Toast.makeText(LoginPanel.this, "خطا در ارسال اطلاعات", Toast.LENGTH_LONG).show();
                                                                            }

                                                                            @Override
                                                                            public void onFailure(Call<NwPass> call, Throwable t) {
                                                                                t.printStackTrace();
                                                                                Log.d("tag", "Ped, message is : " + t.getMessage());
                                                                                Log.d("tag", "Ped, call is : " + call.toString());
                                                                                Toast.makeText(LoginPanel.this, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                        });

                                                }
                                                else
                                                    Toast.makeText(LoginPanel.this, "پاسخ نادرست می باشد", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                else
                                    Toast.makeText(LoginPanel.this, "خطا در ارسال اطلاعات", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onFailure(Call<FpUser> call, Throwable t) {
                                t.printStackTrace();
                                Log.d("tag", "Ped, message is : " + t.getMessage());
                                Log.d("tag", "Ped, call is : " + call.toString());
                                Toast.makeText(LoginPanel.this, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
            }
        });

        createaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginPanel.this, RegisterPanel.class);
                startActivity(intent);
            }
        });

        // Telegram Icon
        /*telegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/Cashaf1Kard"));
                startActivity(intent);
            }
        });*/

        //Call Log Icon
        /*calllog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:+989361778920"));
                startActivity(myIntent);
            }
        });*/

    }

    public Integer uppercaseCounter (String s) {
        int counter =0;
        for (int i=0 ; i<s.length() ; i++)
        {
            if (Character.isUpperCase(s.charAt(i))) {
                counter++;
            }
        }
        return counter;
    }

    public boolean truePass (String s) {
        if (digitsCounter(s) >= 1 && uppercaseCounter(s) >= 1 && lettersCounter(s) >= 7)
            return true;
        else
            return false;
    }

    public Integer digitsCounter (String s) {
        int counter =0;

        for (int i=0 ; i<s.length() ; i++)
        {
            if (Character.isDigit(s.charAt(i))) {
                counter++;
            }
        }
        return counter;
    }

    public boolean trueNumber (String phonenumber, int definenumber) {
        if (digitsCounter(phonenumber) == definenumber)
            return true;
        else
            return false;
    }

    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
            else return false;
        } else
            return false;
    }

    public Integer lettersCounter (String s) {
        int counter = 0;

        for (int i = 0; i < s.length(); i++) {
            if (Character.isLetter(s.charAt(i)))
                counter++;
        }
        return counter;
    }

    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or wifi to access this. Press ok to Exit");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
//                finish();
            }
        });

        return builder;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccess() {
        return access_token;
    }

    public void setAccess(String  access_token) {
        this.access_token = access_token;
    }

    /*private boolean DownloadImage(ResponseBody body) {

        try {
            Log.d("DownloadImage", "Reading and writing file");
            InputStream in = null;
            FileOutputStream out = null;

            try {
                in = body.byteStream();
                out = new FileOutputStream(getExternalFilesDir(null) + File.separator + "Android.jpg");
                int c;

                while ((c = in.read()) != -1) {
                    out.write(c);
                }
            }
            catch (IOException e) {
                Log.d("DownloadImage",e.toString());
                return false;
            }
            finally {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            }

            int width, height;

            ImageView image = (ImageView) findViewById(R.id.IV_image);
            Bitmap bMap = BitmapFactory.decodeFile(getExternalFilesDir(null) + File.separator + "Android.jpg");
            width = bMap.getWidth();
            height = bMap.getHeight();
            Bitmap bMap2 = Bitmap.createScaledBitmap(bMap, width, height, false);
            image.setImageBitmap(bMap2);

            return true;

        } catch (IOException e) {
            Log.d("DownloadImage",e.toString());
            return false;
        }
    }*/

}
