package com.example.pedram.myapplication.Fragments;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.Panels.LoginPanel;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_post.NewPassword;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PrivatePanel extends AppCompatActivity {

    Typeface iransans_font;
    ImageView back;
    private Api api;
    TextView change_creditcard;
    EditText answer,  question;
    Button sabt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_private_panel);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        api = retrofit.create(Api.class);

        question = (EditText) findViewById(R.id.ET_question);
        answer = (EditText) findViewById(R.id.ET_answer);
        sabt = (Button) findViewById(R.id.BTN_sabt);
        back = (ImageView) findViewById(R.id.IV_back);
        change_creditcard = (TextView) findViewById(R.id.TV_change_creditcard);

        iransans_font = Typeface.createFromAsset(getAssets(), "iransans.ttf");

        question.setTypeface(iransans_font);
        answer.setTypeface(iransans_font);
        sabt.setTypeface(iransans_font);
        change_creditcard.setTypeface(iransans_font);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        sabt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (question.getText().toString().equals(""))
                    Toast.makeText(PrivatePanel.this, "لطفا سوال خصوصی خود را وارد کنید", Toast.LENGTH_SHORT).show();
                else
                    if (answer.getText().toString().equals(""))
                        Toast.makeText(PrivatePanel.this, "لطفا پاسخ خصوصی خود را وارد کنید", Toast.LENGTH_SHORT).show();
                    else {
                        LoginPanel loginPanel = new LoginPanel();
                        NewPassword newPassword = new NewPassword();
                        newPassword.setFirstlogin("false");
                        newPassword.setPrivateAnswer(answer.getText().toString());
                        newPassword.setPrivateQuestion(question.getText().toString());

                        Call<NwPass> call = api.newPassword(newPassword, loginPanel.id);

                        call.enqueue(new Callback<NwPass>() {
                            @Override
                            public void onResponse(Call<NwPass> call, Response<NwPass> response) {
                                if (response.isSuccessful()) {
                                    Toast.makeText(PrivatePanel.this, "عملیات با موفقیت انجام شد", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else
                                    Toast.makeText(PrivatePanel.this, "خطا در ارسال اطلاعات", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Call<NwPass> call, Throwable t) {
                                t.printStackTrace();
                                Log.d("tag", "Ped, message is : " + t.getMessage());
                                Log.d("tag", "Ped, call is : " + call.toString());
                                Toast.makeText(PrivatePanel.this, "خطا در اتصال به سرور", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
            }
        });
    }
}
