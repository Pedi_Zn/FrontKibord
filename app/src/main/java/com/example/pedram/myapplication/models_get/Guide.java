package com.example.pedram.myapplication.models_get;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pedram on 5/20/2018.
 */

public class Guide {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("readme")
    @Expose
    private String readme;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("min_bardasht")
    @Expose
    private Integer minBardasht;
    @SerializedName("min_mujudi")
    @Expose
    private Integer minMujudi;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReadme() {
        return readme;
    }

    public void setReadme(String readme) {
        this.readme = readme;
    }

    public Integer getMinBardasht() {
        return minBardasht;
    }

    public void setMinBardasht(Integer minBardasht) {
        this.minBardasht = minBardasht;
    }

    public Integer getMinMujudi() {
        return minMujudi;
    }

    public void setMinMujudi(Integer minMujudi) {
        this.minMujudi = minMujudi;
    }

}
