package com.example.pedram.myapplication.models_post;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pedram on 3/22/2018.
 */

public class User {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("surname")
    @Expose
    private String surname;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("avatar_path")
    @Expose
    private String avatarPath;
    @SerializedName("virtual_account")
    @Expose
    private Integer virtualAccount;
    @SerializedName("removable_account")
    @Expose
    private Integer removableAccount;
    @SerializedName("card_number")
    @Expose
    private String cardNumber;
    @SerializedName("private_question")
    @Expose
    private String privateQuestion;
    @SerializedName("private_answer")
    @Expose
    private String privateAnswer;
    @SerializedName("position")
    @Expose
    private Integer position;
    @SerializedName("first_time_login")
    @Expose
    private Boolean firstTimeLogin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public Integer getVirtualAccount() {
        return virtualAccount;
    }

    public void setVirtualAccount(Integer virtualAccount) {
        this.virtualAccount = virtualAccount;
    }

    public Integer getRemovableAccount() {
        return removableAccount;
    }

    public void setRemovableAccount(Integer removableAccount) {
        this.removableAccount = removableAccount;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getPrivateQuestion() {
        return privateQuestion;
    }

    public void setPrivateQuestion(String privateQuestion) {
        this.privateQuestion = privateQuestion;
    }

    public String getPrivateAnswer() {
        return privateAnswer;
    }

    public void setPrivateAnswer(String privateAnswer) {
        this.privateAnswer = privateAnswer;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Boolean getFirstTimeLogin() {
        return firstTimeLogin;
    }

    public void setFirstTimeLogin(Boolean firstTimeLogin) {
        this.firstTimeLogin = firstTimeLogin;
    }
}
