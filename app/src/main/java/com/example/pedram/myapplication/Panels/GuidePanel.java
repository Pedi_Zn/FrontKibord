package com.example.pedram.myapplication.Panels;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pedram.myapplication.Fragments.Change_pass;
import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.Guide;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_post.NewPassword;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GuidePanel extends AppCompatActivity {

    private Api api;
    TextView readme;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guide_panel);

        readme = (TextView) findViewById(R.id.readme);
        back = (ImageView) findViewById(R.id.IV_back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(GuidePanel.this, UserPanel.class);
                startActivity(intent);
            }
        });


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        api = retrofit.create(Api.class);

        LoginPanel loginPanel = new LoginPanel();
        Call<Guide> callcoin = api.getGuides();

        callcoin.enqueue(new Callback<Guide>() {
            @Override
            public void onResponse(Call<Guide> callcoin, Response<Guide> response) {
                if (response.isSuccessful()) {
                    readme.setText(response.body().getReadme());
                }
                else
                    Toast.makeText(GuidePanel.this, "خطا در ارسال اطلاعات", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Guide> callcoin, Throwable t) {
                t.printStackTrace();
                Log.d("tag", "Ped, message is : " + t.getMessage());
                Log.d("tag", "Ped, call is : " + callcoin.toString());
                Toast.makeText(GuidePanel.this, "خطا در اتصال به سرور", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(GuidePanel.this, UserPanel.class);
        startActivity(intent);
    }
}
