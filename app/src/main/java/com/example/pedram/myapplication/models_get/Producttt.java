package com.example.pedram.myapplication.models_get;

/**
 * Created by pedram on 4/20/2018.
 */

public class Producttt {
    private int id;
    private String t1_name;
    private String t2_name;

    public Producttt(int id, String t1_name, String t2_name) {
        this.id = id;
        this.t1_name = t1_name;
        this.t2_name = t2_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getT1_name() {
        return t1_name;
    }

    public void setT1_name(String t1_name) {
        this.t1_name = t1_name;
    }

    public String getT2_name() {
        return t2_name;
    }

    public void setT2_name(String t2_name) {
        this.t2_name = t2_name;
    }
}
