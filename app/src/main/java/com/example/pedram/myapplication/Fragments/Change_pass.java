package com.example.pedram.myapplication.Fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.Panels.LoginPanel;
import com.example.pedram.myapplication.Panels.PayPanel;
import com.example.pedram.myapplication.Panels.UserPanel;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_post.NewPassword;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Change_pass extends AppCompatActivity {

    ImageView back;
    TextView change_pass;
    EditText last_pass, new_pass, new_repass;
    Button profile_changes;
    Typeface iransans_font;
    private Api api;

    LoginPanel loginPanel = new LoginPanel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_pass);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        api = retrofit.create(Api.class);

        back = (ImageView) findViewById(R.id.IV_back);
        change_pass = (TextView) findViewById(R.id.TV_changePass);
        new_pass = (EditText) findViewById(R.id.ET_new_password);
        new_repass = (EditText) findViewById(R.id.ET_re_password);
        profile_changes = (Button) findViewById(R.id.BTN_Profile_Changes);

        iransans_font = Typeface.createFromAsset(getAssets(), "iransans.ttf");

        change_pass.setTypeface(iransans_font);
        new_pass.setTypeface(iransans_font);
        new_repass.setTypeface(iransans_font);
        profile_changes.setTypeface(iransans_font);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(Change_pass.this, UserPanel.class);
                startActivity(intent);
            }
        });

        profile_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*if (last_pass.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "لطفا کلمه عبور قبلی خود را وارد کنید", Toast.LENGTH_SHORT).show();
                    last_pass.setBackgroundResource(R.drawable.edittext_shape);

//                    ************************************
                } else {
                    last_pass.setBackgroundResource(R.drawable.btn_radius2);*/
                    if (new_pass.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), "لطفا رمز عبور خود را وارد کنید", Toast.LENGTH_SHORT).show();
                        new_pass.setBackgroundResource(R.drawable.edittext_shape);
                    } else {
                        new_pass.setBackgroundResource(R.drawable.btn_radius2);
                        if (!(truePass(new_pass.getText().toString()))) {
                            Toast.makeText(getApplicationContext(), "رمز عبور باید شامل حداقل یک رقم و یک حرف بزرگ باشدرمز عبور باید شامل حداقل 8 کاراکتر، یک رقم و یک حرف بزرگ باشد", Toast.LENGTH_SHORT).show();
                            new_pass.setBackgroundResource(R.drawable.edittext_shape);
                        } else {
                            new_pass.setBackgroundResource(R.drawable.btn_radius2);
                            if (new_repass.getText().toString().equals("")) {
                                Toast.makeText(getApplicationContext(), "لطفا تکرار رمز عبور خود را وارد کنید", Toast.LENGTH_SHORT).show();
                                new_repass.setBackgroundResource(R.drawable.edittext_shape);

                            } else {
                                new_repass.setBackgroundResource(R.drawable.btn_radius2);

                                if (!new_pass.getText().toString().equals(new_repass.getText().toString())) {
                                    Toast.makeText(getApplicationContext(), "کلمه عبور با تکرار آن همخوانی ندارد", Toast.LENGTH_SHORT).show();
                                    new_repass.setBackgroundResource(R.drawable.edittext_shape);
                                } else {
                                    new_repass.setBackgroundResource(R.drawable.btn_radius2);


                                    NewPassword newPassword = new NewPassword();
                                    newPassword.setPassword(new_pass.getText().toString());

                                    Call<NwPass> call = api.newPassword(newPassword, loginPanel.id);

                                    call.enqueue(new Callback<NwPass>() {
                                        @Override
                                        public void onResponse(Call<NwPass> call, Response<NwPass> response) {
                                            if (response.isSuccessful()) {
                                                Toast.makeText(Change_pass.this, "تغییر رمزعبور با موفقیت انجام شد", Toast.LENGTH_SHORT).show();
                                                finish();
                                            } else
                                                Toast.makeText(Change_pass.this, "مشکل در ارسال اطلاعات", Toast.LENGTH_SHORT).show();
                                        }

                                        @Override
                                        public void onFailure(Call<NwPass> call, Throwable t) {
                                            t.printStackTrace();
                                            Log.d("tag", "Ped, message is : " + t.getMessage());
                                            Log.d("tag", "Ped, call is : " + call.toString());
                                            Toast.makeText(Change_pass.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        }
                    }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(Change_pass.this, UserPanel.class);
        startActivity(intent);
    }

    public Integer digitsCounter (String s) {
        int counter =0;
        for (int i=0 ; i<s.length() ; i++)
        {
            if (Character.isDigit(s.charAt(i))) {
                counter++;
            }
        }
        return counter;
    }

    public Integer uppercaseCounter (String s) {
        int counter =0;
        for (int i=0 ; i<s.length() ; i++)
        {
            if (Character.isUpperCase(s.charAt(i))) {
                counter++;
            }
        }
        return counter;
    }

    public boolean truePass (String s) {
        if (digitsCounter(s) >= 1 && uppercaseCounter(s) >= 1 && lettersCounter(s) >= 7)
            return true;
        else
            return false;
    }

    public Integer lettersCounter (String s) {
        int counter = 0;

        for (int i = 0; i < s.length(); i++) {
            if (Character.isLetter(s.charAt(i)))
                counter++;
        }
        return counter;
    }

}
