package com.example.pedram.myapplication.models_post;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pedram on 5/8/2018.
 */

public class OptionsValue {
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("AX")
    @Expose
    private String AX;

    public String getAX() {
        return AX;
    }

    public void setAX(String AX) {
        this.AX = AX;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
