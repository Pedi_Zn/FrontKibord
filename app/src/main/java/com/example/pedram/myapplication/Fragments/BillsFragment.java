package com.example.pedram.myapplication.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pedram.myapplication.Adapter.DataAdapter4;
import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.Panels.LoginPanel;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.Bills;
import com.example.pedram.myapplication.models_get.JSONResponse2;
import com.example.pedram.myapplication.models_get.JSONResponse4;
import com.example.pedram.myapplication.models_get.UserMatch;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pedram on 2/7/2018.
 */

public class BillsFragment extends Fragment {

    private RecyclerView recyclerView;
    private ArrayList<Bills> data;
    private DataAdapter4 adapter;
    private StaggeredGridLayoutManager mGridLayoutManager;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bills, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view2);

        initViews();

        return view;
    }
    private void initViews(){
        recyclerView.setHasFixedSize(true);
        mGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        loadJSON();
    }
    private void loadJSON(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL) //https://api.myjson.com/bins/
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api request = retrofit.create(Api.class);
        LoginPanel loginPanel = new LoginPanel();

        Call<JSONResponse4> call = request.getBills(loginPanel.getId(), loginPanel.getAccess());
        call.enqueue(new Callback<JSONResponse4>() {
            @Override
            public void onResponse(Call<JSONResponse4> call, Response<JSONResponse4> response) {

                JSONResponse4 jsonResponse4 = response.body();
                data = new ArrayList<>(Arrays.asList(jsonResponse4.getAndroid()));
                adapter = new DataAdapter4(getActivity().getApplicationContext(), data);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<JSONResponse4> call, Throwable t) {
                Log.d("Error",t.getMessage());
            }
        });
    }
}
