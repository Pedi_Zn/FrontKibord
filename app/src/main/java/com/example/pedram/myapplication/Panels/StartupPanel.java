package com.example.pedram.myapplication.Panels;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.DbUser;
import com.example.pedram.myapplication.models_get.teamList;

import dmax.dialog.SpotsDialog;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StartupPanel extends AppCompatActivity {

    private Api api;
    public static String IP;

    private static int SPLASH_TIME_OUT = 5500;
    private static int SPLASH_TIME_OUT2 = 750;
    ProgressBar progressBar;
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startup_panel);


        getWindow().setFormat(PixelFormat.UNKNOWN);
        final VideoView videoView = (VideoView) findViewById(R.id.videoView);
        final TextView textView13 = (TextView) findViewById(R.id.textView13);
        final TextView textView15 = (TextView) findViewById(R.id.textView15);
        final RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout3);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        progressBar.setVisibility(View.VISIBLE);

        videoView.setVideoPath("android.resource://"+getPackageName()+"/"+R.raw.clip);
//        String path = "android.resource://com.example.pedram.myapplication" + R.raw.clip;
//        Uri uri = Uri.parse(path);
//        videoView.setVideoURI(uri);
//        videoView.requestFocus();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                videoView.setBackgroundColor(Color.TRANSPARENT);
                videoView.start();
                /*textView13.setVisibility(View.VISIBLE);
                textView15.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);*/
                relativeLayout.setVisibility(View.VISIBLE);
            }
        },SPLASH_TIME_OUT2);
        videoView.setBackgroundColor(Color.WHITE);
        videoView.start();


        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://88.99.113.138")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        api = retrofit.create(Api.class);
        Call<teamList> call = api.getSuccess();

        call.enqueue(new Callback<teamList>() {
            @Override
            public void onResponse(Call<teamList> call, final Response<teamList> response) {
                if (response.isSuccessful()) {
                    IP = "http://88.99.113.138";
                }
            }

            @Override
            public void onFailure(Call<teamList> call, Throwable t) {
                t.printStackTrace();
                Log.d("tag", "Ped, message is : " + t.getMessage());
                Log.d("tag", "Ped, call is : " + call.toString());

                final HttpLoggingInterceptor logging2 = new HttpLoggingInterceptor();
                logging2.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient client2 = new OkHttpClient.Builder()
                        .addInterceptor(logging2)
                        .build();


                Retrofit retrofit2 = new Retrofit.Builder()
                        .baseUrl("http://88.99.113.140")
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client2)
                        .build();

                api = retrofit2.create(Api.class);
                Call<teamList> call2 = api.getSuccess();

                call2.enqueue(new Callback<teamList>() {
                    @Override
                    public void onResponse(Call<teamList> call, final Response<teamList> response) {
                        if (response.isSuccessful()) {
                            IP = "http://88.99.113.140";
                        }
                    }

                    @Override
                    public void onFailure(Call<teamList> call, Throwable t) {
                        t.printStackTrace();
                        Log.d("tag", "Ped, message is : " + t.getMessage());
                        Log.d("tag", "Ped, call is : " + call.toString());

                        final HttpLoggingInterceptor logging3 = new HttpLoggingInterceptor();
                        logging3.setLevel(HttpLoggingInterceptor.Level.BODY);
                        OkHttpClient client3 = new OkHttpClient.Builder()
                                .addInterceptor(logging3)
                                .build();


                        Retrofit retrofit3 = new Retrofit.Builder()
                                .baseUrl("http://88.99.113.141")
                                .addConverterFactory(GsonConverterFactory.create())
                                .client(client3)
                                .build();

                        api = retrofit3.create(Api.class);
                        Call<teamList> call3 = api.getSuccess();

                        call3.enqueue(new Callback<teamList>() {
                            @Override
                            public void onResponse(Call<teamList> call, final Response<teamList> response) {
                                if (response.isSuccessful()) {
                                    IP = "http://88.99.113.141";
                                }
                            }

                            @Override
                            public void onFailure(Call<teamList> call, Throwable t) {
                                t.printStackTrace();
                                Log.d("tag", "Ped, message is : " + t.getMessage());
                                Log.d("tag", "Ped, call is : " + call.toString());

                                final HttpLoggingInterceptor logging4 = new HttpLoggingInterceptor();
                                logging4.setLevel(HttpLoggingInterceptor.Level.BODY);
                                OkHttpClient client4 = new OkHttpClient.Builder()
                                        .addInterceptor(logging4)
                                        .build();


                                Retrofit retrofit4 = new Retrofit.Builder()
                                        .baseUrl("http://88.99.113.143")
                                        .addConverterFactory(GsonConverterFactory.create())
                                        .client(client4)
                                        .build();

                                api = retrofit4.create(Api.class);
                                Call<teamList> call4 = api.getSuccess();

                                call4.enqueue(new Callback<teamList>() {
                                    @Override
                                    public void onResponse(Call<teamList> call, final Response<teamList> response) {
                                        if (response.isSuccessful()) {
                                            IP = "http://88.99.113.143";
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<teamList> call, Throwable t) {
                                        t.printStackTrace();
                                        Log.d("tag", "Ped, message is : " + t.getMessage());
                                        Log.d("tag", "Ped, call is : " + call.toString());

                                        final HttpLoggingInterceptor logging5 = new HttpLoggingInterceptor();
                                        logging5.setLevel(HttpLoggingInterceptor.Level.BODY);
                                        OkHttpClient client5 = new OkHttpClient.Builder()
                                                .addInterceptor(logging5)
                                                .build();


                                        Retrofit retrofit5 = new Retrofit.Builder()
                                                .baseUrl("http://88.99.113.144")
                                                .addConverterFactory(GsonConverterFactory.create())
                                                .client(client5)
                                                .build();

                                        api = retrofit5.create(Api.class);
                                        Call<teamList> call5 = api.getSuccess();

                                        call5.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call, final Response<teamList> response) {
                                                if (response.isSuccessful()) {
                                                    IP = "http://88.99.113.144";
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call, Throwable t) {
                                                t.printStackTrace();
                                                Log.d("tag", "Ped, message is : " + t.getMessage());
                                                Log.d("tag", "Ped, call is : " + call.toString());

                                                final HttpLoggingInterceptor logging6 = new HttpLoggingInterceptor();
                                                logging6.setLevel(HttpLoggingInterceptor.Level.BODY);
                                                OkHttpClient client6 = new OkHttpClient.Builder()
                                                        .addInterceptor(logging6)
                                                        .build();


                                                Retrofit retrofit6 = new Retrofit.Builder()
                                                        .baseUrl("http://88.99.113.146")
                                                        .addConverterFactory(GsonConverterFactory.create())
                                                        .client(client6)
                                                        .build();

                                                api = retrofit6.create(Api.class);
                                                Call<teamList> call6 = api.getSuccess();

                                                call6.enqueue(new Callback<teamList>() {
                                                    @Override
                                                    public void onResponse(Call<teamList> call, final Response<teamList> response) {
                                                        if (response.isSuccessful()) {
                                                            IP = "http://88.99.113.146";
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<teamList> call, Throwable t) {
                                                        t.printStackTrace();
                                                        Log.d("tag", "Ped, message is : " + t.getMessage());
                                                        Log.d("tag", "Ped, call is : " + call.toString());

                                                        final HttpLoggingInterceptor logging7 = new HttpLoggingInterceptor();
                                                        logging7.setLevel(HttpLoggingInterceptor.Level.BODY);
                                                        OkHttpClient client7 = new OkHttpClient.Builder()
                                                                .addInterceptor(logging7)
                                                                .build();


                                                        Retrofit retrofit7 = new Retrofit.Builder()
                                                                .baseUrl("http://88.99.113.147")
                                                                .addConverterFactory(GsonConverterFactory.create())
                                                                .client(client7)
                                                                .build();

                                                        api = retrofit7.create(Api.class);
                                                        Call<teamList> call7 = api.getSuccess();

                                                        call7.enqueue(new Callback<teamList>() {
                                                            @Override
                                                            public void onResponse(Call<teamList> call, final Response<teamList> response) {
                                                                if (response.isSuccessful()) {
                                                                    IP = "http://88.99.113.147";
                                                                }
                                                            }

                                                            @Override
                                                            public void onFailure(Call<teamList> call, Throwable t) {
                                                                t.printStackTrace();
                                                                Log.d("tag", "Ped, message is : " + t.getMessage());
                                                                Log.d("tag", "Ped, call is : " + call.toString());
                                                                IP = "http://88.99.113.138";
                                                                Toast.makeText(StartupPanel.this, "عدم اتصال به سرور لطفا مجدد تلاش کنید", Toast.LENGTH_LONG).show();
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });




        /*







        final HttpLoggingInterceptor logging7 = new HttpLoggingInterceptor();
        logging7.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client7 = new OkHttpClient.Builder()
                .addInterceptor(logging7)
                .build();


        Retrofit retrofit7 = new Retrofit.Builder()
                .baseUrl("http://88.99.113.147")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client7)
                .build();

        api = retrofit7.create(Api.class);
        Call<teamList> call7 = api.getSuccess();

        call7.enqueue(new Callback<teamList>() {
            @Override
            public void onResponse(Call<teamList> call, final Response<teamList> response) {
                if (response.isSuccessful()) {
                    IP = "http://88.99.113.147";
                }
            }

            @Override
            public void onFailure(Call<teamList> call, Throwable t) {
                t.printStackTrace();
                Log.d("tag", "Ped, message is : " + t.getMessage());
                Log.d("tag", "Ped, call is : " + call.toString());
            }
        });*/


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                progressBar.setVisibility(View.GONE);
                finish();
                Intent intent = new Intent(StartupPanel.this, LoginPanel.class);
                startActivity(intent);
            }
        },SPLASH_TIME_OUT);

    }
}
