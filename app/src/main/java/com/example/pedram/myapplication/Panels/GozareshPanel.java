package com.example.pedram.myapplication.Panels;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pedram.myapplication.Adapter.DataAdapter;
import com.example.pedram.myapplication.Adapter.DataAdapter2;
import com.example.pedram.myapplication.Fragments.PrivatePanel;
import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.Matches;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_get.UserMatch;
import com.example.pedram.myapplication.models_get.teamList;
import com.example.pedram.myapplication.models_post.NewPassword;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pedram on 5/11/2018.
 */

public class GozareshPanel extends AppCompatActivity {

    Typeface iransans_font;
    LinearLayout linearLayout;
    TextView group;
    Dialog CoinDialog;
    static TextView tv_price, price, team1_name, team2_name, vaziat, tv_vaziat, title;
    Button button1;
    String Button_Name;
    ImageView back, coin, iv_team1, iv_team2;
    private Api api;
    DataAdapter2 dataAdapter2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gozaresh_panel);

        iv_team1 = (ImageView) findViewById(R.id.iv_team1);
        iv_team2 = (ImageView) findViewById(R.id.iv_team2);
        group = (TextView) findViewById(R.id.group);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout2);
        price = (TextView) findViewById(R.id.price);
        tv_price = (TextView) findViewById(R.id.TV_price);
        team1_name = (TextView) findViewById(R.id.tv_team1_name);
        team2_name = (TextView) findViewById(R.id.tv_team2_name);
        vaziat = (TextView) findViewById(R.id.vaziat);
        tv_vaziat = (TextView) findViewById(R.id.TV_vaziat);
        button1 = (Button) findViewById(R.id.button1);
        back = (ImageView) findViewById(R.id.IV_back);
        coin = (ImageView) findViewById(R.id.IV_coin);
        title = (TextView) findViewById(R.id.title);

        iransans_font = Typeface.createFromAsset(getAssets(), "iransans.ttf");

        price.setTypeface(iransans_font);
        tv_price.setTypeface(iransans_font);
        tv_vaziat.setTypeface(iransans_font);
        vaziat.setTypeface(iransans_font);
        button1.setTypeface(iransans_font);
        team1_name.setTypeface(iransans_font);
        team2_name.setTypeface(iransans_font);

        CoinDialog = new Dialog(this);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        coin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TextView removable_account, virtual_account, txtclose, vizhe, gheyre_vizhe;
                CoinDialog.setContentView(R.layout.coin);
                removable_account = CoinDialog.findViewById(R.id.removable_account);
                virtual_account = CoinDialog.findViewById(R.id.virtual_account);
                vizhe = CoinDialog.findViewById(R.id.vizhe);
                gheyre_vizhe = CoinDialog.findViewById(R.id.gheyre_vizhe);
                vizhe.setTypeface(iransans_font);
                gheyre_vizhe.setTypeface(iransans_font);
                removable_account.setTypeface(iransans_font);
                virtual_account.setTypeface(iransans_font);
                txtclose = CoinDialog.findViewById(R.id.textclose);
                CoinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                CoinDialog.show();

                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient client = new OkHttpClient.Builder()
                        .addInterceptor(logging)
                        .build();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Api.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build();

                api = retrofit.create(Api.class);

                LoginPanel loginPanel = new LoginPanel();
                NewPassword newPassword = new NewPassword();

                Call<NwPass> callcoin = api.newPassword(newPassword, loginPanel.id);

                callcoin.enqueue(new Callback<NwPass>() {
                    @Override
                    public void onResponse(Call<NwPass> callcoin, Response<NwPass> response) {
                        if (response.isSuccessful()) {
                            removable_account.setText(String.valueOf(response.body().getRemovableAccount()));
                            virtual_account.setText(String.valueOf(response.body().getVirtualAccount()));
                            vizhe.setText(String.valueOf(response.body().getValueSpecialOption()));
                            gheyre_vizhe.setText(String.valueOf(response.body().getValueExcSpecialOption()));
                        }
                        else
                            Toast.makeText(GozareshPanel.this, "خطا در ارسال اطلاعات", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<NwPass> callcoin, Throwable t) {
                        t.printStackTrace();
                        Log.d("tag", "Ped, message is : " + t.getMessage());
                        Log.d("tag", "Ped, call is : " + callcoin.toString());
                        Toast.makeText(GozareshPanel.this, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                    }
                });

                txtclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CoinDialog.dismiss();
                    }
                });
            }
        });

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        final Api request = retrofit.create(Api.class);
        final LoginPanel loginPanel = new LoginPanel();

        Call<UserMatch> call0 = request.getOption(loginPanel.getId(), dataAdapter2.MatchID, loginPanel.getAccess());
        call0.enqueue(new Callback<UserMatch>() {
            @Override
            public void onResponse(Call<UserMatch> call2, final Response<UserMatch> response) {

                if (response.isSuccessful()) {
                    price.setText(response.body().getValue().toString() + " تومان ");
                    if (response.body().getWOrL().equals("NULL"))
                        vaziat.setText("نامعلوم");
                    if (response.body().getWOrL().equals("W"))
                        vaziat.setText("گزینه برنده (مبلغ " + response.body().getWinValue() +" طبق ضریب به موجودی نقدی شما اضافه شد) " );
                    if (response.body().getWOrL().equals("L"))
                        vaziat.setText("باخت");
                    if (response.body().getWOrL().equals("N"))
                        vaziat.setText("گزینه خنثی (مبلغ " + response.body().getWinValue() +" به موجودی نقدی شما بازگردانده شد) ");


                    Call<Matches> call = request.getMatch(dataAdapter2.MatchID, loginPanel.getAccess());
                    call.enqueue(new Callback<Matches>() {
                        @Override
                        public void onResponse(Call<Matches> call, Response<Matches> response2) {
                            if (response.isSuccessful()) {

                                title.setText(response2.body().getTitle());

                                if (response.body().getSelectedAnswer().equals("A1"))
                                    button1.setText(response2.body().getName_M1());
                                if (response.body().getSelectedAnswer().equals("A2"))
                                    button1.setText(response2.body().getName_M2());
                                if (response.body().getSelectedAnswer().equals("A3"))
                                    button1.setText(response2.body().getName_M3());
                                if (response.body().getSelectedAnswer().equals("A4"))
                                    button1.setText(response2.body().getName_M4());
                                if (response.body().getSelectedAnswer().equals("A5"))
                                    button1.setText(response2.body().getName_M5());
                                if (response.body().getSelectedAnswer().equals("A6"))
                                    button1.setText(response2.body().getName_M3());

                                if (response2.body().getGroup().toString().equals("NULL"))
                                    group.setVisibility(View.GONE);
                                else
                                    group.setText(response2.body().getGroup() + " : گروه ");

                                Call<teamList> call1 = request.getTeam(response2.body().getTeam_id(), loginPanel.getAccess());
                                call1.enqueue(new Callback<teamList>() {
                                    @Override
                                    public void onResponse(Call<teamList> call, Response<teamList> response) {
                                        if (response.isSuccessful()) {
                                            if (response.body().getName().equals("3A")) {
                                                group.setVisibility(View.VISIBLE);
                                                linearLayout.setVisibility(View.GONE);
                                /*iv_team1.setImageResource(R.drawable.bac);
                                team1_name.setText(responsee.body().getGroup());*/
                                            }
                                            else if (response.body().getName().equals("3B")) {
                                                group.setVisibility(View.VISIBLE);
                                                linearLayout.setVisibility(View.GONE);
                                /*iv_team1.setImageResource(R.drawable.bac);
                                team1_name.setText(responsee.body().getGroup());*/
                                            }
                                            else {
                                                group.setVisibility(View.VISIBLE);
                                                team1_name.setText(response.body().getName());
                                            }
                                            /*if (team1_name.getText().equals("روسیه")) {
                                                iv_team1.setImageResource(R.drawable.flag_russia);
                                            }
                                            if (team1_name.getText().equals("برزیل")) {
                                                iv_team1.setImageResource(R.drawable.flag_brazil);
                                            }
                                            if (team1_name.getText().equals("ایران")) {
                                                iv_team1.setImageResource(R.drawable.flag_iran);
                                            }
                                            if (team1_name.getText().equals("ژاپن")) {
                                                iv_team1.setImageResource(R.drawable.flag_japan);
                                            }
                                            if (team1_name.getText().equals("مکزیک")) {
                                                iv_team1.setImageResource(R.drawable.flag_mexico);
                                            }
                                            if (team1_name.getText().equals("بلژیک")) {
                                                iv_team1.setImageResource(R.drawable.flag_belgium);
                                            }
                                            if (team1_name.getText().equals("کره جنوبی")) {
                                                iv_team1.setImageResource(R.drawable.flag_korea);
                                            }
                                            if (team1_name.getText().equals("عربستان")) {
                                                iv_team1.setImageResource(R.drawable.flag_arabie);
                                            }
                                            if (team1_name.getText().equals("آلمان")) {
                                                iv_team1.setImageResource(R.drawable.flag_almanya);
                                            }
                                            if (team1_name.getText().equals("انگلستان")) {
                                                iv_team1.setImageResource(R.drawable.flag_england);
                                            }
                                            if (team1_name.getText().equals("اسپانیا")) {
                                                iv_team1.setImageResource(R.drawable.flag_spain);
                                            }
                                            if (team1_name.getText().equals("نیجریه")) {
                                                iv_team1.setImageResource(R.drawable.flag_nigeria);
                                            }
                                            if (team1_name.getText().equals("کاستاریکا")) {
                                                iv_team1.setImageResource(R.drawable.flag_costarica);
                                            }
                                            if (team1_name.getText().equals("لهستان")) {
                                                iv_team1.setImageResource(R.drawable.flag_polska);
                                            }
                                            if (team1_name.getText().equals("مصر")) {
                                                iv_team1.setImageResource(R.drawable.flag_egypt);
                                            }
                                            if (team1_name.getText().equals("ایسلند")) {
                                                iv_team1.setImageResource(R.drawable.flag_iceland);
                                            }
                                            if (team1_name.getText().equals("صربستان")) {
                                                iv_team1.setImageResource(R.drawable.flag_serbia);
                                            }
                                            if (team1_name.getText().equals("پرتغال")) {
                                                iv_team1.setImageResource(R.drawable.flag_portugal);
                                            }
                                            if (team1_name.getText().equals("فرانسه")) {
                                                iv_team1.setImageResource(R.drawable.flag_france);
                                            }
                                            if (team1_name.getText().equals("اروگوئه")) {
                                                iv_team1.setImageResource(R.drawable.flag_uruguay);
                                            }
                                            if (team1_name.getText().equals("آرژانتین")) {
                                                iv_team1.setImageResource(R.drawable.flag_argentina);
                                            }
                                            if (team1_name.getText().equals("کلمبیا")) {
                                                iv_team1.setImageResource(R.drawable.flag_colombia);
                                            }
                                            if (team1_name.getText().equals("پاناما")) {
                                                iv_team1.setImageResource(R.drawable.flag_panama);
                                            }
                                            if (team1_name.getText().equals("سنگال")) {
                                                iv_team1.setImageResource(R.drawable.flag_senegal);
                                            }
                                            if (team1_name.getText().equals("مراکش")) {
                                                iv_team1.setImageResource(R.drawable.flag_morocco);
                                            }
                                            if (team1_name.getText().equals("تونس")) {
                                                iv_team1.setImageResource(R.drawable.flag_tunisia);
                                            }
                                            if (team1_name.getText().equals("سوئیس")) {
                                                iv_team1.setImageResource(R.drawable.flag_swiss);
                                            }
                                            if (team1_name.getText().equals("کرواسی")) {
                                                iv_team1.setImageResource(R.drawable.flag_croatia);
                                            }
                                            if (team1_name.getText().equals("سوئد")) {
                                                iv_team1.setImageResource(R.drawable.flag_sweden);
                                            }
                                            if (team1_name.getText().equals("دانمارک")) {
                                                iv_team1.setImageResource(R.drawable.flag_denmark);
                                            }
                                            if (team1_name.getText().equals("استرالیا")) {
                                                iv_team1.setImageResource(R.drawable.flag_australia);
                                            }
                                            if (team1_name.getText().equals("پرو")) {
                                                iv_team1.setImageResource(R.drawable.flag_peru);
                                            }*/
                                        } else
                                            Toast.makeText(GozareshPanel.this, "خطا در ارسال اطلاعات", Toast.LENGTH_LONG).show();
                                    }
                                    @Override
                                    public void onFailure(Call<teamList> call, Throwable t) {
                                        t.printStackTrace();
                                        Log.d("tag", "Ped, message is : " + t.getMessage());
                                        Log.d("tag", "Ped, call is : " + call.toString());
                                        Toast.makeText(GozareshPanel.this, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                                    }
                                });

                                Call<teamList> call2 = request.getTeam(response2.body().getTeam2_id(), loginPanel.getAccess());
                                call2.enqueue(new Callback<teamList>() {
                                    @Override
                                    public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                        if (response.isSuccessful()) {
                                            team2_name.setText(response.body().getName());

                                            /*if (team2_name.getText().equals("روسیه")) {
                                                iv_team2.setImageResource(R.drawable.flag_russia);
                                            }
                                            if (team2_name.getText().equals("برزیل")) {
                                                iv_team2.setImageResource(R.drawable.flag_brazil);
                                            }
                                            if (team2_name.getText().equals("ایران")) {
                                                iv_team2.setImageResource(R.drawable.flag_iran);
                                            }
                                            if (team2_name.getText().equals("ژاپن")) {
                                                iv_team2.setImageResource(R.drawable.flag_japan);
                                            }
                                            if (team2_name.getText().equals("مکزیک")) {
                                                iv_team2.setImageResource(R.drawable.flag_mexico);
                                            }
                                            if (team2_name.getText().equals("بلژیک")) {
                                                iv_team2.setImageResource(R.drawable.flag_belgium);
                                            }
                                            if (team2_name.getText().equals("کره جنوبی")) {
                                                iv_team2.setImageResource(R.drawable.flag_korea);
                                            }
                                            if (team2_name.getText().equals("عربستان")) {
                                                iv_team2.setImageResource(R.drawable.flag_arabie);
                                            }
                                            if (team2_name.getText().equals("آلمان")) {
                                                iv_team2.setImageResource(R.drawable.flag_almanya);
                                            }
                                            if (team2_name.getText().equals("انگلستان")) {
                                                iv_team2.setImageResource(R.drawable.flag_england);
                                            }
                                            if (team2_name.getText().equals("اسپانیا")) {
                                                iv_team2.setImageResource(R.drawable.flag_spain);
                                            }
                                            if (team2_name.getText().equals("نیجریه")) {
                                                iv_team2.setImageResource(R.drawable.flag_nigeria);
                                            }
                                            if (team2_name.getText().equals("کاستاریکا")) {
                                                iv_team2.setImageResource(R.drawable.flag_costarica);
                                            }
                                            if (team2_name.getText().equals("لهستان")) {
                                                iv_team2.setImageResource(R.drawable.flag_polska);
                                            }
                                            if (team2_name.getText().equals("مصر")) {
                                                iv_team2.setImageResource(R.drawable.flag_egypt);
                                            }
                                            if (team2_name.getText().equals("ایسلند")) {
                                                iv_team2.setImageResource(R.drawable.flag_iceland);
                                            }
                                            if (team2_name.getText().equals("صربستان")) {
                                                iv_team2.setImageResource(R.drawable.flag_serbia);
                                            }
                                            if (team2_name.getText().equals("پرتغال")) {
                                                iv_team2.setImageResource(R.drawable.flag_portugal);
                                            }
                                            if (team2_name.getText().equals("فرانسه")) {
                                                iv_team2.setImageResource(R.drawable.flag_france);
                                            }
                                            if (team2_name.getText().equals("اروگوئه")) {
                                                iv_team2.setImageResource(R.drawable.flag_uruguay);
                                            }
                                            if (team2_name.getText().equals("آرژانتین")) {
                                                iv_team2.setImageResource(R.drawable.flag_argentina);
                                            }
                                            if (team2_name.getText().equals("کلمبیا")) {
                                                iv_team2.setImageResource(R.drawable.flag_colombia);
                                            }
                                            if (team2_name.getText().equals("پاناما")) {
                                                iv_team2.setImageResource(R.drawable.flag_panama);
                                            }
                                            if (team2_name.getText().equals("سنگال")) {
                                                iv_team2.setImageResource(R.drawable.flag_senegal);
                                            }
                                            if (team2_name.getText().equals("مراکش")) {
                                                iv_team2.setImageResource(R.drawable.flag_morocco);
                                            }
                                            if (team2_name.getText().equals("تونس")) {
                                                iv_team2.setImageResource(R.drawable.flag_tunisia);
                                            }
                                            if (team2_name.getText().equals("سوئیس")) {
                                                iv_team2.setImageResource(R.drawable.flag_swiss);
                                            }
                                            if (team2_name.getText().equals("کرواسی")) {
                                                iv_team2.setImageResource(R.drawable.flag_croatia);
                                            }
                                            if (team2_name.getText().equals("سوئد")) {
                                                iv_team2.setImageResource(R.drawable.flag_sweden);
                                            }
                                            if (team2_name.getText().equals("دانمارک")) {
                                                iv_team2.setImageResource(R.drawable.flag_denmark);
                                            }
                                            if (team2_name.getText().equals("استرالیا")) {
                                                iv_team2.setImageResource(R.drawable.flag_australia);
                                            }
                                            if (team2_name.getText().equals("پرو")) {
                                                iv_team2.setImageResource(R.drawable.flag_peru);
                                            }*/
                                        } else
                                            Toast.makeText(GozareshPanel.this, "خطا در ارسال اطلاعات", Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onFailure(Call<teamList> call2, Throwable t) {
                                        t.printStackTrace();
                                        Log.d("tag", "Ped, message is : " + t.getMessage());
                                        Log.d("tag", "Ped, call is : " + call2.toString());
                                        Toast.makeText(GozareshPanel.this, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            else {
                                Toast.makeText(GozareshPanel.this, "خطا در ارسال اطلاعات", Toast.LENGTH_LONG).show();
                            }



                            Call<ResponseBody> call_team = request.getTeamImage(response2.body().getTeam_id(), loginPanel.getAccess());

                            call_team.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {
                                        boolean FileDownloaded = DownloadImage(response.body());

                                    } else
                                        Toast.makeText(GozareshPanel.this, "خطا در ارسال اطلاعات", Toast.LENGTH_LONG).show();
                                }

                                private boolean DownloadImage(ResponseBody body) {
                                    try {
                                        Log.d("DownloadImage", "Reading and writing file");
                                        InputStream in = null;
                                        FileOutputStream out = null;

                                        try {
                                            in = body.byteStream();
                                            out = new FileOutputStream(GozareshPanel.this.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                            int c;

                                            while ((c = in.read()) != -1) {
                                                out.write(c);
                                            }
                                        }
                                        catch (IOException e) {
                                            Log.d("DownloadImage",e.toString());
                                            return false;
                                        }
                                        finally {
                                            if (in != null) {
                                                in.close();
                                            }
                                            if (out != null) {
                                                out.close();
                                            }
                                        }

                                        int width, height;

                                        Bitmap bMap = BitmapFactory.decodeFile(GozareshPanel.this.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                        width = bMap.getWidth();
                                        height = bMap.getHeight();
                                        Bitmap bMap2 = Bitmap.createScaledBitmap(bMap, width, height, false);
                                        iv_team1.setImageBitmap(bMap2);

                                        return true;

                                    } catch (IOException e) {
                                        Log.d("DownloadImage",e.toString());
                                        return false;
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    t.printStackTrace();
                                    Log.d("tag", "Ped, message is : " + t.getMessage());
                                    Log.d("tag", "Ped, call is : " + call.toString());
                                    Toast.makeText(GozareshPanel.this, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                                }
                            });

                            Call<ResponseBody> call_team2 = request.getTeamImage(response2.body().getTeam2_id(), loginPanel.getAccess());

                            call_team2.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {
                                        boolean FileDownloaded = DownloadImage(response.body());

                                    } else
                                        Toast.makeText(GozareshPanel.this, "خطا در ارسال اطلاعات", Toast.LENGTH_LONG).show();
                                }

                                private boolean DownloadImage(ResponseBody body) {
                                    try {
                                        Log.d("DownloadImage", "Reading and writing file");
                                        InputStream in = null;
                                        FileOutputStream out = null;

                                        try {
                                            in = body.byteStream();
                                            out = new FileOutputStream(getExternalFilesDir(null) + File.separator + "Android.jpg");
                                            int c;

                                            while ((c = in.read()) != -1) {
                                                out.write(c);
                                            }
                                        }
                                        catch (IOException e) {
                                            Log.d("DownloadImage",e.toString());
                                            return false;
                                        }
                                        finally {
                                            if (in != null) {
                                                in.close();
                                            }
                                            if (out != null) {
                                                out.close();
                                            }
                                        }

                                        int width, height;

                                        Bitmap bMap = BitmapFactory.decodeFile(getExternalFilesDir(null) + File.separator + "Android.jpg");
                                        width = bMap.getWidth();
                                        height = bMap.getHeight();
                                        Bitmap bMap2 = Bitmap.createScaledBitmap(bMap, width, height, false);
                                        iv_team2.setImageBitmap(bMap2);

                                        return true;

                                    } catch (IOException e) {
                                        Log.d("DownloadImage",e.toString());
                                        return false;
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    t.printStackTrace();
                                    Log.d("tag", "Ped, message is : " + t.getMessage());
                                    Log.d("tag", "Ped, call is : " + call.toString());
                                    Toast.makeText(GozareshPanel.this, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                                }
                            });



                        }

                        @Override
                        public void onFailure(Call<Matches> call0, Throwable t) {
                            t.printStackTrace();
                            Log.d("tag", "Ped, message is : " + t.getMessage());
                            Log.d("tag", "Ped, call is : " + call0.toString());
                            Toast.makeText(GozareshPanel.this, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                        }
                    });
                }
                else
                    Toast.makeText(GozareshPanel.this, "خطا در ارسال اطلاعات", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<UserMatch> call2, Throwable t) {
                t.printStackTrace();
                Log.d("tag", "Ped, message is : " + t.getMessage());
                Log.d("tag", "Ped, call is : " + call2.toString());
                Toast.makeText(GozareshPanel.this, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
            }
        });

    }
}
