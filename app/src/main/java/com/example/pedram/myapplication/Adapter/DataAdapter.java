package com.example.pedram.myapplication.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pedram.myapplication.Panels.GozareshPanel;
import com.example.pedram.myapplication.Panels.LoginPanel;
import com.example.pedram.myapplication.Panels.MatchPanel;
import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.Panels.UserPanel;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.DbUser;
import com.example.pedram.myapplication.models_get.FpUser;
import com.example.pedram.myapplication.models_get.Guide;
import com.example.pedram.myapplication.models_get.JSONResponse;
import com.example.pedram.myapplication.models_get.JSONResponse2;
import com.example.pedram.myapplication.models_get.JSONResponse3;
import com.example.pedram.myapplication.models_get.JSONResponse4;
import com.example.pedram.myapplication.models_get.Matches;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_get.RgUser;
import com.example.pedram.myapplication.models_get.UserMatch;
import com.example.pedram.myapplication.models_get.teamList;
import com.example.pedram.myapplication.models_post.ForgotPassword;
import com.example.pedram.myapplication.models_post.Login;
import com.example.pedram.myapplication.models_post.NewPassword;
import com.example.pedram.myapplication.models_post.OptionAnswer;
import com.example.pedram.myapplication.models_post.Options;
import com.example.pedram.myapplication.models_post.OptionsValue;
import com.example.pedram.myapplication.models_post.Register;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Path;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Matches> android;
    public static int MatchID;
    public static int team1_ID, team2_ID;
    public static String team1_name, team2_name;

    public DataAdapter(Context context, ArrayList<Matches> android) {
        this.android = android;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        final Api request = retrofit.create(Api.class);
        final LoginPanel loginPanel = new LoginPanel();

        Call<teamList> call = request.getTeam(android.get(i).getTeam_id(), loginPanel.getAccess());
        team1_ID = android.get(i).getTeam_id();
        call.enqueue(new Callback<teamList>() {
            @Override
            public void onResponse(Call<teamList> call, Response<teamList> response) {
                team1_name = response.body().getName();
//                +++++++++++++++++++++++++++++++++
//                TEAM 1 ba TEAM2 jashun bar axe
//                +++++++++++++++++++++++++++++++++
                if (team1_name.equals("3A")) {
                    viewHolder.mid.setVisibility(View.VISIBLE);
                    viewHolder.tv_api_level.setVisibility(View.GONE);
                    viewHolder.tv_name.setVisibility(View.GONE);
                    viewHolder.tv_version.setVisibility(View.GONE);
                    viewHolder.mid.setText(android.get(i).getTitle());

                    /*if (android.get(i).getGroup().equals("A"))
                        viewHolder.iv_team1.setImageResource(R.drawable.group_a1);
                    if (android.get(i).getGroup().equals("B"))
                        viewHolder.iv_team1.setImageResource(R.drawable.group_b1);
                    if (android.get(i).getGroup().equals("C"))
                        viewHolder.iv_team1.setImageResource(R.drawable.group_c1);
                    if (android.get(i).getGroup().equals("D"))
                        viewHolder.iv_team1.setImageResource(R.drawable.group_d1);
                    if (android.get(i).getGroup().equals("E"))
                        viewHolder.iv_team1.setImageResource(R.drawable.group_e1);
                    if (android.get(i).getGroup().equals("F"))
                        viewHolder.iv_team1.setImageResource(R.drawable.group_f1);
                    if (android.get(i).getGroup().equals("G"))
                        viewHolder.iv_team1.setImageResource(R.drawable.group_g1);
                    if (android.get(i).getGroup().equals("H"))
                        viewHolder.iv_team1.setImageResource(R.drawable.group_h1);*/


                    Call<ResponseBody> call_right_team = request.getRightTeamImage(android.get(i).getId(), loginPanel.getAccess());

                    call_right_team.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                boolean FileDownloaded = DownloadImage(response.body());

                            }
                        }

                        private boolean DownloadImage(ResponseBody body) {
                            try {
                                Log.d("DownloadImage", "Reading and writing file");
                                InputStream in = null;
                                FileOutputStream out = null;

                                try {
                                    in = body.byteStream();
                                    out = new FileOutputStream(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                    int c;

                                    while ((c = in.read()) != -1) {
                                        out.write(c);
                                    }
                                }
                                catch (IOException e) {
                                    Log.d("DownloadImage",e.toString());
                                    return false;
                                }
                                finally {
                                    if (in != null) {
                                        in.close();
                                    }
                                    if (out != null) {
                                        out.close();
                                    }
                                }

                                int width, height;

                                Bitmap bMap = BitmapFactory.decodeFile(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                width = bMap.getWidth();
                                height = bMap.getHeight();
                                Bitmap bMap2 = Bitmap.createScaledBitmap(bMap, width, height, false);
                                viewHolder.iv_team1.setImageBitmap(bMap2);

                                return true;

                            } catch (IOException e) {
                                Log.d("DownloadImage",e.toString());
                                return false;
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            t.printStackTrace();
                            Log.d("tag", "Ped, message is : " + t.getMessage());
                            Log.d("tag", "Ped, call is : " + call.toString());
                            Toast.makeText(context, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                        }
                    });


                } else if (team1_name.equals("3B")) {
                    viewHolder.mid.setVisibility(View.VISIBLE);
                    viewHolder.tv_api_level.setVisibility(View.GONE);
                    viewHolder.tv_name.setVisibility(View.GONE);
                    viewHolder.tv_version.setVisibility(View.GONE);
                    viewHolder.mid.setText(android.get(i).getTitle());



                    Call<ResponseBody> call_right_team = request.getRightTeamImage(android.get(i).getId(), loginPanel.getAccess());

                    call_right_team.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                boolean FileDownloaded = DownloadImage(response.body());

                            }
                        }

                        private boolean DownloadImage(ResponseBody body) {
                            try {
                                Log.d("DownloadImage", "Reading and writing file");
                                InputStream in = null;
                                FileOutputStream out = null;

                                try {
                                    in = body.byteStream();
                                    out = new FileOutputStream(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                    int c;

                                    while ((c = in.read()) != -1) {
                                        out.write(c);
                                    }
                                }
                                catch (IOException e) {
                                    Log.d("DownloadImage",e.toString());
                                    return false;
                                }
                                finally {
                                    if (in != null) {
                                        in.close();
                                    }
                                    if (out != null) {
                                        out.close();
                                    }
                                }

                                int width, height;

                                Bitmap bMap = BitmapFactory.decodeFile(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                width = bMap.getWidth();
                                height = bMap.getHeight();
                                Bitmap bMap2 = Bitmap.createScaledBitmap(bMap, width, height, false);
                                viewHolder.iv_team1.setImageBitmap(bMap2);

                                return true;

                            } catch (IOException e) {
                                Log.d("DownloadImage",e.toString());
                                return false;
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            t.printStackTrace();
                            Log.d("tag", "Ped, message is : " + t.getMessage());
                            Log.d("tag", "Ped, call is : " + call.toString());
                            Toast.makeText(context, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                        }
                    });

                    //viewHolder.iv_team1.setImageResource(R.drawable.cup);
                }
                else {
                    viewHolder.tv_api_level.setVisibility(View.VISIBLE);
                    viewHolder.tv_name.setVisibility(View.VISIBLE);
                    viewHolder.tv_version.setVisibility(View.VISIBLE);
                    viewHolder.mid.setVisibility(View.GONE);
                    viewHolder.tv_name.setText(team1_name);


                    Call<ResponseBody> call_team = request.getTeamImage(android.get(i).getTeam_id(), loginPanel.getAccess());

                    call_team.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                boolean FileDownloaded = DownloadImage(response.body());

                            }
                        }

                        private boolean DownloadImage(ResponseBody body) {
                            try {
                                Log.d("DownloadImage", "Reading and writing file");
                                InputStream in = null;
                                FileOutputStream out = null;

                                try {
                                    in = body.byteStream();
                                    out = new FileOutputStream(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                    int c;

                                    while ((c = in.read()) != -1) {
                                        out.write(c);
                                    }
                                }
                                catch (IOException e) {
                                    Log.d("DownloadImage",e.toString());
                                    return false;
                                }
                                finally {
                                    if (in != null) {
                                        in.close();
                                    }
                                    if (out != null) {
                                        out.close();
                                    }
                                }

                                int width, height;

                                Bitmap bMap = BitmapFactory.decodeFile(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                width = bMap.getWidth();
                                height = bMap.getHeight();
                                Bitmap bMap2 = Bitmap.createScaledBitmap(bMap, width, height, false);
                                viewHolder.iv_team1.setImageBitmap(bMap2);

                                return true;

                            } catch (IOException e) {
                                Log.d("DownloadImage",e.toString());
                                return false;
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            t.printStackTrace();
                            Log.d("tag", "Ped, message is : " + t.getMessage());
                            Log.d("tag", "Ped, call is : " + call.toString());
                            Toast.makeText(context, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                        }
                    });

                }
                /*if (team1_name.equals("روسیه")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_russia);
                }
                if (team1_name.equals("برزیل")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_brazil);
                }
                if (team1_name.equals("ایران")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_iran);
                }
                if (team1_name.equals("ژاپن")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_japan);
                }
                if (team1_name.equals("مکزیک")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_mexico);
                }
                if (team1_name.equals("بلژیک")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_belgium);
                }
                if (team1_name.equals("کره جنوبی")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_korea);
                }
                if (team1_name.equals("عربستان")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_arabie);
                }
                if (team1_name.equals("آلمان")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_almanya);
                }
                if (team1_name.equals("انگلستان")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_england);
                }
                if (team1_name.equals("اسپانیا")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_spain);
                }
                if (team1_name.equals("نیجریه")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_nigeria);
                }
                if (team1_name.equals("کاستاریکا")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_costarica);
                }
                if (team1_name.equals("لهستان")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_polska);
                }
                if (team1_name.equals("مصر")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_egypt);
                }
                if (team1_name.equals("ایسلند")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_iceland);
                }
                if (team1_name.equals("صربستان")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_serbia);
                }
                if (team1_name.equals("پرتغال")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_portugal);
                }
                if (team1_name.equals("فرانسه")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_france);
                }
                if (team1_name.equals("اروگوئه")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_uruguay);
                }
                if (team1_name.equals("آرژانتین")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_argentina);
                }
                if (team1_name.equals("کلمبیا")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_colombia);
                }
                if (team1_name.equals("پاناما")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_panama);
                }
                if (team1_name.equals("سنگال")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_senegal);
                }
                if (team1_name.equals("مراکش")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_morocco);
                }
                if (team1_name.equals("تونس")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_tunisia);
                }
                if (team1_name.equals("سوئیس")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_swiss);
                }
                if (team1_name.equals("کرواسی")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_croatia);
                }
                if (team1_name.equals("سوئد")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_sweden);
                }
                if (team1_name.equals("دانمارک")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_denmark);
                }
                if (team1_name.equals("استرالیا")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_australia);
                }
                if (team1_name.equals("پرو")) {
                    viewHolder.iv_team1.setImageResource(R.drawable.flag_peru);
                }*/
            }

            @Override
            public void onFailure(Call<teamList> call, Throwable t) {
                Log.d("Error",t.getMessage());
            }
        });

        Call<teamList> call2 = request.getTeam(android.get(i).getTeam2_id(), loginPanel.getAccess());
        team2_ID = android.get(i).getTeam2_id();
        call2.enqueue(new Callback<teamList>() {
            @Override
            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                team2_name = response.body().getName();

//                +++++++++++++++++++++++++++++++++
//                TEAM 1 ba TEAM2 jashun bar axe
//                +++++++++++++++++++++++++++++++++
                if (team2_name.equals("3A")) {
                    viewHolder.mid.setVisibility(View.VISIBLE);
                    viewHolder.tv_api_level.setVisibility(View.GONE);
                    viewHolder.tv_name.setVisibility(View.GONE);
                    viewHolder.tv_version.setVisibility(View.GONE);
                    viewHolder.mid.setText(android.get(i).getTitle());

                    /*if (android.get(i).getGroup().equals("A"))
                        viewHolder.iv_team2.setImageResource(R.drawable.group_a2);
                    if (android.get(i).getGroup().equals("B"))
                        viewHolder.iv_team2.setImageResource(R.drawable.group_b2);
                    if (android.get(i).getGroup().equals("C"))
                        viewHolder.iv_team2.setImageResource(R.drawable.group_c2);
                    if (android.get(i).getGroup().equals("D"))
                        viewHolder.iv_team2.setImageResource(R.drawable.group_d2);
                    if (android.get(i).getGroup().equals("E"))
                        viewHolder.iv_team2.setImageResource(R.drawable.group_e2);
                    if (android.get(i).getGroup().equals("F"))
                        viewHolder.iv_team2.setImageResource(R.drawable.group_f2);
                    if (android.get(i).getGroup().equals("G"))
                        viewHolder.iv_team2.setImageResource(R.drawable.group_g2);
                    if (android.get(i).getGroup().equals("H"))
                        viewHolder.iv_team2.setImageResource(R.drawable.group_h2);*/


                    Call<ResponseBody> call_left_team2 = request.getLeftTeamImage(android.get(i).getId(), loginPanel.getAccess());

                    call_left_team2.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                boolean FileDownloaded = DownloadImage(response.body());

                            }
                        }

                        private boolean DownloadImage(ResponseBody body) {
                            try {
                                Log.d("DownloadImage", "Reading and writing file");
                                InputStream in = null;
                                FileOutputStream out = null;

                                try {
                                    in = body.byteStream();
                                    out = new FileOutputStream(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                    int c;

                                    while ((c = in.read()) != -1) {
                                        out.write(c);
                                    }
                                }
                                catch (IOException e) {
                                    Log.d("DownloadImage",e.toString());
                                    return false;
                                }
                                finally {
                                    if (in != null) {
                                        in.close();
                                    }
                                    if (out != null) {
                                        out.close();
                                    }
                                }

                                int width, height;

                                Bitmap bMap = BitmapFactory.decodeFile(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                width = bMap.getWidth();
                                height = bMap.getHeight();
                                Bitmap bMap2 = Bitmap.createScaledBitmap(bMap, width, height, false);
                                viewHolder.iv_team2.setImageBitmap(bMap2);

                                return true;

                            } catch (IOException e) {
                                Log.d("DownloadImage",e.toString());
                                return false;
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            t.printStackTrace();
                            Log.d("tag", "Ped, message is : " + t.getMessage());
                            Log.d("tag", "Ped, call is : " + call.toString());
                            Toast.makeText(context, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                        }
                    });



                } else if (team2_name.equals("3B")) {
                    viewHolder.mid.setVisibility(View.VISIBLE);
                    viewHolder.mid.setText(android.get(i).getTitle());
                    viewHolder.tv_api_level.setVisibility(View.GONE);
                    viewHolder.tv_name.setVisibility(View.GONE);
                    viewHolder.tv_version.setVisibility(View.GONE);


                    Call<ResponseBody> call_left_team2 = request.getLeftTeamImage(android.get(i).getId(), loginPanel.getAccess());

                    call_left_team2.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                boolean FileDownloaded = DownloadImage(response.body());

                            }
                        }

                        private boolean DownloadImage(ResponseBody body) {
                            try {
                                Log.d("DownloadImage", "Reading and writing file");
                                InputStream in = null;
                                FileOutputStream out = null;

                                try {
                                    in = body.byteStream();
                                    out = new FileOutputStream(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                    int c;

                                    while ((c = in.read()) != -1) {
                                        out.write(c);
                                    }
                                }
                                catch (IOException e) {
                                    Log.d("DownloadImage",e.toString());
                                    return false;
                                }
                                finally {
                                    if (in != null) {
                                        in.close();
                                    }
                                    if (out != null) {
                                        out.close();
                                    }
                                }

                                int width, height;

                                Bitmap bMap = BitmapFactory.decodeFile(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                width = bMap.getWidth();
                                height = bMap.getHeight();
                                Bitmap bMap2 = Bitmap.createScaledBitmap(bMap, width, height, false);
                                viewHolder.iv_team2.setImageBitmap(bMap2);

                                return true;

                            } catch (IOException e) {
                                Log.d("DownloadImage",e.toString());
                                return false;
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            t.printStackTrace();
                            Log.d("tag", "Ped, message is : " + t.getMessage());
                            Log.d("tag", "Ped, call is : " + call.toString());
                            Toast.makeText(context, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                        }
                    });

//                    viewHolder.iv_team2.setImageResource(R.drawable.cup);
                }
                else {
                    viewHolder.tv_api_level.setVisibility(View.VISIBLE);
                    viewHolder.tv_name.setVisibility(View.VISIBLE);
                    viewHolder.tv_version.setVisibility(View.VISIBLE);
                    viewHolder.mid.setVisibility(View.GONE);
                    viewHolder.tv_version.setText(team2_name);


                    Call<ResponseBody> call_team2 = request.getTeamImage(android.get(i).getTeam2_id(), loginPanel.getAccess());

                    call_team2.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                boolean FileDownloaded = DownloadImage(response.body());

                            }
                        }

                        private boolean DownloadImage(ResponseBody body) {
                            try {
                                Log.d("DownloadImage", "Reading and writing file");
                                InputStream in = null;
                                FileOutputStream out = null;

                                try {
                                    in = body.byteStream();
                                    out = new FileOutputStream(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                    int c;

                                    while ((c = in.read()) != -1) {
                                        out.write(c);
                                    }
                                }
                                catch (IOException e) {
                                    Log.d("DownloadImage",e.toString());
                                    return false;
                                }
                                finally {
                                    if (in != null) {
                                        in.close();
                                    }
                                    if (out != null) {
                                        out.close();
                                    }
                                }

                                int width, height;

                                Bitmap bMap = BitmapFactory.decodeFile(context.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                width = bMap.getWidth();
                                height = bMap.getHeight();
                                Bitmap bMap2 = Bitmap.createScaledBitmap(bMap, width, height, false);
                                viewHolder.iv_team2.setImageBitmap(bMap2);

                                return true;

                            } catch (IOException e) {
                                Log.d("DownloadImage",e.toString());
                                return false;
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            t.printStackTrace();
                            Log.d("tag", "Ped, message is : " + t.getMessage());
                            Log.d("tag", "Ped, call is : " + call.toString());
                            Toast.makeText(context, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                        }
                    });

                }



                /*if (team2_name.equals("روسیه")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_russia);
                }
                if (team2_name.equals("برزیل")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_brazil);
                }
                if (team2_name.equals("ایران")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_iran);
                }
                if (team2_name.equals("ژاپن")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_japan);
                }
                if (team2_name.equals("مکزیک")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_mexico);
                }
                if (team2_name.equals("بلژیک")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_belgium);
                }
                if (team2_name.equals("کره جنوبی")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_korea);
                }
                if (team2_name.equals("عربستان")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_arabie);
                }
                if (team2_name.equals("آلمان")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_almanya);
                }
                if (team2_name.equals("انگلستان")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_england);
                }
                if (team2_name.equals("اسپانیا")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_spain);
                }
                if (team2_name.equals("نیجریه")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_nigeria);
                }
                if (team2_name.equals("کاستاریکا")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_costarica);
                }
                if (team2_name.equals("لهستان")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_polska);
                }
                if (team2_name.equals("مصر")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_egypt);
                }
                if (team2_name.equals("ایسلند")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_iceland);
                }
                if (team2_name.equals("صربستان")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_serbia);
                }
                if (team2_name.equals("پرتغال")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_portugal);
                }
                if (team2_name.equals("فرانسه")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_france);
                }
                if (team2_name.equals("اروگوئه")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_uruguay);
                }
                if (team2_name.equals("آرژانتین")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_argentina);
                }
                if (team2_name.equals("کلمبیا")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_colombia);
                }
                if (team2_name.equals("پاناما")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_panama);
                }
                if (team2_name.equals("سنگال")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_senegal);
                }
                if (team2_name.equals("مراکش")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_morocco);
                }
                if (team2_name.equals("تونس")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_tunisia);
                }
                if (team2_name.equals("سوئیس")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_swiss);
                }
                if (team2_name.equals("کرواسی")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_croatia);
                }
                if (team2_name.equals("سوئد")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_sweden);
                }
                if (team2_name.equals("دانمارک")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_denmark);
                }
                if (team2_name.equals("استرالیا")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_australia);
                }
                if (team2_name.equals("پرو")) {
                    viewHolder.iv_team2.setImageResource(R.drawable.flag_peru);
                }*/
            }



            @Override
            public void onFailure(Call<teamList> call2, Throwable t) {
                Log.d("Error",t.getMessage());
            }
        });




        viewHolder.setItemClickListener(new Api() {
            @Override
            public Call<DbUser> tokenAccess(@Body Login login) {
                return null;
            }

            @Override
            public Call<RgUser> registerUsers(@Body Register register) {
                return null;
            }

            @Override
            public Call<FpUser> forgotUsers(@Body ForgotPassword forgotPassword) {
                return null;
            }

            @Override
            public Call<NwPass> newPassword(@Body NewPassword newPassword, @Path("id") Integer id) {
                return null;
            }

            @Override
            public Call<RgUser> getUser(@Path("id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<JSONResponse> getJSON(@Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<JSONResponse3> getCups(@Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<JSONResponse4> getBills(@Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<Guide> getGuides() {
                return null;
            }

            @Override
            public Call<teamList> getTeam(@Path("id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<Matches> getMatch(@Path("id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> setOption(@Body Options options, @Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> sendCash(@Body Options options, @Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> addValue1(@Body Options options, @Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> addValue(@Body Options options, @Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> setValue(@Body OptionsValue optionsValue, @Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> setAnswer(@Body OptionAnswer optionAnswer, @Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<UserMatch> getOption(@Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<ResponseBody> getCupImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<ResponseBody> getTeamImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<ResponseBody> getLeftTeamImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<ResponseBody> getRightTeamImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<JSONResponse2> getPreMatches(@Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> getSuccess() {
                return null;
            }

            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if (isLongClick) {
//                    Toast.makeText(context, "#" + android.get(i).getId() + " - "  + " (Long click)", Toast.LENGTH_SHORT).show();
                    MatchID = android.get(i).getId();
                    setMatchId(MatchID);
                    Intent intent = new Intent(context, MatchPanel.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {
                    MatchID = android.get(i).getId();
                    setMatchId(MatchID);
                    Intent intent = new Intent(context, MatchPanel.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
//                    Toast.makeText(context, "#" + android.get(i).getId() + " - " , Toast.LENGTH_SHORT).show();
                }
            }


        });
    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView tv_name, tv_version, mid;
        private EditText tv_api_level;
        private ImageView iv_team1, iv_team2;
        private Api requestInterface;
        private RelativeLayout relativeLayout;

        public ViewHolder(View view) {
            super(view);

            tv_name = (TextView)view.findViewById(R.id.tv_name);
            tv_version = (TextView)view.findViewById(R.id.tv_version);
            tv_api_level = (EditText) view.findViewById(R.id.tv_api_level);
            mid = (TextView) view.findViewById(R.id.mid);

            iv_team2 = (ImageView)view.findViewById(R.id.IV_team1);
            iv_team1 = (ImageView)view.findViewById(R.id.IV_team2);

            view.setTag(view);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);

        }

        public void setItemClickListener (Api requestInterface) {
            this.requestInterface = requestInterface;
        }

        @Override
        public void onClick(View view) {
            requestInterface.onClick(view, getPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            requestInterface.onClick(view, getPosition(), true);

            return true;
        }
    }


    public Integer getMatchId() {
        return MatchID;
    }

    public void setMatchId(Integer id) {
        this.MatchID = MatchID;
    }

}