package com.example.pedram.myapplication.Interface;

import android.content.Context;
import android.content.Intent;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.example.pedram.myapplication.Panels.PayPanel;
import com.example.pedram.myapplication.Panels.UserPanel;

/**
 * Created by pedram on 6/4/2018.
 */

public class WebAppInterface {

    private Context context;

    public WebAppInterface (Context context) {
        this.context = context;
    }

    @JavascriptInterface
    public void showToast (String toast) {
        /*Intent intent = new Intent(context, UserPanel.class);
        context.startActivity(intent);*/
        Toast.makeText(context, toast, Toast.LENGTH_SHORT).show();
    }
}
