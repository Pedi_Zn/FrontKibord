package com.example.pedram.myapplication.Fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.Panels.LoginPanel;
import com.example.pedram.myapplication.Panels.PayPanel;
import com.example.pedram.myapplication.Panels.UserPanel;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_post.NewPassword;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Change_creditcard extends AppCompatActivity {

    ImageView back;
    TextView change_creditcard;
    EditText  last_creditcard, new_creditcard;
    Button profile_changes;
    Typeface iransans_font;
    private Api api;

    LoginPanel loginPanel = new LoginPanel();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_creditcard);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        back = (ImageView) findViewById(R.id.IV_back);
        change_creditcard = (TextView) findViewById(R.id.TV_change_creditcard);
        new_creditcard = (EditText) findViewById(R.id.ET_new_creditcard);
        profile_changes = (Button) findViewById(R.id.BTN_Profile_Changes);

        iransans_font = Typeface.createFromAsset(getAssets(), "iransans.ttf");

        change_creditcard.setTypeface(iransans_font);
        new_creditcard.setTypeface(iransans_font);
        profile_changes.setTypeface(iransans_font);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(Change_creditcard.this, UserPanel.class);
                startActivity(intent);
            }
        });

        
        profile_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!(trueNumber(new_creditcard.getText().toString(), 24))) {
                    Toast.makeText(Change_creditcard.this, "شماره شبا نا معتبر می باشد", Toast.LENGTH_SHORT).show();
                }
                else {
                    HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                    logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                    OkHttpClient client = new OkHttpClient.Builder()
                            .addInterceptor(logging)
                            .build();


                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Api.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(client)
                            .build();

                    api = retrofit.create(Api.class);
                    NewPassword newPassword = new NewPassword();
                    newPassword.setSheba(new_creditcard.getText().toString());

                    Call<NwPass> call = api.newPassword(newPassword, loginPanel.id);

                    call.enqueue(new Callback<NwPass>() {
                        @Override
                        public void onResponse(Call<NwPass> call, Response<NwPass> response) {
                            if (response.isSuccessful()) {
                                Toast.makeText(Change_creditcard.this, "تغییرات با موفقیت انجام شد", Toast.LENGTH_SHORT).show();
                                finish();
                                Intent intent = new Intent(Change_creditcard.this, UserPanel.class);
                                startActivity(intent);
                            } else
                                Toast.makeText(Change_creditcard.this, "خطا در ارسال اطلاعات", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<NwPass> call, Throwable t) {
                            t.printStackTrace();
                            Log.d("tag", "Ped, message is : " + t.getMessage());
                            Log.d("tag", "Ped, call is : " + call.toString());
                            Toast.makeText(Change_creditcard.this, "خطا در اتصال به سرور", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(Change_creditcard.this, UserPanel.class);
        startActivity(intent);
    }

    public Integer digitsCounter (String s) {
        int counter =0;

        for (int i=0 ; i<s.length() ; i++)
        {
            if (Character.isDigit(s.charAt(i))) {
                counter++;
            }
        }
        return counter;
    }

    public boolean trueNumber (String phonenumber, int definenumber) {
        if (digitsCounter(phonenumber) == definenumber)
            return true;
        else
            return false;
    }
}
