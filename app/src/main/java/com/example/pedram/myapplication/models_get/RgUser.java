package com.example.pedram.myapplication.models_get;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pedram on 3/30/2018.
 */

public class RgUser {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("surname")
    @Expose
    private String surname;
    @SerializedName("virtual_account")
    @Expose
    private Integer virtualAccount;
    @SerializedName("removable_account")
    @Expose
    private Integer removableAccount;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("id")
    @Expose
    private Integer id;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getVirtualAccount() {
        return virtualAccount;
    }

    public void setVirtualAccount(Integer virtualAccount) {
        this.virtualAccount = virtualAccount;
    }

    public Integer getRemovableAccount() {
        return removableAccount;
    }

    public void setRemovableAccount(Integer removableAccount) {
        this.removableAccount = removableAccount;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


}
