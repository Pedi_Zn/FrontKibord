package com.example.pedram.myapplication.Panels;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.CaptivePortal;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.droidbyme.dialoglib.AnimUtils;
import com.droidbyme.dialoglib.DroidDialog;
import com.example.pedram.myapplication.Fragments.BillsFragment;
import com.example.pedram.myapplication.Fragments.Change_creditcard;
import com.example.pedram.myapplication.Fragments.Change_pass;
import com.example.pedram.myapplication.Fragments.PrivatePanel;
import com.example.pedram.myapplication.Fragments.tab1fragment;
import com.example.pedram.myapplication.Fragments.tab2fragment;
import com.example.pedram.myapplication.Fragments.tab3fragment;
import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.Adapter.SectionsPageAdapter;
import com.example.pedram.myapplication.Fragments.Withdrawal;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_get.RgUser;
import com.example.pedram.myapplication.models_post.NewPassword;
import com.example.pedram.myapplication.models_post.User;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserPanel2 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ViewPager mViewPager;
    Typeface iransans_font;
    TextView cash, phonenumber, name;
    Dialog myDialog, CoinDialog;
    ImageView profilePic, coin;
    private Api api;
    private static final String TAG = "UserPanel2" ;
    private SectionsPageAdapter msectionsPageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_panel);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        cash = (TextView) findViewById(R.id.TV_cash);
        coin = (ImageView) findViewById(R.id.IV_coin);
        name = (TextView) headerView.findViewById(R.id.TV_name);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarr);
        profilePic = (ImageView) headerView.findViewById(R.id.IV_profile_picture);

        setSupportActionBar(toolbar);

        iransans_font = Typeface.createFromAsset(getAssets(), "iransans.ttf");

        cash.setTypeface(iransans_font);
        name.setTypeface(iransans_font);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        api = retrofit.create(Api.class);

        final LoginPanel loginPanel = new LoginPanel();
        final NewPassword newPassword = new NewPassword();

        Call<NwPass> call = api.newPassword(newPassword, loginPanel.getId());

        call.enqueue(new Callback<NwPass>() {
            @Override
            public void onResponse(Call<NwPass> call, Response<NwPass> response) {
                if (response.isSuccessful()) {
//                    Toast.makeText(UserPanel.this, loginPanel.access_token, Toast.LENGTH_LONG).show();
//                    cash.setText(response.body().getRemovableAccount().toString());
                    name.setText(response.body().getName().toString() + " " + response.body().getSurname().toString());

                    if (response.body().getFirstLogin().equals("true")) {
                        Intent intent = new Intent(UserPanel2.this, PrivatePanel.class);
                        startActivity(intent);
                    }
                }
                else
                    Toast.makeText(UserPanel2.this, "مشکل در ارسال اطلاعات", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<NwPass> call, Throwable t) {
                t.printStackTrace();
                Log.d("tag", "Ped, message is : " + t.getMessage());
                Log.d("tag", "Ped, call is : " + call.toString());
                Toast.makeText(UserPanel2.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // default fragment for home
        FragmentTransaction ft  = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.flMain, new tab1fragment());
        ft.commit();

        navigationView.setCheckedItem(R.id.nav_pishbini);

        // -----------------------

        myDialog = new Dialog(this);
        CoinDialog = new Dialog(this);

        coin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TextView removable_account, virtual_account, txtclose, vizhe, gheyre_vizhe;
                CoinDialog.setContentView(R.layout.coin);
                removable_account = CoinDialog.findViewById(R.id.removable_account);
                virtual_account = CoinDialog.findViewById(R.id.virtual_account);
                vizhe = CoinDialog.findViewById(R.id.vizhe);
                gheyre_vizhe = CoinDialog.findViewById(R.id.gheyre_vizhe);
                vizhe.setTypeface(iransans_font);
                gheyre_vizhe.setTypeface(iransans_font);
                txtclose = CoinDialog.findViewById(R.id.textclose);
                removable_account.setTypeface(iransans_font);
                virtual_account.setTypeface(iransans_font);

                CoinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                CoinDialog.show();

                LoginPanel loginPanel = new LoginPanel();
                final NewPassword newPassword = new NewPassword();

                Call<NwPass> call2 = api.newPassword(newPassword, loginPanel.getId());

                call2.enqueue(new Callback<NwPass>() {
                    @Override
                    public void onResponse(Call<NwPass> call2, Response<NwPass> response) {
                        if (response.isSuccessful()) {
                            removable_account.setText(String.valueOf(response.body().getRemovableAccount()));
                            virtual_account.setText(String.valueOf(response.body().getVirtualAccount()));
                            vizhe.setText(String.valueOf(response.body().getValueSpecialOption()));
                            gheyre_vizhe.setText(String.valueOf(response.body().getValueExcSpecialOption()));
                        }
                        else
                            Toast.makeText(UserPanel2.this, "مشکل در ارسال اطلاعات", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<NwPass> call2, Throwable t) {
                        t.printStackTrace();
                        Log.d("tag", "Ped, message is : " + t.getMessage());
                        Log.d("tag", "Ped, call is : " + call2.toString());
                        Toast.makeText(UserPanel2.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();
                    }
                });

                txtclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CoinDialog.dismiss();
                    }
                });
            }
        });

        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*final TextView dorost, nadorost, kol, txtclose;
                myDialog.setContentView(R.layout.profile_picture);
                dorost = myDialog.findViewById(R.id.P_dorost);
                nadorost = myDialog.findViewById(R.id.P_nadorost);
                kol = myDialog.findViewById(R.id.P_kol);
                txtclose = myDialog.findViewById(R.id.textclose);

                myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialog.show();

                LoginPanel loginPanel = new LoginPanel();
                final NewPassword newPassword = new NewPassword();

                Call<NwPass> call2 = api.newPassword(newPassword, loginPanel.id, loginPanel.getAccess());

                call2.enqueue(new Callback<NwPass>() {
                    @Override
                    public void onResponse(Call<NwPass> call2, Response<NwPass> response) {
                        /*if (response.isSuccessful()) {
                            dorost.setText(response.body().getVirtualAccount().toString());
                            nadorost.setText(response.body().getName().toString();
                            kol.setText(response.body().getName().toString();
                        }
                    }

                    @Override
                    public void onFailure(Call<NwPass> call2, Throwable t) {
                        t.printStackTrace();
                        Log.d("tag", "Ped, message is : " + t.getMessage());
                        Log.d("tag", "Ped, call is : " + call2.toString());
                        Toast.makeText(UserPanel.this, "Try Again Later", Toast.LENGTH_LONG).show();
                    }
                });

                txtclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myDialog.dismiss();
                    }
                });*/
            }
        });

    }

    /*@Override
    protected void onStart() {
        super.onStart();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
        Log.d(TAG, "onCreate: Starting.");
        msectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        serupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }*/

    @Override
    public void onBackPressed() {
        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new DroidDialog.Builder(UserPanel2.this)
                    .icon(R.drawable.ic_action_close)
                    .title("خروج                                                                                                            ")
                    .content("آیا از خروج خود اطمینان دارید ؟                                    ")
                    .cancelable(true, true)
                    .positiveButton("خروج", new DroidDialog.onPositiveListener() {
                        @Override
                        public void onPositive(Dialog droidDialog) {
                            finish();
                            Intent intent = new Intent(UserPanel2.this, LoginPanel.class);
                            startActivity(intent);
                        }
                    })
                    .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                        @Override
                        public void onNeutral(Dialog droidDialog) {
                            droidDialog.dismiss();
                        }
                    })
                    .animation(AnimUtils.AnimFadeInOut)
                    .color(ContextCompat.getColor(UserPanel2.this, R.color.red), ContextCompat.getColor(UserPanel2.this, R.color.white),
                            ContextCompat.getColor(UserPanel2.this, R.color.red))
                    .divider(true, ContextCompat.getColor(UserPanel2.this, R.color.orange))
                    .show();

        }*/

        finish();
        Intent intent = new Intent(UserPanel2.this, UserPanel.class);
        startActivity(intent);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_pishbini) {
            finish();
            Intent intent = new Intent(UserPanel2.this, UserPanel.class);
            startActivity(intent);
            /*FragmentTransaction ft  = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flMain, new tab1fragment());
            ft.commit();*/
        }
        else if (id == R.id.nav_gozaresh) {
            FragmentTransaction ft  = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flMain, new tab2fragment());
            ft.commit();
        }
        else if (id == R.id.nav_surathesab) {
            FragmentTransaction ft  = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flMain, new BillsFragment());
            ft.commit();
        }
        else if (id == R.id.nav_sharzh) {
            finish();
            Intent intent = new Intent(UserPanel2.this, PayPanel.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_bardasht) {
            finish();
            Intent intent = new Intent(UserPanel2.this, Withdrawal.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_change_pass) {
            finish();
            Intent intent = new Intent(UserPanel2.this, Change_pass.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_change_creditcard) {
            finish();
            Intent intent = new Intent(UserPanel2.this, Change_creditcard.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_info) {
            finish();
            Intent intent = new Intent(UserPanel2.this, GuidePanel.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_telegram) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/joinchat/I2myUUaebUh9Wc2gfdBr4w"));
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

//    private void serupViewPager (ViewPager viewPager) {
//        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
//        adapter.addFragment(new tab1fragment(), "پیش بینی نتایج");
//        adapter.addFragment(new tab2fragment(), "گزارش فعالیت قبلی");
//        viewPager.setAdapter(adapter);
//    }

}
