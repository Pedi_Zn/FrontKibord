package com.example.pedram.myapplication.Panels;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuAdapter;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.droidbyme.dialoglib.AnimUtils;
import com.droidbyme.dialoglib.DroidDialog;
import com.example.pedram.myapplication.Adapter.DataAdapter;
import com.example.pedram.myapplication.Adapter.DataAdapter2;
import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.Matches;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_get.UserMatch;
import com.example.pedram.myapplication.models_get.teamList;
import com.example.pedram.myapplication.models_post.NewPassword;
import com.example.pedram.myapplication.models_post.OptionAnswer;
import com.example.pedram.myapplication.models_post.Options;
import com.example.pedram.myapplication.models_post.OptionsValue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MatchPanel extends AppCompatActivity {

    Typeface iransans_font;
    LinearLayout linearLayout, linearLayout6, linearLayout7, linearLayout8, linearLayout9, linearLayout10, linearLayout11, linearLayout12;
    DataAdapter dataAdapter;
    Dialog CoinDialog;
    private Api api;
    TextView team1_name, team2_name, group;
    TextView factor, factor2, factor3, factor4, factor5, factor6, factor7, factor8, factor9, factor10;
    TextView time, TV_time, textView11, textView12, zarib, title, textView4, textView5, textView6, textView7, textView8, textView9, textView10;
    ImageView image1, image2, image3, image4, image5, image6, image7, image8, image9, image10, back, coin, iv_team1, iv_team2 ;
    EditText value;
    public static int MatchID;
    static Boolean firstTime = true;
    static String  lastW = "NULL";
    static Button button1, button2, button3, button4, button5, button6, button7, button8, button9, button10;
    static CheckBox checkbox1, checkbox2, checkbox3, checkbox4, checkbox5, checkbox6, checkbox7, checkbox8, checkbox9, checkbox10, checkbox11;
    int finalValue = 0;
    static String lastOption = "NULL";
    static int price;
    int sum;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.match_panel);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout2);
        linearLayout6 = (LinearLayout) findViewById(R.id.linearLayout6);
        linearLayout7 = (LinearLayout) findViewById(R.id.linearLayout7);
        linearLayout8 = (LinearLayout) findViewById(R.id.linearLayout8);
        linearLayout9 = (LinearLayout) findViewById(R.id.linearLayout9);
        linearLayout10 = (LinearLayout) findViewById(R.id.linearLayout10);
        linearLayout11 = (LinearLayout) findViewById(R.id.linearLayout11);
        linearLayout12 = (LinearLayout) findViewById(R.id.linearLayout12);


        team1_name = (TextView) findViewById(R.id.tv_team1_name);
        team2_name = (TextView) findViewById(R.id.tv_team2_name);

        value = (EditText) findViewById(R.id.value);
        zarib = (TextView) findViewById(R.id.zarib);
        title = (TextView) findViewById(R.id.title);
        time = (TextView) findViewById(R.id.time);
        TV_time = (TextView) findViewById(R.id.TV_time);
        group = (TextView) findViewById(R.id.group);
        textView4 = (TextView) findViewById(R.id.textView4);
        textView5 = (TextView) findViewById(R.id.textView5);
        textView6 = (TextView) findViewById(R.id.textView6);
        textView7 = (TextView) findViewById(R.id.textView7);
        textView8 = (TextView) findViewById(R.id.textView8);
        textView9 = (TextView) findViewById(R.id.textView9);
        textView10 = (TextView) findViewById(R.id.textView10);
        textView11 = (TextView) findViewById(R.id.textView11);
        textView12 = (TextView) findViewById(R.id.textView12);

        factor = (TextView) findViewById(R.id.textView);
        factor2 = (TextView) findViewById(R.id.textView2);
        factor3 = (TextView) findViewById(R.id.textView3);
        factor4 = (TextView) findViewById(R.id.textView4);
        factor5 = (TextView) findViewById(R.id.textView5);
        factor6 = (TextView) findViewById(R.id.textView6);
        factor7 = (TextView) findViewById(R.id.textView7);
        factor8 = (TextView) findViewById(R.id.textView8);
        factor9 = (TextView) findViewById(R.id.textView9);
        factor10 = (TextView) findViewById(R.id.textView10);

        image1 = (ImageView) findViewById(R.id.image1);
        image2 = (ImageView) findViewById(R.id.image2);
        image3 = (ImageView) findViewById(R.id.image3);
        image4 = (ImageView) findViewById(R.id.image4);
        image5 = (ImageView) findViewById(R.id.image5);
        image6 = (ImageView) findViewById(R.id.image6);
        image7 = (ImageView) findViewById(R.id.image7);
        image8 = (ImageView) findViewById(R.id.image8);
        image9 = (ImageView) findViewById(R.id.image9);
        image10 = (ImageView) findViewById(R.id.image10);

        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);
        button9 = (Button) findViewById(R.id.button9);
        button10 = (Button) findViewById(R.id.button10);

        checkbox1 = (CheckBox) findViewById(R.id.checkbox1);
        checkbox2 = (CheckBox) findViewById(R.id.checkbox2);
        checkbox3 = (CheckBox) findViewById(R.id.checkbox3);
        checkbox4 = (CheckBox) findViewById(R.id.checkbox4);
        checkbox5 = (CheckBox) findViewById(R.id.checkbox5);
        checkbox6 = (CheckBox) findViewById(R.id.checkbox6);
        checkbox7 = (CheckBox) findViewById(R.id.checkbox7);
        checkbox8 = (CheckBox) findViewById(R.id.checkbox8);
        checkbox9 = (CheckBox) findViewById(R.id.checkbox9);
        checkbox10 = (CheckBox) findViewById(R.id.checkbox10);
        checkbox11 = (CheckBox) findViewById(R.id.checkbox11);

        coin = (ImageView) findViewById(R.id.IV_coin);
        iv_team1 = (ImageView) findViewById(R.id.iv_team1);
        iv_team2 = (ImageView) findViewById(R.id.iv_team2);

        image1.setVisibility(View.INVISIBLE);
        image2.setVisibility(View.INVISIBLE);
        image3.setVisibility(View.INVISIBLE);
        image4.setVisibility(View.INVISIBLE);
        image5.setVisibility(View.INVISIBLE);
        image6.setVisibility(View.INVISIBLE);
        image7.setVisibility(View.INVISIBLE);
        image8.setVisibility(View.INVISIBLE);
        image9.setVisibility(View.INVISIBLE);
        image10.setVisibility(View.INVISIBLE);
        checkbox1.setVisibility(View.INVISIBLE);
        checkbox2.setVisibility(View.INVISIBLE);
        checkbox3.setVisibility(View.INVISIBLE);
        checkbox4.setVisibility(View.INVISIBLE);
        checkbox5.setVisibility(View.INVISIBLE);
        checkbox6.setVisibility(View.INVISIBLE);
        checkbox7.setVisibility(View.INVISIBLE);
        checkbox8.setVisibility(View.INVISIBLE);
        checkbox9.setVisibility(View.INVISIBLE);
        checkbox10.setVisibility(View.INVISIBLE);
        checkbox11.setVisibility(View.VISIBLE);
        checkbox11.setChecked(true);

        iransans_font = Typeface.createFromAsset(getAssets(), "iransans.ttf");

        team1_name.setTypeface(iransans_font);
        team2_name.setTypeface(iransans_font);
        value.setTypeface(iransans_font);
        time.setTypeface(iransans_font);
        textView11.setTypeface(iransans_font);
        textView12.setTypeface(iransans_font);
        zarib.setTypeface(iransans_font);
        group.setTypeface(iransans_font);
        title.setTypeface(iransans_font);
        factor.setTypeface(iransans_font);
        factor2.setTypeface(iransans_font);
        factor3.setTypeface(iransans_font);
        factor4.setTypeface(iransans_font);
        factor5.setTypeface(iransans_font);
        factor6.setTypeface(iransans_font);
        factor7.setTypeface(iransans_font);
        factor8.setTypeface(iransans_font);
        factor9.setTypeface(iransans_font);
        factor10.setTypeface(iransans_font);
        button1.setTypeface(iransans_font);
        button2.setTypeface(iransans_font);
        button3.setTypeface(iransans_font);
        button4.setTypeface(iransans_font);
        button5.setTypeface(iransans_font);
        button6.setTypeface(iransans_font);
        button7.setTypeface(iransans_font);
        button8.setTypeface(iransans_font);
        button9.setTypeface(iransans_font);
        button10.setTypeface(iransans_font);


        CoinDialog = new Dialog(this);

        back = (ImageView) findViewById(R.id.IV_back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        coin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TextView removable_account, virtual_account, txtclose, vizhe, gheyre_vizhe;
                CoinDialog.setContentView(R.layout.coin);
                removable_account = CoinDialog.findViewById(R.id.removable_account);
                virtual_account = CoinDialog.findViewById(R.id.virtual_account);
                vizhe = CoinDialog.findViewById(R.id.vizhe);
                gheyre_vizhe = CoinDialog.findViewById(R.id.gheyre_vizhe);
                vizhe.setTypeface(iransans_font);
                gheyre_vizhe.setTypeface(iransans_font);
                removable_account.setTypeface(iransans_font);
                virtual_account.setTypeface(iransans_font);
                txtclose = CoinDialog.findViewById(R.id.textclose);
                CoinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                CoinDialog.show();

                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient client = new OkHttpClient.Builder()
                        .addInterceptor(logging)
                        .build();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Api.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build();

                api = retrofit.create(Api.class);

                LoginPanel loginPanel = new LoginPanel();
                NewPassword newPassword = new NewPassword();

                Call<NwPass> callcoin = api.newPassword(newPassword, loginPanel.id);

                callcoin.enqueue(new Callback<NwPass>() {
                    @Override
                    public void onResponse(Call<NwPass> callcoin, Response<NwPass> response) {
                        if (response.isSuccessful()) {
                            removable_account.setText(String.valueOf(response.body().getRemovableAccount()));
                            virtual_account.setText(String.valueOf(response.body().getVirtualAccount()));
                            vizhe.setText(String.valueOf(response.body().getValueSpecialOption()));
                            gheyre_vizhe.setText(String.valueOf(response.body().getValueExcSpecialOption()));
                        }
                    }

                    @Override
                    public void onFailure(Call<NwPass> callcoin, Throwable t) {
                        t.printStackTrace();
                        Log.d("tag", "Ped, message is : " + t.getMessage());
                        Log.d("tag", "Ped, call is : " + callcoin.toString());
                        Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();
                    }
                });

                txtclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CoinDialog.dismiss();
                    }
                });
            }
        });

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        final Api request = retrofit.create(Api.class);

        final LoginPanel loginPanel = new LoginPanel();
        Call<UserMatch> call0 = request.getOption(loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
        call0.enqueue(new Callback<UserMatch>() {
            @Override
            public void onResponse(Call<UserMatch> call2, final Response<UserMatch> response) {

                if (response.isSuccessful()) {

                    lastW = response.body().getWOrL();
                    lastOption = response.body().getSelectedAnswer();
                    //Toast.makeText(MatchPanel.this, lastOption, Toast.LENGTH_LONG).show();
                    firstTime = false;

                    if (response.body().getSelectedAnswer().equals("A1")) {
//                        button1.setBackgroundResource(R.drawable.btn_radius7);
                        checkbox1.setChecked(true);
                        checkbox1.setVisibility(View.VISIBLE);
                    }
                    if (response.body().getSelectedAnswer().equals("A2")) {
//                        button2.setBackgroundResource(R.drawable.btn_radius7);
                        checkbox2.setChecked(true);
                        checkbox2.setVisibility(View.VISIBLE);
                    }
                    if (response.body().getSelectedAnswer().equals("A3")) {
//                        button3.setBackgroundResource(R.drawable.btn_radius7);
                        checkbox3.setChecked(true);
                        checkbox3.setVisibility(View.VISIBLE);
                    }
                    if (response.body().getSelectedAnswer().equals("A4")) {
//                        button4.setBackgroundResource(R.drawable.btn_radius7);
                        checkbox4.setChecked(true);
                        checkbox4.setVisibility(View.VISIBLE);
                    }
                    if (response.body().getSelectedAnswer().equals("A5")) {
//                        button5.setBackgroundResource(R.drawable.btn_radius7);
                        checkbox5.setChecked(true);
                        checkbox5.setVisibility(View.VISIBLE);
                    }
                    if (response.body().getSelectedAnswer().equals("A6")) {
//                        button6.setBackgroundResource(R.drawable.btn_radius7);
                        checkbox6.setChecked(true);
                        checkbox6.setVisibility(View.VISIBLE);
                    }
                    if (response.body().getSelectedAnswer().equals("A7")) {
//                        button1.setBackgroundResource(R.drawable.btn_radius7);
                        checkbox7.setChecked(true);
                        checkbox7.setVisibility(View.VISIBLE);
                    }
                    if (response.body().getSelectedAnswer().equals("A8")) {
//                        button2.setBackgroundResource(R.drawable.btn_radius7);
                        checkbox8.setChecked(true);
                        checkbox8.setVisibility(View.VISIBLE);
                    }
                    if (response.body().getSelectedAnswer().equals("A9")) {
//                        button3.setBackgroundResource(R.drawable.btn_radius7);
                        checkbox9.setChecked(true);
                        checkbox9.setVisibility(View.VISIBLE);
                    }
                    if (response.body().getSelectedAnswer().equals("A10")) {
//                        button4.setBackgroundResource(R.drawable.btn_radius7);
                        checkbox10.setChecked(true);
                        checkbox10.setVisibility(View.VISIBLE);
                    }
                } else {
                    lastOption = "NULL";
                    lastW = "NULL";
                    firstTime = true;
                    checkbox1.setVisibility(View.INVISIBLE);
                    checkbox2.setVisibility(View.INVISIBLE);
                    checkbox3.setVisibility(View.INVISIBLE);
                    checkbox4.setVisibility(View.INVISIBLE);
                    checkbox5.setVisibility(View.INVISIBLE);
                    checkbox6.setVisibility(View.INVISIBLE);
                    checkbox7.setVisibility(View.INVISIBLE);
                    checkbox8.setVisibility(View.INVISIBLE);
                    checkbox9.setVisibility(View.INVISIBLE);
                    checkbox10.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<UserMatch> call2, Throwable t) {
                Log.d("Error", t.getMessage());
                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();
            }
        });

        Call<Matches> call = request.getMatch(dataAdapter.MatchID, loginPanel.getAccess());
        call.enqueue(new Callback<Matches>() {
            @Override
            public void onResponse(Call<Matches> call, final Response<Matches> responsee) {
                if (responsee.isSuccessful()) {

                    title.setText(responsee.body().getTitle());

                    if (responsee.body().getGroup().toString().equals("NULL"))
                        group.setVisibility(View.GONE);
                    else
                        group.setText(responsee.body().getGroup() + " : گروه ");
                    factor.setText(responsee.body().getFactor_M1().toString());
                    factor2.setText(responsee.body().getFactor_M2().toString());
                    factor3.setText(responsee.body().getFactor_M3().toString());
                    factor4.setText(responsee.body().getFactor_M4().toString());
                    factor5.setText(responsee.body().getFactor_M5().toString());
                    factor6.setText(responsee.body().getFactor_M6().toString());
                    factor7.setText(responsee.body().getFactor_M7().toString());
                    factor8.setText(responsee.body().getFactor_M8().toString());
                    factor9.setText(responsee.body().getFactor_M9().toString());
                    factor10.setText(responsee.body().getFactor_M10().toString());

                    time.setText(responsee.body().getEnd_time());

                    button1.setText(responsee.body().getName_M1());
                    button2.setText(responsee.body().getName_M2());
                    button3.setText(responsee.body().getName_M3());
                    button4.setText(responsee.body().getName_M4());
                    button5.setText(responsee.body().getName_M5());
                    button6.setText(responsee.body().getName_M6());
                    button7.setText(responsee.body().getName_M7());
                    button8.setText(responsee.body().getName_M8());
                    button9.setText(responsee.body().getName_M9());
                    button10.setText(responsee.body().getName_M10());

                    if (responsee.body().getSpecial_option().equals("A1"))
                        image1.setVisibility(View.VISIBLE);
                    if (responsee.body().getSpecial_option().equals("A2"))
                        image2.setVisibility(View.VISIBLE);
                    if (responsee.body().getSpecial_option().equals("A3"))
                        image3.setVisibility(View.VISIBLE);
                    if (responsee.body().getSpecial_option().equals("A4"))
                        image4.setVisibility(View.VISIBLE);
                    if (responsee.body().getSpecial_option().equals("A5"))
                        image5.setVisibility(View.VISIBLE);
                    if (responsee.body().getSpecial_option().equals("A6"))
                        image6.setVisibility(View.VISIBLE);
                    if (responsee.body().getSpecial_option().equals("A7"))
                        image7.setVisibility(View.VISIBLE);
                    if (responsee.body().getSpecial_option().equals("A8"))
                        image8.setVisibility(View.VISIBLE);
                    if (responsee.body().getSpecial_option().equals("A9"))
                        image9.setVisibility(View.VISIBLE);
                    if (responsee.body().getSpecial_option().equals("A10"))
                        image10.setVisibility(View.VISIBLE);

                    Call<teamList> call1 = request.getTeam(responsee.body().getTeam_id(), loginPanel.getAccess());
                    call1.enqueue(new Callback<teamList>() {
                        @Override
                        public void onResponse(Call<teamList> call, Response<teamList> response) {

//                +++++++++++++++++++++++++++++++++
//                TEAM 1 ba TEAM2 jashun bar axe
//                +++++++++++++++++++++++++++++++++

                            if (response.body().getName().equals("3A")) {
                                group.setVisibility(View.VISIBLE);
                                linearLayout.setVisibility(View.GONE);
                                if (responsee.body().getNumber_option().toString().equals("6")) {
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                /*iv_team1.setImageResource(R.drawable.bac);
                                team1_name.setText(responsee.body().getGroup());*/
                            }
                            else if (response.body().getName().equals("3B")) {
                                group.setVisibility(View.GONE);
                                linearLayout.setVisibility(View.GONE);
                                /*iv_team1.setImageResource(R.drawable.bac);
                                team1_name.setText(responsee.body().getGroup());*/
                                if (responsee.body().getNumber_option().toString().equals("3")) {
                                    linearLayout6.setVisibility(View.GONE);
                                    linearLayout7.setVisibility(View.GONE);
                                    linearLayout8.setVisibility(View.GONE);
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView4.setVisibility(View.GONE);
                                    textView5.setVisibility(View.GONE);
                                    textView6.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("4")) {
                                    linearLayout7.setVisibility(View.GONE);
                                    linearLayout8.setVisibility(View.GONE);
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView5.setVisibility(View.GONE);
                                    textView6.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("5")) {
                                    linearLayout8.setVisibility(View.GONE);
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView6.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("6")) {
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("7")) {
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("8")) {
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("9")) {
                                    linearLayout12.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                            }
                            else {
                                group.setVisibility(View.VISIBLE);
                                team1_name.setText(response.body().getName());
                                if (responsee.body().getNumber_option().toString().equals("3")) {
                                    linearLayout6.setVisibility(View.GONE);
                                    linearLayout7.setVisibility(View.GONE);
                                    linearLayout8.setVisibility(View.GONE);
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView4.setVisibility(View.GONE);
                                    textView5.setVisibility(View.GONE);
                                    textView6.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("6")) {
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }

                                Call<ResponseBody> call_team = request.getTeamImage(responsee.body().getTeam_id(), loginPanel.getAccess());

                                call_team.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                                        if (response.isSuccessful()) {
                                            boolean FileDownloaded = DownloadImage(response.body());

                                        }
                                    }

                                    private boolean DownloadImage(ResponseBody body) {
                                        try {
                                            Log.d("DownloadImage", "Reading and writing file");
                                            InputStream in = null;
                                            FileOutputStream out = null;

                                            try {
                                                in = body.byteStream();
                                                out = new FileOutputStream(MatchPanel.this.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                                int c;

                                                while ((c = in.read()) != -1) {
                                                    out.write(c);
                                                }
                                            }
                                            catch (IOException e) {
                                                Log.d("DownloadImage",e.toString());
                                                return false;
                                            }
                                            finally {
                                                if (in != null) {
                                                    in.close();
                                                }
                                                if (out != null) {
                                                    out.close();
                                                }
                                            }

                                            int width, height;

                                            Bitmap bMap = BitmapFactory.decodeFile(MatchPanel.this.getExternalFilesDir(null) + File.separator + "Android.jpg");
                                            width = bMap.getWidth();
                                            height = bMap.getHeight();
                                            Bitmap bMap2 = Bitmap.createScaledBitmap(bMap, width, height, false);
                                            iv_team1.setImageBitmap(bMap2);

                                            return true;

                                        } catch (IOException e) {
                                            Log.d("DownloadImage",e.toString());
                                            return false;
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        t.printStackTrace();
                                        Log.d("tag", "Ped, message is : " + t.getMessage());
                                        Log.d("tag", "Ped, call is : " + call.toString());
                                        Toast.makeText(MatchPanel.this, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                                    }
                                });

                            }

                            /*if (team1_name.getText().equals("روسیه")) {
                                iv_team1.setImageResource(R.drawable.flag_russia);
                            }
                            if (team1_name.getText().equals("برزیل")) {
                                iv_team1.setImageResource(R.drawable.flag_brazil);
                            }
                            if (team1_name.getText().equals("ایران")) {
                                iv_team1.setImageResource(R.drawable.flag_iran);
                            }
                            if (team1_name.getText().equals("ژاپن")) {
                                iv_team1.setImageResource(R.drawable.flag_japan);
                            }
                            if (team1_name.getText().equals("مکزیک")) {
                                iv_team1.setImageResource(R.drawable.flag_mexico);
                            }
                            if (team1_name.getText().equals("بلژیک")) {
                                iv_team1.setImageResource(R.drawable.flag_belgium);
                            }
                            if (team1_name.getText().equals("کره جنوبی")) {
                                iv_team1.setImageResource(R.drawable.flag_korea);
                            }
                            if (team1_name.getText().equals("عربستان")) {
                                iv_team1.setImageResource(R.drawable.flag_arabie);
                            }
                            if (team1_name.getText().equals("آلمان")) {
                                iv_team1.setImageResource(R.drawable.flag_almanya);
                            }
                            if (team1_name.getText().equals("انگلستان")) {
                                iv_team1.setImageResource(R.drawable.flag_england);
                            }
                            if (team1_name.getText().equals("اسپانیا")) {
                                iv_team1.setImageResource(R.drawable.flag_spain);
                            }
                            if (team1_name.getText().equals("نیجریه")) {
                                iv_team1.setImageResource(R.drawable.flag_nigeria);
                            }
                            if (team1_name.getText().equals("کاستاریکا")) {
                                iv_team1.setImageResource(R.drawable.flag_costarica);
                            }
                            if (team1_name.getText().equals("لهستان")) {
                                iv_team1.setImageResource(R.drawable.flag_polska);
                            }
                            if (team1_name.getText().equals("مصر")) {
                                iv_team1.setImageResource(R.drawable.flag_egypt);
                            }
                            if (team1_name.getText().equals("ایسلند")) {
                                iv_team1.setImageResource(R.drawable.flag_iceland);
                            }
                            if (team1_name.getText().equals("صربستان")) {
                                iv_team1.setImageResource(R.drawable.flag_serbia);
                            }
                            if (team1_name.getText().equals("پرتغال")) {
                                iv_team1.setImageResource(R.drawable.flag_portugal);
                            }
                            if (team1_name.getText().equals("فرانسه")) {
                                iv_team1.setImageResource(R.drawable.flag_france);
                            }
                            if (team1_name.getText().equals("اروگوئه")) {
                                iv_team1.setImageResource(R.drawable.flag_uruguay);
                            }
                            if (team1_name.getText().equals("آرژانتین")) {
                                iv_team1.setImageResource(R.drawable.flag_argentina);
                            }
                            if (team1_name.getText().equals("کلمبیا")) {
                                iv_team1.setImageResource(R.drawable.flag_colombia);
                            }
                            if (team1_name.getText().equals("پاناما")) {
                                iv_team1.setImageResource(R.drawable.flag_panama);
                            }
                            if (team1_name.getText().equals("سنگال")) {
                                iv_team1.setImageResource(R.drawable.flag_senegal);
                            }
                            if (team1_name.getText().equals("مراکش")) {
                                iv_team1.setImageResource(R.drawable.flag_morocco);
                            }
                            if (team1_name.getText().equals("تونس")) {
                                iv_team1.setImageResource(R.drawable.flag_tunisia);
                            }
                            if (team1_name.getText().equals("سوئیس")) {
                                iv_team1.setImageResource(R.drawable.flag_swiss);
                            }
                            if (team1_name.getText().equals("کرواسی")) {
                                iv_team1.setImageResource(R.drawable.flag_croatia);
                            }
                            if (team1_name.getText().equals("سوئد")) {
                                iv_team1.setImageResource(R.drawable.flag_sweden);
                            }
                            if (team1_name.getText().equals("دانمارک")) {
                                iv_team1.setImageResource(R.drawable.flag_denmark);
                            }
                            if (team1_name.getText().equals("استرالیا")) {
                                iv_team1.setImageResource(R.drawable.flag_australia);
                            }
                            if (team1_name.getText().equals("پرو")) {
                                iv_team1.setImageResource(R.drawable.flag_peru);
                            }*/
                        }

                        @Override
                        public void onFailure(Call<teamList> call, Throwable t) {
                            Log.d("Error", t.getMessage());
                            Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();
                        }
                    });

                    Call<teamList> call2 = request.getTeam(responsee.body().getTeam2_id(), loginPanel.getAccess());
                    call2.enqueue(new Callback<teamList>() {
                        @Override
                        public void onResponse(Call<teamList> call2, final Response<teamList> response) {

//                +++++++++++++++++++++++++++++++++
//                TEAM 1 ba TEAM2 jashun bar axe
//                +++++++++++++++++++++++++++++++++
                            if (response.body().getName().equals("3A")) {
                                group.setVisibility(View.VISIBLE);
                                linearLayout.setVisibility(View.GONE);
                                /*iv_team2.setImageResource(R.drawable.first_back);
                                team2_name.setText("A - H");*/

                                if (responsee.body().getNumber_option().toString().equals("6")) {
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }

                            } else if (response.body().getName().equals("3B")) {
                                group.setVisibility(View.GONE);
                                linearLayout.setVisibility(View.GONE);
                                /*iv_team1.setImageResource(R.drawable.bac);
                                team1_name.setText(responsee.body().getGroup());*/

                                if (responsee.body().getNumber_option().toString().equals("3")) {
                                    linearLayout6.setVisibility(View.GONE);
                                    linearLayout7.setVisibility(View.GONE);
                                    linearLayout8.setVisibility(View.GONE);
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView4.setVisibility(View.GONE);
                                    textView5.setVisibility(View.GONE);
                                    textView6.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("4")) {
                                    linearLayout7.setVisibility(View.GONE);
                                    linearLayout8.setVisibility(View.GONE);
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView5.setVisibility(View.GONE);
                                    textView6.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("5")) {
                                    linearLayout8.setVisibility(View.GONE);
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView6.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("6")) {
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("7")) {
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("8")) {
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("9")) {
                                    linearLayout12.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                            }
                            else {
                                group.setVisibility(View.VISIBLE);
                                team2_name.setText(response.body().getName());
                                if (responsee.body().getNumber_option().toString().equals("3")) {
                                    linearLayout6.setVisibility(View.GONE);
                                    linearLayout7.setVisibility(View.GONE);
                                    linearLayout8.setVisibility(View.GONE);
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView4.setVisibility(View.GONE);
                                    textView5.setVisibility(View.GONE);
                                    textView6.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }
                                if (responsee.body().getNumber_option().toString().equals("6")) {
                                    linearLayout9.setVisibility(View.GONE);
                                    linearLayout10.setVisibility(View.GONE);
                                    linearLayout11.setVisibility(View.GONE);
                                    linearLayout12.setVisibility(View.GONE);
                                    textView7.setVisibility(View.GONE);
                                    textView8.setVisibility(View.GONE);
                                    textView9.setVisibility(View.GONE);
                                    textView10.setVisibility(View.GONE);
                                }

                                Call<ResponseBody> call_team2 = request.getTeamImage(responsee.body().getTeam2_id(), loginPanel.getAccess());

                                call_team2.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                                        if (response.isSuccessful()) {
                                            boolean FileDownloaded = DownloadImage(response.body());

                                        }
                                    }

                                    private boolean DownloadImage(ResponseBody body) {
                                        try {
                                            Log.d("DownloadImage", "Reading and writing file");
                                            InputStream in = null;
                                            FileOutputStream out = null;

                                            try {
                                                in = body.byteStream();
                                                out = new FileOutputStream(getExternalFilesDir(null) + File.separator + "Android.jpg");
                                                int c;

                                                while ((c = in.read()) != -1) {
                                                    out.write(c);
                                                }
                                            }
                                            catch (IOException e) {
                                                Log.d("DownloadImage",e.toString());
                                                return false;
                                            }
                                            finally {
                                                if (in != null) {
                                                    in.close();
                                                }
                                                if (out != null) {
                                                    out.close();
                                                }
                                            }

                                            int width, height;

                                            Bitmap bMap = BitmapFactory.decodeFile(getExternalFilesDir(null) + File.separator + "Android.jpg");
                                            width = bMap.getWidth();
                                            height = bMap.getHeight();
                                            Bitmap bMap2 = Bitmap.createScaledBitmap(bMap, width, height, false);
                                            iv_team2.setImageBitmap(bMap2);

                                            return true;

                                        } catch (IOException e) {
                                            Log.d("DownloadImage",e.toString());
                                            return false;
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        t.printStackTrace();
                                        Log.d("tag", "Ped, message is : " + t.getMessage());
                                        Log.d("tag", "Ped, call is : " + call.toString());
                                        Toast.makeText(MatchPanel.this, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            /*if (team2_name.getText().equals("روسیه")) {
                                iv_team2.setImageResource(R.drawable.flag_russia);
                            }
                            if (team2_name.getText().equals("برزیل")) {
                                iv_team2.setImageResource(R.drawable.flag_brazil);
                            }
                            if (team2_name.getText().equals("ایران")) {
                                iv_team2.setImageResource(R.drawable.flag_iran);
                            }
                            if (team2_name.getText().equals("ژاپن")) {
                                iv_team2.setImageResource(R.drawable.flag_japan);
                            }
                            if (team2_name.getText().equals("مکزیک")) {
                                iv_team2.setImageResource(R.drawable.flag_mexico);
                            }
                            if (team2_name.getText().equals("بلژیک")) {
                                iv_team2.setImageResource(R.drawable.flag_belgium);
                            }
                            if (team2_name.getText().equals("کره جنوبی")) {
                                iv_team2.setImageResource(R.drawable.flag_korea);
                            }
                            if (team2_name.getText().equals("عربستان")) {
                                iv_team2.setImageResource(R.drawable.flag_arabie);
                            }
                            if (team2_name.getText().equals("آلمان")) {
                                iv_team2.setImageResource(R.drawable.flag_almanya);
                            }
                            if (team2_name.getText().equals("انگلستان")) {
                                iv_team2.setImageResource(R.drawable.flag_england);
                            }
                            if (team2_name.getText().equals("اسپانیا")) {
                                iv_team2.setImageResource(R.drawable.flag_spain);
                            }
                            if (team2_name.getText().equals("نیجریه")) {
                                iv_team2.setImageResource(R.drawable.flag_nigeria);
                            }
                            if (team2_name.getText().equals("کاستاریکا")) {
                                iv_team2.setImageResource(R.drawable.flag_costarica);
                            }
                            if (team2_name.getText().equals("لهستان")) {
                                iv_team2.setImageResource(R.drawable.flag_polska);
                            }
                            if (team2_name.getText().equals("مصر")) {
                                iv_team2.setImageResource(R.drawable.flag_egypt);
                            }
                            if (team2_name.getText().equals("ایسلند")) {
                                iv_team2.setImageResource(R.drawable.flag_iceland);
                            }
                            if (team2_name.getText().equals("صربستان")) {
                                iv_team2.setImageResource(R.drawable.flag_serbia);
                            }
                            if (team2_name.getText().equals("پرتغال")) {
                                iv_team2.setImageResource(R.drawable.flag_portugal);
                            }
                            if (team2_name.getText().equals("فرانسه")) {
                                iv_team2.setImageResource(R.drawable.flag_france);
                            }
                            if (team2_name.getText().equals("اروگوئه")) {
                                iv_team2.setImageResource(R.drawable.flag_uruguay);
                            }
                            if (team2_name.getText().equals("آرژانتین")) {
                                iv_team2.setImageResource(R.drawable.flag_argentina);
                            }
                            if (team2_name.getText().equals("کلمبیا")) {
                                iv_team2.setImageResource(R.drawable.flag_colombia);
                            }
                            if (team2_name.getText().equals("پاناما")) {
                                iv_team2.setImageResource(R.drawable.flag_panama);
                            }
                            if (team2_name.getText().equals("سنگال")) {
                                iv_team2.setImageResource(R.drawable.flag_senegal);
                            }
                            if (team2_name.getText().equals("مراکش")) {
                                iv_team2.setImageResource(R.drawable.flag_morocco);
                            }
                            if (team2_name.getText().equals("تونس")) {
                                iv_team2.setImageResource(R.drawable.flag_tunisia);
                            }
                            if (team2_name.getText().equals("سوئیس")) {
                                iv_team2.setImageResource(R.drawable.flag_swiss);
                            }
                            if (team2_name.getText().equals("کرواسی")) {
                                iv_team2.setImageResource(R.drawable.flag_croatia);
                            }
                            if (team2_name.getText().equals("سوئد")) {
                                iv_team2.setImageResource(R.drawable.flag_sweden);
                            }
                            if (team2_name.getText().equals("دانمارک")) {
                                iv_team2.setImageResource(R.drawable.flag_denmark);
                            }
                            if (team2_name.getText().equals("استرالیا")) {
                                iv_team2.setImageResource(R.drawable.flag_australia);
                            }
                            if (team2_name.getText().equals("پرو")) {
                                iv_team2.setImageResource(R.drawable.flag_peru);
                            }*/
                        }

                        @Override
                        public void onFailure(Call<teamList> call2, Throwable t) {
                            Log.d("Error", t.getMessage());
                            Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                        }
                    });
                }



            }

            @Override
            public void onFailure(Call<Matches> call, Throwable t) {
                Log.d("Error", t.getMessage());
                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

            }
        });

        Call<UserMatch> callPrice = request.getOption(loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
        callPrice.enqueue(new Callback<UserMatch>() {
            @Override
            public void onResponse(Call<UserMatch> call2, final Response<UserMatch> response) {
                if (response.isSuccessful()) {
                    price = response.body().getValue();
                }
                else {
                    price = 0;
                }
            }

            @Override
            public void onFailure(Call<UserMatch> call2, Throwable t) {
                Log.d("Error", t.getMessage());
                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (value.getText().toString().equals("")) {
                    finalValue = 0;
                    Toast.makeText(MatchPanel.this, "مبلغ را وارد کنید", Toast.LENGTH_LONG).show();
                }
                else
                    if (Integer.parseInt(value.getText().toString()) < 10000 && Integer.parseInt(value.getText().toString()) >= 0)
                        Toast.makeText(MatchPanel.this, "کمتر از 10000 تومان نمی توانید ببندید", Toast.LENGTH_LONG).show();
                else {
                    int sum;
                    sum = price + Integer.parseInt(value.getText().toString());
                    new DroidDialog.Builder(MatchPanel.this)
                                .icon(R.drawable.ic_action_tick)
                                .title("گزینه 1                                                                  ")
                                .content(" آیا مایلید " + sum + "  تومان روی گزینه 1 ببندید ؟                         ")
                                .cancelable(true, true)
                                .positiveButton("بله", new DroidDialog.onPositiveListener() {
                                    @Override
                                    public void onPositive(Dialog droidDialog) {
                                        droidDialog.dismiss();
                                        if (firstTime) {
                                            lastOption = "A1";
                                            finalValue = Integer.parseInt(value.getText().toString());
                                            Options options = new Options();
                                            options.setValue(finalValue);
                                            options.setAX("A1");
                                            final LoginPanel loginPanel = new LoginPanel();
                                            Call<teamList> call = request.setOption(options, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                            call.enqueue(new Callback<teamList>() {
                                                @Override
                                                public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                    if (response.code() == 200)
                                                        Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                    else
                                                        if (response.code() == 400)
                                                            Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                    else
                                                        if (response.code() == 401)
                                                            Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                    else
                                                        if (response.code() == 402)
                                                            Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                    else
                                                        if (response.code() == 422)
                                                            Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                    else
                                                        if (response.isSuccessful()) {
                                                            checkbox1.setChecked(true);
                                                            checkbox1.setVisibility(View.VISIBLE);
                                                            checkbox2.setChecked(false);
                                                            checkbox3.setChecked(false);
                                                            checkbox4.setChecked(false);
                                                            checkbox5.setChecked(false);
                                                            checkbox6.setChecked(false);
                                                            checkbox7.setChecked(false);
                                                            checkbox8.setChecked(false);
                                                            checkbox9.setChecked(false);
                                                            checkbox10.setChecked(false);
                                                    /*button1.setBackgroundResource(R.drawable.btn_radius7);
                                                    button2.setBackgroundResource(R.drawable.btn_radius2);
                                                    button3.setBackgroundResource(R.drawable.btn_radius2);
                                                    button4.setBackgroundResource(R.drawable.btn_radius2);
                                                    button5.setBackgroundResource(R.drawable.btn_radius2);
                                                    button6.setBackgroundResource(R.drawable.btn_radius2);*/

                                                            finish();
                                                            Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                            Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                            startActivity(intent);
                                                        }
                                                }

                                                @Override
                                                public void onFailure(Call<teamList> call2, Throwable t) {
                                                    Log.d("Error", t.getMessage());
                                                    Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                                }
                                            });
                                        }

                                        /*else
                                            if ((!firstTime) && lastW != "NULL") {
                                                lastOption = "A1";
                                                finalValue = Integer.parseInt(value.getText().toString());
                                                Options options = new Options();
                                                options.setValue(finalValue);
                                                options.setAX("A1");
                                                final LoginPanel loginPanel = new LoginPanel();
                                                Call<teamList> call = request.setOption(options, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                                call.enqueue(new Callback<teamList>() {
                                                    @Override
                                                    public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                        if (response.code() == 200)
                                                            Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                        else
                                                        if (response.code() == 400)
                                                            Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                        else
                                                        if (response.code() == 401)
                                                            Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                        else
                                                        if (response.code() == 402)
                                                            Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                        else
                                                        if (response.code() == 422)
                                                            Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                        else
                                                        if (response.isSuccessful()) {
                                                            checkbox1.setChecked(true);
                                                            checkbox1.setVisibility(View.VISIBLE);
                                                            checkbox2.setChecked(false);
                                                            checkbox3.setChecked(false);
                                                            checkbox4.setChecked(false);
                                                            checkbox5.setChecked(false);
                                                            checkbox6.setChecked(false);
                                                            checkbox7.setChecked(false);
                                                            checkbox8.setChecked(false);
                                                            checkbox9.setChecked(false);
                                                            checkbox10.setChecked(false);
                                                    /*button1.setBackgroundResource(R.drawable.btn_radius7);
                                                    button2.setBackgroundResource(R.drawable.btn_radius2);
                                                    button3.setBackgroundResource(R.drawable.btn_radius2);
                                                    button4.setBackgroundResource(R.drawable.btn_radius2);
                                                    button5.setBackgroundResource(R.drawable.btn_radius2);
                                                    button6.setBackgroundResource(R.drawable.btn_radius2);

                                                            finish();
                                                            Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                            Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                            startActivity(intent);
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<teamList> call2, Throwable t) {
                                                        Log.d("Error", t.getMessage());
                                                        Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                                    }
                                                });
                                            }*/
                                        else {

                                            finalValue = Integer.parseInt(value.getText().toString());
                                            OptionsValue optionsValue = new OptionsValue();
                                            optionsValue.setValue(finalValue);
                                            optionsValue.setAX("A1");
                                            final LoginPanel loginPanel = new LoginPanel();
                                            Call<teamList> callvalue = request.setValue(optionsValue, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                            callvalue.enqueue(new Callback<teamList>() {
                                                @Override
                                                public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                    if (response.code() == 200)
                                                        Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                    else
                                                        if (response.code() == 400)
                                                        Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                    else
                                                        if (response.code() == 401)
                                                        Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                    else
                                                        if (response.code() == 402)
                                                            Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                    else
                                                        if (response.code() == 422)
                                                        Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();
                                                    else
                                                        if (response.isSuccessful()) {

                                                            OptionAnswer optionAnswer = new OptionAnswer();
                                                            optionAnswer.setAX("A1");
                                                            optionAnswer.setValue(finalValue);
                                                            final LoginPanel lognPanel = new LoginPanel();
                                                            Call<teamList> callanswer = request.setAnswer(optionAnswer, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                                            callanswer.enqueue(new Callback<teamList>() {
                                                                @Override
                                                                public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                                    if (response.code() == 200)
                                                                        Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                                    else
                                                                    if (response.code() == 400)
                                                                        Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                                    else
                                                                    if (response.code() == 401)
                                                                        Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                                    else
                                                                    if (response.code() == 402)
                                                                        Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                                    else
                                                                    if (response.code() == 422)
                                                                        Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();
                                                                    else
                                                                    if (response.isSuccessful()) {
                                                                        checkbox1.setChecked(true);
                                                                        checkbox1.setVisibility(View.VISIBLE);
                                                                        checkbox2.setChecked(false);
                                                                        checkbox3.setChecked(false);
                                                                        checkbox4.setChecked(false);
                                                                        checkbox5.setChecked(false);
                                                                        checkbox6.setChecked(false);
                                                                        checkbox7.setChecked(false);
                                                                        checkbox8.setChecked(false);
                                                                        checkbox9.setChecked(false);
                                                                        checkbox10.setChecked(false);

                                                            /*button1.setBackgroundResource(R.drawable.btn_radius7);
                                                            button2.setBackgroundResource(R.drawable.btn_radius2);
                                                            button3.setBackgroundResource(R.drawable.btn_radius2);
                                                            button4.setBackgroundResource(R.drawable.btn_radius2);
                                                            button5.setBackgroundResource(R.drawable.btn_radius2);
                                                            button6.setBackgroundResource(R.drawable.btn_radius2);*/

                                                                        finish();
                                                                        Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                                        Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                                        startActivity(intent);

                                                                    }

                                                                }

                                                                @Override
                                                                public void onFailure(Call<teamList> call2, Throwable t) {
                                                                    Log.d("Error", t.getMessage());
                                                                    Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();
                                                                }
                                                            });

                                                        }

                                                }

                                                @Override
                                                public void onFailure(Call<teamList> call2, Throwable t) {
                                                    Log.d("Error", t.getMessage());
                                                    Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                                }
                                            });

                                        }
                                    }
                                })
                                .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                                    @Override
                                    public void onNeutral(Dialog droidDialog) {
                                        droidDialog.dismiss();
                                    }
                                })
                                .animation(AnimUtils.AnimFadeInOut)
                                .color(ContextCompat.getColor(MatchPanel.this, R.color.green), ContextCompat.getColor(MatchPanel.this, R.color.white),
                                        ContextCompat.getColor(MatchPanel.this, R.color.green))
                                .divider(true, ContextCompat.getColor(MatchPanel.this, R.color.blue))
                                .show();
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (value.getText().toString().equals("")) {
                    finalValue = 0;
                    Toast.makeText(MatchPanel.this, "مبلغ را وارد کنید", Toast.LENGTH_LONG).show();
                }
                else
                    if (Integer.parseInt(value.getText().toString()) < 10000 && Integer.parseInt(value.getText().toString()) >= 0)
                        Toast.makeText(MatchPanel.this, "کمتر از 10000 تومان نمی توانید ببندید", Toast.LENGTH_LONG).show();
                else {
                    sum = price + Integer.parseInt(value.getText().toString());
                    new DroidDialog.Builder(MatchPanel.this)
                            .icon(R.drawable.ic_action_tick)
                            .title("گزینه 2                                                                  ")
                            .content(" آیا مایلید " + sum + "  تومان روی گزینه 2 ببندید ؟                         ")
                            .cancelable(true, true)
                            .positiveButton("بله", new DroidDialog.onPositiveListener() {
                                @Override
                                public void onPositive(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                    if (firstTime) {
                                        lastOption = "A2";
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        Options options = new Options();
                                        options.setValue(finalValue);
                                        options.setAX("A2");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> call = request.setOption(options, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        call.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                    if (response.isSuccessful()) {
                                                        checkbox1.setChecked(false);
                                                        checkbox2.setChecked(true);
                                                        checkbox2.setVisibility(View.VISIBLE);
                                                        checkbox3.setChecked(false);
                                                        checkbox4.setChecked(false);
                                                        checkbox5.setChecked(false);
                                                        checkbox6.setChecked(false);
                                                        checkbox7.setChecked(false);
                                                        checkbox8.setChecked(false);
                                                        checkbox9.setChecked(false);
                                                        checkbox10.setChecked(false);
                                                        finish();
                                                        Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                        Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                        startActivity(intent);
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });
                                    }
                                    else {
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        OptionsValue optionsValue = new OptionsValue();
                                        optionsValue.setValue(finalValue);
                                        optionsValue.setAX("A2");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> callvalue = request.setValue(optionsValue, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        callvalue.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                    if (response.isSuccessful()) {
                                                        OptionAnswer optionAnswer = new OptionAnswer();
                                                        optionAnswer.setAX("A2");
                                                        optionAnswer.setValue(finalValue);
                                                        final LoginPanel lognPanel = new LoginPanel();
                                                        Call<teamList> callanswer = request.setAnswer(optionAnswer, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                                        callanswer.enqueue(new Callback<teamList>() {
                                                            @Override
                                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                                if (response.code() == 200)
                                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 400)
                                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 401)
                                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 402)
                                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 422)
                                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                                else
                                                                if (response.isSuccessful()) {
                                                                    checkbox1.setChecked(false);
                                                                    checkbox2.setChecked(true);
                                                                    checkbox2.setVisibility(View.VISIBLE);
                                                                    checkbox3.setChecked(false);
                                                                    checkbox4.setChecked(false);
                                                                    checkbox5.setChecked(false);
                                                                    checkbox6.setChecked(false);
                                                                    checkbox7.setChecked(false);
                                                                    checkbox8.setChecked(false);
                                                                    checkbox9.setChecked(false);
                                                                    checkbox10.setChecked(false);
                                                                    finish();
                                                                    Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                                    Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                                    startActivity(intent);
                                                                }

                                                            }

                                                            @Override
                                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                                Log.d("Error", t.getMessage());
                                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                                            }
                                                        });
                                                    }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });
                                    }
                                }
                            })
                            .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                                @Override
                                public void onNeutral(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                }
                            })
                            .animation(AnimUtils.AnimFadeInOut)
                            .color(ContextCompat.getColor(MatchPanel.this, R.color.green), ContextCompat.getColor(MatchPanel.this, R.color.white),
                                    ContextCompat.getColor(MatchPanel.this, R.color.green))
                            .divider(true, ContextCompat.getColor(MatchPanel.this, R.color.blue))
                            .show();
                }
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (value.getText().toString().equals("")) {
                    finalValue = 0;
                    Toast.makeText(MatchPanel.this, "مبلغ را وارد کنید", Toast.LENGTH_LONG).show();
                }
                else
                    if (Integer.parseInt(value.getText().toString()) < 10000 && Integer.parseInt(value.getText().toString()) >= 0)
                        Toast.makeText(MatchPanel.this, "کمتر از 10000 تومان نمی توانید ببندید", Toast.LENGTH_LONG).show();
                else {
                    sum = price + Integer.parseInt(value.getText().toString());
                    new DroidDialog.Builder(MatchPanel.this)
                            .icon(R.drawable.ic_action_tick)
                            .title("گزینه 3                                                                  ")
                            .content(" آیا مایلید " + sum + "  تومان روی گزینه 3 ببندید ؟                         ")
                            .cancelable(true, true)
                            .positiveButton("بله", new DroidDialog.onPositiveListener() {
                                @Override
                                public void onPositive(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                    if (firstTime) {
                                        lastOption = "A3";
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        Options options = new Options();
                                        options.setValue(finalValue);
                                        options.setAX("A3");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> call = request.setOption(options, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        call.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                    if (response.isSuccessful()) {
                                                        checkbox1.setChecked(false);
                                                        checkbox2.setChecked(false);
                                                        checkbox3.setChecked(true);
                                                        checkbox3.setVisibility(View.VISIBLE);
                                                        checkbox4.setChecked(false);
                                                        checkbox5.setChecked(false);
                                                        checkbox6.setChecked(false);
                                                        checkbox7.setChecked(false);
                                                        checkbox8.setChecked(false);
                                                        checkbox9.setChecked(false);
                                                        checkbox10.setChecked(false);
                                                        finish();
                                                        Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                        Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                        startActivity(intent);
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });
                                    }
                                    else {
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        OptionsValue optionsValue = new OptionsValue();
                                        optionsValue.setValue(finalValue);
                                        optionsValue.setAX("A3");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> callvalue = request.setValue(optionsValue, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        callvalue.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                    if (response.isSuccessful()) {
                                                        OptionAnswer optionAnswer = new OptionAnswer();
                                                        optionAnswer.setAX("A3");
                                                        optionAnswer.setValue(finalValue);
                                                        final LoginPanel lognPanel = new LoginPanel();
                                                        Call<teamList> callanswer = request.setAnswer(optionAnswer, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                                        callanswer.enqueue(new Callback<teamList>() {
                                                            @Override
                                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                                if (response.code() == 200)
                                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 400)
                                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 401)
                                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 402)
                                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 422)
                                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.isSuccessful()) {
                                                                    checkbox1.setChecked(false);
                                                                    checkbox2.setChecked(false);
                                                                    checkbox3.setChecked(true);
                                                                    checkbox3.setVisibility(View.VISIBLE);
                                                                    checkbox4.setChecked(false);
                                                                    checkbox5.setChecked(false);
                                                                    checkbox6.setChecked(false);
                                                                    checkbox7.setChecked(false);
                                                                    checkbox8.setChecked(false);
                                                                    checkbox9.setChecked(false);
                                                                    checkbox10.setChecked(false);
                                                                    finish();
                                                                    Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                                    Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                                    startActivity(intent);
                                                                }

                                                            }

                                                            @Override
                                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                                Log.d("Error", t.getMessage());
                                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                                            }
                                                        });
                                                    }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });

                                    }
                                }
                            })
                            .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                                @Override
                                public void onNeutral(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                }
                            })
                            .animation(AnimUtils.AnimFadeInOut)
                            .color(ContextCompat.getColor(MatchPanel.this, R.color.green), ContextCompat.getColor(MatchPanel.this, R.color.white),
                                    ContextCompat.getColor(MatchPanel.this, R.color.green))
                            .divider(true, ContextCompat.getColor(MatchPanel.this, R.color.blue))
                            .show();
                }
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (value.getText().toString().equals("")) {
                    finalValue = 0;
                    Toast.makeText(MatchPanel.this, "مبلغ را وارد کنید", Toast.LENGTH_LONG).show();
                }
                else
                    if (Integer.parseInt(value.getText().toString()) < 10000 && Integer.parseInt(value.getText().toString()) >= 0)
                        Toast.makeText(MatchPanel.this, "کمتر از 10000 تومان نمی توانید ببندید", Toast.LENGTH_LONG).show();
                else {
                    sum = price + Integer.parseInt(value.getText().toString());
                    new DroidDialog.Builder(MatchPanel.this)
                            .icon(R.drawable.ic_action_tick)
                            .title("گزینه 4                                                                  ")
                            .content(" آیا مایلید " + sum + "  تومان روی گزینه 4 ببندید ؟                         ")
                            .cancelable(true, true)
                            .positiveButton("بله", new DroidDialog.onPositiveListener() {
                                @Override
                                public void onPositive(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                    if (firstTime) {
                                        lastOption = "A4";
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        Options options = new Options();
                                        options.setValue(finalValue);
                                        options.setAX("A4");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> call = request.setOption(options, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        call.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                    if (response.isSuccessful()) {
                                                        checkbox1.setChecked(false);
                                                        checkbox2.setChecked(false);
                                                        checkbox3.setChecked(false);
                                                        checkbox4.setChecked(true);
                                                        checkbox4.setVisibility(View.VISIBLE);
                                                        checkbox5.setChecked(false);
                                                        checkbox6.setChecked(false);
                                                        checkbox7.setChecked(false);
                                                        checkbox8.setChecked(false);
                                                        checkbox9.setChecked(false);
                                                        checkbox10.setChecked(false);
                                                        finish();
                                                        Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                        Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                        startActivity(intent);
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                    else {
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        OptionsValue optionsValue = new OptionsValue();
                                        optionsValue.setValue(finalValue);
                                        optionsValue.setAX("A4");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> callvalue = request.setValue(optionsValue, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        callvalue.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                    if (response.isSuccessful()) {
                                                        OptionAnswer optionAnswer = new OptionAnswer();
                                                        optionAnswer.setAX("A4");
                                                        optionAnswer.setValue(finalValue);
                                                        LoginPanel lognPanel = new LoginPanel();
                                                        Call<teamList> callanswer = request.setAnswer(optionAnswer, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                                        callanswer.enqueue(new Callback<teamList>() {
                                                            @Override
                                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                                if (response.code() == 200)
                                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 400)
                                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 401)
                                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 402)
                                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 422)
                                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                                else
                                                                if (response.isSuccessful()) {
                                                                    checkbox1.setChecked(false);
                                                                    checkbox2.setChecked(false);
                                                                    checkbox3.setChecked(false);
                                                                    checkbox4.setChecked(true);
                                                                    checkbox4.setVisibility(View.VISIBLE);
                                                                    checkbox5.setChecked(false);
                                                                    checkbox6.setChecked(false);
                                                                    checkbox7.setChecked(false);
                                                                    checkbox8.setChecked(false);
                                                                    checkbox9.setChecked(false);
                                                                    checkbox10.setChecked(false);
                                                                    finish();
                                                                    Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                                    Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                                    startActivity(intent);
                                                                }

                                                            }

                                                            @Override
                                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                                Log.d("Error", t.getMessage());
                                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                                            }
                                                        });
                                                    }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });

                                    }
                                }
                            })
                            .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                                @Override
                                public void onNeutral(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                }
                            })
                            .animation(AnimUtils.AnimFadeInOut)
                            .color(ContextCompat.getColor(MatchPanel.this, R.color.green), ContextCompat.getColor(MatchPanel.this, R.color.white),
                                    ContextCompat.getColor(MatchPanel.this, R.color.green))
                            .divider(true, ContextCompat.getColor(MatchPanel.this, R.color.blue))
                            .show();
                }
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (value.getText().toString().equals("")) {
                    finalValue = 0;
                    Toast.makeText(MatchPanel.this, "مبلغ را وارد کنید", Toast.LENGTH_LONG).show();
                }
                else
                    if (Integer.parseInt(value.getText().toString()) < 10000 && Integer.parseInt(value.getText().toString()) >= 0)
                        Toast.makeText(MatchPanel.this, "کمتر از 10000 تومان نمی توانید ببندید", Toast.LENGTH_LONG).show();
                else {
                    sum = price + Integer.parseInt(value.getText().toString());
                    new DroidDialog.Builder(MatchPanel.this)
                            .icon(R.drawable.ic_action_tick)
                            .title("گزینه 5                                                                  ")
                            .content(" آیا مایلید " + sum + "  تومان روی گزینه 5 ببندید ؟                         ")
                            .cancelable(true, true)
                            .positiveButton("بله", new DroidDialog.onPositiveListener() {
                                @Override
                                public void onPositive(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                    if (firstTime) {
                                        lastOption = "A5";
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        Options options = new Options();
                                        options.setValue(finalValue);
                                        options.setAX("A5");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> call = request.setOption(options, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        call.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                    if (response.isSuccessful()) {
                                                        checkbox1.setChecked(false);
                                                        checkbox2.setChecked(false);
                                                        checkbox3.setChecked(false);
                                                        checkbox4.setChecked(false);
                                                        checkbox5.setChecked(true);
                                                        checkbox5.setVisibility(View.VISIBLE);
                                                        checkbox6.setChecked(false);
                                                        checkbox7.setChecked(false);
                                                        checkbox8.setChecked(false);
                                                        checkbox9.setChecked(false);
                                                        checkbox10.setChecked(false);
                                                        finish();
                                                        Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                        Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                        startActivity(intent);
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });
                                    }
                                    else {
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        OptionsValue optionsValue = new OptionsValue();
                                        optionsValue.setValue(finalValue);
                                        optionsValue.setAX("A5");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> callvalue = request.setValue(optionsValue, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        callvalue.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                    if (response.isSuccessful()) {
                                                        OptionAnswer optionAnswer = new OptionAnswer();
                                                        optionAnswer.setValue(finalValue);
                                                        optionAnswer.setAX("A5");
                                                        LoginPanel lognPanel = new LoginPanel();
                                                        Call<teamList> callanswer = request.setAnswer(optionAnswer, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                                        callanswer.enqueue(new Callback<teamList>() {
                                                            @Override
                                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                                if (response.code() == 200)
                                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 400)
                                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 401)
                                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 402)
                                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 422)
                                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                                else
                                                                if (response.isSuccessful()) {
                                                                    checkbox1.setChecked(false);
                                                                    checkbox2.setChecked(false);
                                                                    checkbox3.setChecked(false);
                                                                    checkbox4.setChecked(false);
                                                                    checkbox5.setChecked(true);
                                                                    checkbox5.setVisibility(View.VISIBLE);
                                                                    checkbox6.setChecked(false);
                                                                    checkbox7.setChecked(false);
                                                                    checkbox8.setChecked(false);
                                                                    checkbox9.setChecked(false);
                                                                    checkbox10.setChecked(false);
                                                                    finish();
                                                                    Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                                    Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                                    startActivity(intent);
                                                                }

                                                            }

                                                            @Override
                                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                                Log.d("Error", t.getMessage());
                                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();


                                                            }
                                                        });
                                                    }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });

                                    }
                                }
                            })
                            .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                                @Override
                                public void onNeutral(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                }
                            })
                            .animation(AnimUtils.AnimFadeInOut)
                            .color(ContextCompat.getColor(MatchPanel.this, R.color.green), ContextCompat.getColor(MatchPanel.this, R.color.white),
                                    ContextCompat.getColor(MatchPanel.this, R.color.green))
                            .divider(true, ContextCompat.getColor(MatchPanel.this, R.color.blue))
                            .show();
                }
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (value.getText().toString().equals("")) {
                    finalValue = 0;
                    Toast.makeText(MatchPanel.this, "مبلغ را وارد کنید", Toast.LENGTH_LONG).show();
                }
                else
                    if (Integer.parseInt(value.getText().toString()) < 10000 && Integer.parseInt(value.getText().toString()) >= 0)
                        Toast.makeText(MatchPanel.this, "کمتر از 10000 تومان نمی توانید ببندید", Toast.LENGTH_LONG).show();
                else {
                    sum = price + Integer.parseInt(value.getText().toString());
                    new DroidDialog.Builder(MatchPanel.this)
                            .icon(R.drawable.ic_action_tick)
                            .title("گزینه 6                                                                  ")
                            .content(" آیا مایلید " + sum + "  تومان روی گزینه 6 ببندید ؟                         ")
                            .cancelable(true, true)
                            .positiveButton("بله", new DroidDialog.onPositiveListener() {
                                @Override
                                public void onPositive(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                    if (firstTime) {
                                        lastOption = "A6";
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        Options options = new Options();
                                        options.setValue(finalValue);
                                        options.setAX("A6");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> call = request.setOption(options, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        call.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                    if (response.isSuccessful()) {
                                                        checkbox1.setChecked(false);
                                                        checkbox2.setChecked(false);
                                                        checkbox3.setChecked(false);
                                                        checkbox4.setChecked(false);
                                                        checkbox5.setChecked(false);
                                                        checkbox6.setChecked(true);
                                                        checkbox6.setVisibility(View.VISIBLE);
                                                        checkbox7.setChecked(false);
                                                        checkbox8.setChecked(false);
                                                        checkbox9.setChecked(false);
                                                        checkbox10.setChecked(false);
                                                        Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                        finish();
                                                        Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                        startActivity(intent);
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });
                                    }
                                    else {
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        OptionsValue optionsValue = new OptionsValue();
                                        optionsValue.setValue(finalValue);
                                        optionsValue.setAX("A6");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> callvalue = request.setValue(optionsValue, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        callvalue.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                    if (response.isSuccessful()) {
                                                        OptionAnswer optionAnswer = new OptionAnswer();
                                                        optionAnswer.setValue(finalValue);
                                                        optionAnswer.setAX("A6");
                                                        LoginPanel lognPanel = new LoginPanel();
                                                        Call<teamList> callanswer = request.setAnswer(optionAnswer, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                                        callanswer.enqueue(new Callback<teamList>() {
                                                            @Override
                                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                                if (response.code() == 200)
                                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 400)
                                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 401)
                                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 402)
                                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                                else
                                                                if (response.code() == 422)
                                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                                else
                                                                if (response.isSuccessful()) {
                                                                    checkbox1.setChecked(false);
                                                                    checkbox2.setChecked(false);
                                                                    checkbox3.setChecked(false);
                                                                    checkbox4.setChecked(false);
                                                                    checkbox5.setChecked(false);
                                                                    checkbox6.setChecked(true);
                                                                    checkbox6.setVisibility(View.VISIBLE);
                                                                    checkbox7.setChecked(false);
                                                                    checkbox8.setChecked(false);
                                                                    checkbox9.setChecked(false);
                                                                    checkbox10.setChecked(false);
                                                                    Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                                    finish();
                                                                    Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                                    startActivity(intent);
                                                                }

                                                            }

                                                            @Override
                                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                                Log.d("Error", t.getMessage());
                                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                                            }
                                                        });
                                                    }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });

                                    }
                                }
                            })
                            .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                                @Override
                                public void onNeutral(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                }
                            })
                            .animation(AnimUtils.AnimFadeInOut)
                            .color(ContextCompat.getColor(MatchPanel.this, R.color.green), ContextCompat.getColor(MatchPanel.this, R.color.white),
                                    ContextCompat.getColor(MatchPanel.this, R.color.green))
                            .divider(true, ContextCompat.getColor(MatchPanel.this, R.color.blue))
                            .show();
                }
            }
        });
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (value.getText().toString().equals("")) {
                    finalValue = 0;
                    Toast.makeText(MatchPanel.this, "مبلغ را وارد کنید", Toast.LENGTH_LONG).show();
                }
                else
                if (Integer.parseInt(value.getText().toString()) < 10000 && Integer.parseInt(value.getText().toString()) >= 0)
                    Toast.makeText(MatchPanel.this, "کمتر از 10000 تومان نمی توانید ببندید", Toast.LENGTH_LONG).show();
                else {
                    sum = price + Integer.parseInt(value.getText().toString());
                    new DroidDialog.Builder(MatchPanel.this)
                            .icon(R.drawable.ic_action_tick)
                            .title("گزینه 7                                                                  ")
                            .content(" آیا مایلید " + sum + "  تومان روی گزینه 7 ببندید ؟                         ")
                            .cancelable(true, true)
                            .positiveButton("بله", new DroidDialog.onPositiveListener() {
                                @Override
                                public void onPositive(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                    if (firstTime) {
                                        lastOption = "A7";
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        Options options = new Options();
                                        options.setValue(finalValue);
                                        options.setAX("A7");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> call = request.setOption(options, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        call.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                if (response.isSuccessful()) {
                                                    checkbox1.setChecked(false);
                                                    checkbox2.setChecked(false);
                                                    checkbox3.setChecked(false);
                                                    checkbox4.setChecked(false);
                                                    checkbox5.setChecked(false);
                                                    checkbox6.setChecked(false);
                                                    checkbox7.setChecked(true);
                                                    checkbox7.setVisibility(View.VISIBLE);
                                                    checkbox8.setChecked(false);
                                                    checkbox9.setChecked(false);
                                                    checkbox10.setChecked(false);
                                                    Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                    finish();
                                                    Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                    startActivity(intent);
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });
                                    }
                                    else {
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        OptionsValue optionsValue = new OptionsValue();
                                        optionsValue.setValue(finalValue);
                                        optionsValue.setAX("A7");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> callvalue = request.setValue(optionsValue, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        callvalue.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                if (response.isSuccessful()) {
                                                    OptionAnswer optionAnswer = new OptionAnswer();
                                                    optionAnswer.setValue(finalValue);
                                                    optionAnswer.setAX("A7");
                                                    LoginPanel lognPanel = new LoginPanel();
                                                    Call<teamList> callanswer = request.setAnswer(optionAnswer, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                                    callanswer.enqueue(new Callback<teamList>() {
                                                        @Override
                                                        public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                            if (response.code() == 200)
                                                                Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 400)
                                                                Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 401)
                                                                Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 402)
                                                                Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 422)
                                                                Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                            else
                                                            if (response.isSuccessful()) {
                                                                checkbox1.setChecked(false);
                                                                checkbox2.setChecked(false);
                                                                checkbox3.setChecked(false);
                                                                checkbox4.setChecked(false);
                                                                checkbox5.setChecked(false);
                                                                checkbox6.setChecked(false);
                                                                checkbox7.setChecked(true);
                                                                checkbox7.setVisibility(View.VISIBLE);
                                                                checkbox8.setChecked(false);
                                                                checkbox9.setChecked(false);
                                                                checkbox10.setChecked(false);
                                                                Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                                finish();
                                                                Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                                startActivity(intent);
                                                            }

                                                        }

                                                        @Override
                                                        public void onFailure(Call<teamList> call2, Throwable t) {
                                                            Log.d("Error", t.getMessage());
                                                            Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                                        }
                                                    });
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });

                                    }
                                }
                            })
                            .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                                @Override
                                public void onNeutral(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                }
                            })
                            .animation(AnimUtils.AnimFadeInOut)
                            .color(ContextCompat.getColor(MatchPanel.this, R.color.green), ContextCompat.getColor(MatchPanel.this, R.color.white),
                                    ContextCompat.getColor(MatchPanel.this, R.color.green))
                            .divider(true, ContextCompat.getColor(MatchPanel.this, R.color.blue))
                            .show();
                }
            }
        });

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (value.getText().toString().equals("")) {
                    finalValue = 0;
                    Toast.makeText(MatchPanel.this, "مبلغ را وارد کنید", Toast.LENGTH_LONG).show();
                }
                else
                if (Integer.parseInt(value.getText().toString()) < 10000 && Integer.parseInt(value.getText().toString()) >= 0)
                    Toast.makeText(MatchPanel.this, "کمتر از 10000 تومان نمی توانید ببندید", Toast.LENGTH_LONG).show();
                else {
                    sum = price + Integer.parseInt(value.getText().toString());
                    new DroidDialog.Builder(MatchPanel.this)
                            .icon(R.drawable.ic_action_tick)
                            .title("گزینه 8                                                                  ")
                            .content(" آیا مایلید " + sum + "  تومان روی گزینه 8 ببندید ؟                         ")
                            .cancelable(true, true)
                            .positiveButton("بله", new DroidDialog.onPositiveListener() {
                                @Override
                                public void onPositive(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                    if (firstTime) {
                                        lastOption = "A8";
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        Options options = new Options();
                                        options.setValue(finalValue);
                                        options.setAX("A8");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> call = request.setOption(options, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        call.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                if (response.isSuccessful()) {
                                                    checkbox1.setChecked(false);
                                                    checkbox2.setChecked(false);
                                                    checkbox3.setChecked(false);
                                                    checkbox4.setChecked(false);
                                                    checkbox5.setChecked(false);
                                                    checkbox6.setChecked(false);
                                                    checkbox7.setChecked(false);
                                                    checkbox8.setChecked(true);
                                                    checkbox8.setVisibility(View.VISIBLE);
                                                    checkbox9.setChecked(false);
                                                    checkbox10.setChecked(false);
                                                    Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                    finish();
                                                    Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                    startActivity(intent);
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });
                                    }
                                    else {
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        OptionsValue optionsValue = new OptionsValue();
                                        optionsValue.setValue(finalValue);
                                        optionsValue.setAX("A8");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> callvalue = request.setValue(optionsValue, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        callvalue.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                if (response.isSuccessful()) {
                                                    OptionAnswer optionAnswer = new OptionAnswer();
                                                    optionAnswer.setValue(finalValue);
                                                    optionAnswer.setAX("A8");
                                                    LoginPanel lognPanel = new LoginPanel();
                                                    Call<teamList> callanswer = request.setAnswer(optionAnswer, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                                    callanswer.enqueue(new Callback<teamList>() {
                                                        @Override
                                                        public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                            if (response.code() == 200)
                                                                Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 400)
                                                                Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 401)
                                                                Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 402)
                                                                Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 422)
                                                                Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                            else
                                                            if (response.isSuccessful()) {
                                                                checkbox1.setChecked(false);
                                                                checkbox2.setChecked(false);
                                                                checkbox3.setChecked(false);
                                                                checkbox4.setChecked(false);
                                                                checkbox5.setChecked(false);
                                                                checkbox6.setChecked(false);
                                                                checkbox7.setChecked(false);
                                                                checkbox8.setVisibility(View.VISIBLE);
                                                                checkbox8.setChecked(true);
                                                                checkbox9.setChecked(false);
                                                                checkbox10.setChecked(false);
                                                                Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                                finish();
                                                                Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                                startActivity(intent);
                                                            }

                                                        }

                                                        @Override
                                                        public void onFailure(Call<teamList> call2, Throwable t) {
                                                            Log.d("Error", t.getMessage());
                                                            Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                                        }
                                                    });
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });

                                    }
                                }
                            })
                            .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                                @Override
                                public void onNeutral(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                }
                            })
                            .animation(AnimUtils.AnimFadeInOut)
                            .color(ContextCompat.getColor(MatchPanel.this, R.color.green), ContextCompat.getColor(MatchPanel.this, R.color.white),
                                    ContextCompat.getColor(MatchPanel.this, R.color.green))
                            .divider(true, ContextCompat.getColor(MatchPanel.this, R.color.blue))
                            .show();
                }
            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (value.getText().toString().equals("")) {
                    finalValue = 0;
                    Toast.makeText(MatchPanel.this, "مبلغ را وارد کنید", Toast.LENGTH_LONG).show();
                }
                else
                if (Integer.parseInt(value.getText().toString()) < 10000 && Integer.parseInt(value.getText().toString()) >= 0)
                    Toast.makeText(MatchPanel.this, "کمتر از 10000 تومان نمی توانید ببندید", Toast.LENGTH_LONG).show();
                else {
                    sum = price + Integer.parseInt(value.getText().toString());
                    new DroidDialog.Builder(MatchPanel.this)
                            .icon(R.drawable.ic_action_tick)
                            .title("گزینه 9                                                                  ")
                            .content(" آیا مایلید " + sum + "  تومان روی گزینه 9 ببندید ؟                         ")
                            .cancelable(true, true)
                            .positiveButton("بله", new DroidDialog.onPositiveListener() {
                                @Override
                                public void onPositive(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                    if (firstTime) {
                                        lastOption = "A9";
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        Options options = new Options();
                                        options.setValue(finalValue);
                                        options.setAX("A9");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> call = request.setOption(options, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        call.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                if (response.isSuccessful()) {
                                                    checkbox1.setChecked(false);
                                                    checkbox2.setChecked(false);
                                                    checkbox3.setChecked(false);
                                                    checkbox4.setChecked(false);
                                                    checkbox5.setChecked(false);
                                                    checkbox6.setChecked(false);
                                                    checkbox7.setChecked(false);
                                                    checkbox8.setChecked(false);
                                                    checkbox9.setVisibility(View.VISIBLE);
                                                    checkbox9.setChecked(true);
                                                    checkbox10.setChecked(false);
                                                    Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                    finish();
                                                    Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                    startActivity(intent);
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });
                                    }
                                    else {
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        OptionsValue optionsValue = new OptionsValue();
                                        optionsValue.setValue(finalValue);
                                        optionsValue.setAX("A9");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> callvalue = request.setValue(optionsValue, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        callvalue.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                if (response.isSuccessful()) {
                                                    OptionAnswer optionAnswer = new OptionAnswer();
                                                    optionAnswer.setValue(finalValue);
                                                    optionAnswer.setAX("A9");
                                                    LoginPanel lognPanel = new LoginPanel();
                                                    Call<teamList> callanswer = request.setAnswer(optionAnswer, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                                    callanswer.enqueue(new Callback<teamList>() {
                                                        @Override
                                                        public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                            if (response.code() == 200)
                                                                Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 400)
                                                                Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 401)
                                                                Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 402)
                                                                Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 422)
                                                                Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                            else
                                                            if (response.isSuccessful()) {
                                                                checkbox1.setChecked(false);
                                                                checkbox2.setChecked(false);
                                                                checkbox3.setChecked(false);
                                                                checkbox4.setChecked(false);
                                                                checkbox5.setChecked(false);
                                                                checkbox6.setChecked(false);
                                                                checkbox7.setChecked(false);
                                                                checkbox8.setChecked(false);
                                                                checkbox9.setVisibility(View.VISIBLE);
                                                                checkbox9.setChecked(true);
                                                                checkbox10.setChecked(false);
                                                                Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                                finish();
                                                                Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                                startActivity(intent);
                                                            }

                                                        }

                                                        @Override
                                                        public void onFailure(Call<teamList> call2, Throwable t) {
                                                            Log.d("Error", t.getMessage());
                                                            Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                                        }
                                                    });
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });

                                    }
                                }
                            })
                            .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                                @Override
                                public void onNeutral(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                }
                            })
                            .animation(AnimUtils.AnimFadeInOut)
                            .color(ContextCompat.getColor(MatchPanel.this, R.color.green), ContextCompat.getColor(MatchPanel.this, R.color.white),
                                    ContextCompat.getColor(MatchPanel.this, R.color.green))
                            .divider(true, ContextCompat.getColor(MatchPanel.this, R.color.blue))
                            .show();
                }
            }
        });
        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (value.getText().toString().equals("")) {
                    finalValue = 0;
                    Toast.makeText(MatchPanel.this, "مبلغ را وارد کنید", Toast.LENGTH_LONG).show();
                }
                else
                if (Integer.parseInt(value.getText().toString()) < 10000 && Integer.parseInt(value.getText().toString()) >= 0)
                    Toast.makeText(MatchPanel.this, "کمتر از 10000 تومان نمی توانید ببندید", Toast.LENGTH_LONG).show();
                else {
                    sum = price + Integer.parseInt(value.getText().toString());
                    new DroidDialog.Builder(MatchPanel.this)
                            .icon(R.drawable.ic_action_tick)
                            .title("گزینه 10                                                                  ")
                            .content(" آیا مایلید " + sum + "  تومان روی گزینه 10 ببندید ؟                         ")
                            .cancelable(true, true)
                            .positiveButton("بله", new DroidDialog.onPositiveListener() {
                                @Override
                                public void onPositive(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                    if (firstTime) {
                                        lastOption = "A10";
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        Options options = new Options();
                                        options.setValue(finalValue);
                                        options.setAX("A10");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> call = request.setOption(options, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        call.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                if (response.isSuccessful()) {
                                                    checkbox1.setChecked(false);
                                                    checkbox2.setChecked(false);
                                                    checkbox3.setChecked(false);
                                                    checkbox4.setChecked(false);
                                                    checkbox5.setChecked(false);
                                                    checkbox6.setChecked(false);
                                                    checkbox7.setChecked(false);
                                                    checkbox8.setChecked(false);
                                                    checkbox9.setChecked(false);
                                                    checkbox10.setVisibility(View.VISIBLE);
                                                    checkbox10.setChecked(true);
                                                    Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                    finish();
                                                    Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                    startActivity(intent);
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });
                                    }
                                    else {
                                        finalValue = Integer.parseInt(value.getText().toString());
                                        OptionsValue optionsValue = new OptionsValue();
                                        optionsValue.setValue(finalValue);
                                        optionsValue.setAX("A10");
                                        final LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> callvalue = request.setValue(optionsValue, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                        callvalue.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.code() == 200)
                                                    Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 400)
                                                    Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 401)
                                                    Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 402)
                                                    Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                else
                                                if (response.code() == 422)
                                                    Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                else
                                                if (response.isSuccessful()) {
                                                    OptionAnswer optionAnswer = new OptionAnswer();
                                                    optionAnswer.setValue(finalValue);
                                                    optionAnswer.setAX("A10");
                                                    LoginPanel lognPanel = new LoginPanel();
                                                    Call<teamList> callanswer = request.setAnswer(optionAnswer, loginPanel.getId(), dataAdapter.MatchID, loginPanel.getAccess());
                                                    callanswer.enqueue(new Callback<teamList>() {
                                                        @Override
                                                        public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                            if (response.code() == 200)
                                                                Toast.makeText(MatchPanel.this, "بازی به پایان رسیده", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 400)
                                                                Toast.makeText(MatchPanel.this, "موجودی حساب شما کافی نیست لطفا حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 401)
                                                                Toast.makeText(MatchPanel.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 402)
                                                                Toast.makeText(MatchPanel.this, "مبلغ نباید برابر 0 باشد", Toast.LENGTH_LONG).show();
                                                            else
                                                            if (response.code() == 422)
                                                                Toast.makeText(MatchPanel.this, "گزینه خود را به گزینه ویژه نمی توانید تغییر بدهید", Toast.LENGTH_LONG).show();

                                                            else
                                                            if (response.isSuccessful()) {
                                                                checkbox1.setChecked(false);
                                                                checkbox2.setChecked(false);
                                                                checkbox3.setChecked(false);
                                                                checkbox4.setChecked(false);
                                                                checkbox5.setChecked(false);
                                                                checkbox6.setChecked(false);
                                                                checkbox7.setChecked(false);
                                                                checkbox8.setChecked(false);
                                                                checkbox9.setChecked(false);
                                                                checkbox10.setVisibility(View.VISIBLE);
                                                                checkbox10.setChecked(true);
                                                                Toast.makeText(MatchPanel.this, "انتخاب شما با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                                                                finish();
                                                                Intent intent = new Intent(MatchPanel.this, UserPanel.class);
                                                                startActivity(intent);
                                                            }

                                                        }

                                                        @Override
                                                        public void onFailure(Call<teamList> call2, Throwable t) {
                                                            Log.d("Error", t.getMessage());
                                                            Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                                        }
                                                    });
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(MatchPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });

                                    }
                                }
                            })
                            .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                                @Override
                                public void onNeutral(Dialog droidDialog) {
                                    droidDialog.dismiss();
                                }
                            })
                            .animation(AnimUtils.AnimFadeInOut)
                            .color(ContextCompat.getColor(MatchPanel.this, R.color.green), ContextCompat.getColor(MatchPanel.this, R.color.white),
                                    ContextCompat.getColor(MatchPanel.this, R.color.green))
                            .divider(true, ContextCompat.getColor(MatchPanel.this, R.color.blue))
                            .show();
                }
            }
        });
    }
}
