package com.example.pedram.myapplication.Panels;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.droidbyme.dialoglib.AnimUtils;
import com.droidbyme.dialoglib.DroidDialog;
import com.example.pedram.myapplication.Fragments.Change_pass;
import com.example.pedram.myapplication.Fragments.PrivatePanel;
import com.example.pedram.myapplication.Fragments.Withdrawal;
import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.Interface.WebAppInterface;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_get.teamList;
import com.example.pedram.myapplication.models_post.NewPassword;
import com.example.pedram.myapplication.models_post.OptionAnswer;
import com.example.pedram.myapplication.models_post.Options;
import com.example.pedram.myapplication.models_post.OptionsValue;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class PayPanel extends AppCompatActivity {

    Dialog CoinDialog;
    Typeface sahar_font, iransans_font;
    Button bt1, bt2, bt3, pay, cash_deduction, backto;
    EditText your_choice, creditcard;
    TextView etebar, etebar_number;
    ImageView back, coin;
    private Api api;
    TextView credit;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_panel);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        your_choice = (EditText) findViewById(R.id.ET_yourchoise);
        pay = (Button) findViewById(R.id.BTN_pay);
        bt1 = (Button) findViewById(R.id.BTN_1);
        bt2 = (Button) findViewById(R.id.BTN_2);
        bt3 = (Button) findViewById(R.id.BTN_3);
        credit = (TextView) findViewById(R.id.TV_etebar_number);
        cash_deduction = (Button) findViewById(R.id.BTN_cash_deduction);
        etebar = (TextView) findViewById(R.id.TV_etebar);
        etebar_number = (TextView) findViewById(R.id.TV_etebar_number);
        back = (ImageView) findViewById(R.id.IV_back);
        coin = (ImageView) findViewById(R.id.IV_coin);

        sahar_font = Typeface.createFromAsset(getAssets(), "sahar.ttf");
        iransans_font = Typeface.createFromAsset(getAssets(), "iransans.ttf");

        etebar_number.setTypeface(sahar_font);
        etebar.setTypeface(iransans_font);
        your_choice.setTypeface(iransans_font);
        credit.setTypeface(iransans_font);
        pay.setTypeface(iransans_font);
        bt1.setTypeface(sahar_font);
        bt2.setTypeface(sahar_font);
        bt3.setTypeface(sahar_font);

        CoinDialog = new Dialog(this);

        coin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TextView removable_account, virtual_account, txtclose, vizhe, gheyre_vizhe;
                CoinDialog.setContentView(R.layout.coin);
                removable_account = CoinDialog.findViewById(R.id.removable_account);
                virtual_account = CoinDialog.findViewById(R.id.virtual_account);
                vizhe = CoinDialog.findViewById(R.id.vizhe);
                gheyre_vizhe = CoinDialog.findViewById(R.id.gheyre_vizhe);
                vizhe.setTypeface(iransans_font);
                gheyre_vizhe.setTypeface(iransans_font);
                removable_account.setTypeface(iransans_font);
                virtual_account.setTypeface(iransans_font);

                txtclose = CoinDialog.findViewById(R.id.textclose);
                CoinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                CoinDialog.show();

                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient client = new OkHttpClient.Builder()
                        .addInterceptor(logging)
                        .build();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Api.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build();

                api = retrofit.create(Api.class);

                LoginPanel loginPanel = new LoginPanel();
                NewPassword newPassword = new NewPassword();

                Call<NwPass> callcoin = api.newPassword(newPassword, loginPanel.id);

                callcoin.enqueue(new Callback<NwPass>() {
                    @Override
                    public void onResponse(Call<NwPass> callcoin, Response<NwPass> response) {
                        if (response.isSuccessful()) {
                            removable_account.setText(String.valueOf(response.body().getRemovableAccount()));
                            virtual_account.setText(String.valueOf(response.body().getVirtualAccount()));
                            vizhe.setText(String.valueOf(response.body().getValueSpecialOption()));
                            gheyre_vizhe.setText(String.valueOf(response.body().getValueExcSpecialOption()));
                        }
                        else
                            Toast.makeText(PayPanel.this, "مشکل در ارسال اطلاعات", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<NwPass> callcoin, Throwable t) {
                        t.printStackTrace();
                        Log.d("tag", "Ped, message is : " + t.getMessage());
                        Log.d("tag", "Ped, call is : " + callcoin.toString());
                        Toast.makeText(PayPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();
                    }
                });

                txtclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CoinDialog.dismiss();
                    }
                });
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(PayPanel.this, UserPanel.class);
                startActivity(intent);
            }
        });

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                your_choice.setText("100000");
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                your_choice.setText("50000");
            }
        });
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                your_choice.setText("20000");
            }
        });

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        api = retrofit.create(Api.class);

        LoginPanel loginPanel = new LoginPanel();
        final NewPassword newPassword = new NewPassword();

        Call<NwPass> call = api.newPassword(newPassword, loginPanel.id);

        call.enqueue(new Callback<NwPass>() {
            @Override
            public void onResponse(Call<NwPass> call, Response<NwPass> response) {
                if (response.isSuccessful()) {
                    credit.setText(response.body().getRemovableAccount().toString() + " تومان ");
                }
                else
                    Toast.makeText(PayPanel.this, "مشکل در ارسال اطلاعات", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<NwPass> call, Throwable t) {
                t.printStackTrace();
                Log.d("tag", "Ped, message is : " + t.getMessage());
                Log.d("tag", "Ped, call is : " + call.toString());
                Toast.makeText(PayPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();
            }
        });

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (your_choice.getText().toString().equals(""))
                    Toast.makeText(PayPanel.this, "لطفا مبلغ را وارد کنید", Toast.LENGTH_LONG).show();
                else
                    if (Integer.parseInt(your_choice.getText().toString()) < 10000)
                        Toast.makeText(PayPanel.this, "نمی توانید کمتر از 10000 تومان حساب خود را شارژ کنید", Toast.LENGTH_LONG).show();
                    else {
                        new DroidDialog.Builder(PayPanel.this)
                                .icon(R.drawable.ic_action_tick)
                                .title("شارژ حساب                                                                  ")
                                .content(" آیا مایلید " + your_choice.getText().toString() + "  تومان حساب خود راشارژ کنید ؟                         ")
                                .cancelable(true, true)
                                .positiveButton("بله", new DroidDialog.onPositiveListener() {
                                    @Override
                                    public void onPositive(final Dialog droidDialog) {
                                        Options options = new Options();
                                        options.setValue(Integer.parseInt(your_choice.getText().toString()));
                                        LoginPanel loginPanel = new LoginPanel();
                                        Call<teamList> call = api.addValue1(options, loginPanel.getId(), loginPanel.getAccess());
                                        call.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call2, final Response<teamList> response) {
                                                if (response.isSuccessful()) {

//                                                    Toast.makeText(PayPanel.this, "حساب شما با موفقیت شارژ شد", Toast.LENGTH_LONG).show();
//                                                    finish();
                                                    Intent intent = new Intent(PayPanel.this, UserPanel.class);
                                                    startActivity(intent);

                                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                                            Uri.parse("https://www.zarinpal.com/pg/StartPay/"+ response.body().getAuthority()));
                                                    startActivity(browserIntent);

                                                } else
                                                    Toast.makeText(PayPanel.this, "مشکل در ارسال اطلاعات", Toast.LENGTH_LONG).show();
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call2, Throwable t) {
                                                Log.d("Error", t.getMessage());
                                                Toast.makeText(PayPanel.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();

                                            }
                                        });

                                    }
                                })
                                .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                                    @Override
                                    public void onNeutral(Dialog droidDialog) {
                                        droidDialog.dismiss();
                                    }
                                })
                                .animation(AnimUtils.AnimFadeInOut)
                                .color(ContextCompat.getColor(PayPanel.this, R.color.green), ContextCompat.getColor(PayPanel.this, R.color.white),
                                        ContextCompat.getColor(PayPanel.this, R.color.green))
                                .divider(true, ContextCompat.getColor(PayPanel.this, R.color.blue))
                                .show();
                    }

            }
        });

    }
    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(PayPanel.this, UserPanel.class);
        startActivity(intent);
    }
}
