package com.example.pedram.myapplication.Fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.droidbyme.dialoglib.AnimUtils;
import com.droidbyme.dialoglib.DroidDialog;
import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.Panels.LoginPanel;
import com.example.pedram.myapplication.Panels.MatchPanel;
import com.example.pedram.myapplication.Panels.PayPanel;
import com.example.pedram.myapplication.Panels.UserPanel;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.DbUser;
import com.example.pedram.myapplication.models_get.Guide;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_get.teamList;
import com.example.pedram.myapplication.models_post.Login;
import com.example.pedram.myapplication.models_post.NewPassword;
import com.example.pedram.myapplication.models_post.Options;

import dmax.dialog.SpotsDialog;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Withdrawal extends AppCompatActivity {

    Dialog CoinDialog;
    Typeface sahar_font, iransans_font;
    ImageView back, coin;
    Button cash_deduction;
    EditText your_choice;
    private Api api;
    TextView credit, creditcard, bardasht, baghimande, TV_bardasht, TV_baghimande;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.withdrawal);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        back = (ImageView) findViewById(R.id.IV_back);
        coin = (ImageView) findViewById(R.id.IV_coin);
        credit = (TextView) findViewById(R.id.TV_etebar_number);
        creditcard = (TextView) findViewById(R.id.TV_creditcard);
        bardasht = (TextView) findViewById(R.id.bardasht);
        baghimande = (TextView) findViewById(R.id.baghimande);
        TV_bardasht = (TextView) findViewById(R.id.TV_bardasht);
        TV_baghimande = (TextView) findViewById(R.id.TV_baghimande);
        cash_deduction = (Button) findViewById(R.id.BTN_cash_deduction);
        your_choice = (EditText) findViewById(R.id.ET_yourchoise);

        iransans_font = Typeface.createFromAsset(getAssets(), "iransans.ttf");
        credit.setTypeface(iransans_font);
        creditcard.setTypeface(iransans_font);
        bardasht.setTypeface(iransans_font);
        baghimande.setTypeface(iransans_font);
        TV_baghimande.setTypeface(iransans_font);
        TV_bardasht.setTypeface(iransans_font);


        CoinDialog = new Dialog(this);

        coin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TextView removable_account, virtual_account, txtclose, vizhe, gheyre_vizhe;
                CoinDialog.setContentView(R.layout.coin);
                removable_account = CoinDialog.findViewById(R.id.removable_account);
                virtual_account = CoinDialog.findViewById(R.id.virtual_account);
                vizhe = CoinDialog.findViewById(R.id.vizhe);
                gheyre_vizhe = CoinDialog.findViewById(R.id.gheyre_vizhe);
                vizhe.setTypeface(iransans_font);
                gheyre_vizhe.setTypeface(iransans_font);
                removable_account.setTypeface(iransans_font);
                virtual_account.setTypeface(iransans_font);

                txtclose = CoinDialog.findViewById(R.id.textclose);
                CoinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                CoinDialog.show();

                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient client = new OkHttpClient.Builder()
                        .addInterceptor(logging)
                        .build();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Api.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build();

                api = retrofit.create(Api.class);

                LoginPanel loginPanel = new LoginPanel();
                NewPassword newPassword = new NewPassword();

                Call<NwPass> callcoin = api.newPassword(newPassword, loginPanel.id);

                callcoin.enqueue(new Callback<NwPass>() {
                    @Override
                    public void onResponse(Call<NwPass> callcoin, Response<NwPass> response) {
                        if (response.isSuccessful()) {
                            removable_account.setText(String.valueOf(response.body().getRemovableAccount()));
                            virtual_account.setText(String.valueOf(response.body().getVirtualAccount()));
                            vizhe.setText(String.valueOf(response.body().getValueSpecialOption()));
                            gheyre_vizhe.setText(String.valueOf(response.body().getValueExcSpecialOption()));
                        }
                        else
                            Toast.makeText(Withdrawal.this, "مشکل در ارسال اطلاعات", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<NwPass> callcoin, Throwable t) {
                        t.printStackTrace();
                        Log.d("tag", "Ped, message is : " + t.getMessage());
                        Log.d("tag", "Ped, call is : " + callcoin.toString());
                        Toast.makeText(Withdrawal.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();
                    }
                });

                txtclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CoinDialog.dismiss();
                    }
                });
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(Withdrawal.this, UserPanel.class);
                startActivity(intent);
            }
        });

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        api = retrofit.create(Api.class);

        final LoginPanel loginPanel = new LoginPanel();
        final NewPassword newPassword = new NewPassword();

        Call<NwPass> call = api.newPassword(newPassword, loginPanel.id);

        call.enqueue(new Callback<NwPass>() {
            @Override
            public void onResponse(Call<NwPass> call, Response<NwPass> response) {
                if (response.isSuccessful()) {
                    credit.setText(response.body().getRemovableAccount().toString() + " تومان ");
                    if (response.body().getSheba().toString().equals("NULL")) {
                        finish();
                        Intent intent = new Intent(Withdrawal.this, Change_creditcard.class);
                        startActivity(intent);
                    }
                    else {
                        creditcard.setText(response.body().getSheba().toString());
                    }
                }
                else
                    Toast.makeText(Withdrawal.this, "مشکل در ارسال اطلاعات", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<NwPass> call, Throwable t) {
                t.printStackTrace();
                Log.d("tag", "Ped, message is : " + t.getMessage());
                Log.d("tag", "Ped, call is : " + call.toString());
                Toast.makeText(Withdrawal.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();
            }
        });

        Call<Guide> call0 = api.getGuides();

        call0.enqueue(new Callback<Guide>() {
            @Override
            public void onResponse(Call<Guide> call0, Response<Guide> response) {
                if (response.isSuccessful()) {
                    bardasht.setText(response.body().getMinBardasht().toString());
                    baghimande.setText(response.body().getMinMujudi().toString());
                }
                else
                    Toast.makeText(Withdrawal.this, "مشکل در ارسال اطلاعات", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Guide> call0, Throwable t) {
                t.printStackTrace();
                Log.d("tag", "Ped, message is : " + t.getMessage());
                Log.d("tag", "Ped, call is : " + call0.toString());
                Toast.makeText(Withdrawal.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();
            }
        });

        cash_deduction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (your_choice.getText().toString().equals(""))
                    Toast.makeText(Withdrawal.this, "لطفا مبلغ را وارد کنید", Toast.LENGTH_LONG).show();
                else
                    if (Integer.parseInt(your_choice.getText().toString()) < Integer.parseInt(bardasht.getText().toString()))
                        Toast.makeText(Withdrawal.this, "مبلغ وارد شده از حداقل مبلغ قابل برداشت باید بیشتر باشد ", Toast.LENGTH_LONG).show();
                    else {
                        new DroidDialog.Builder(Withdrawal.this)
                                .icon(R.drawable.ic_action_tick)
                                .title("برداشت از حساب                                                                  ")
                                .content(" آیا مایلید " + your_choice.getText().toString() + "  تومان از حساب خود برداشت کنید ؟                         ")
                                .cancelable(true, true)
                                .positiveButton("بله", new DroidDialog.onPositiveListener() {
                                    @Override
                                    public void onPositive(Dialog droidDialog) {
                                        droidDialog.dismiss();
                                        Options options = new Options();
                                        options.setValue(Integer.parseInt(your_choice.getText().toString()));

                                        Call<teamList> call = api.sendCash(options, loginPanel.id, loginPanel.getAccess());

                                        call.enqueue(new Callback<teamList>() {
                                            @Override
                                            public void onResponse(Call<teamList> call, final Response<teamList> response) {
                                                if (response.code() == 400)
                                                    Toast.makeText(Withdrawal.this, "موجودی حساب شما کافی نیست", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.code() == 401)
                                                        Toast.makeText(Withdrawal.this, "حساب شما به دلیل نقض قوانین بسته شده", Toast.LENGTH_LONG).show();
                                                else
                                                    if (response.isSuccessful()) {
                                                        Toast.makeText(Withdrawal.this, "درخواست شما با موفقیت ارسال شد", Toast.LENGTH_LONG).show();
                                                        finish();
                                                        Intent intent = new Intent(Withdrawal.this, UserPanel.class);
                                                        startActivity(intent);
                                                } else
                                                    Toast.makeText(Withdrawal.this, "خطا در ارسال اطلاعات", Toast.LENGTH_LONG).show();
                                            }

                                            @Override
                                            public void onFailure(Call<teamList> call, Throwable t) {
                                                t.printStackTrace();
                                                Log.d("tag", "Ped, message is : " + t.getMessage());
                                                Log.d("tag", "Ped, call is : " + call.toString());
                                                Toast.makeText(Withdrawal.this, "مشکل از سرور لطفا بعدا اقدام نمایید", Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                })
                                .neutralButton("لغو", new DroidDialog.onNeutralListener() {
                                    @Override
                                    public void onNeutral(Dialog droidDialog) {
                                        droidDialog.dismiss();
                                    }
                                })
                                .animation(AnimUtils.AnimFadeInOut)
                                .color(ContextCompat.getColor(Withdrawal.this, R.color.green), ContextCompat.getColor(Withdrawal.this, R.color.white),
                                        ContextCompat.getColor(Withdrawal.this, R.color.green))
                                .divider(true, ContextCompat.getColor(Withdrawal.this, R.color.blue))
                                .show();
                    }
            }
        });

    }
    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(Withdrawal.this, UserPanel.class);
        startActivity(intent);
    }
}
