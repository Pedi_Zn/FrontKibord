package com.example.pedram.myapplication.models_post;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pedram on 5/15/2018.
 */

public class Options {
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("AX")
    @Expose
    private String AX;

    public String getAX() {
        return AX;
    }

    public void setAX(String AX) {
        this.AX = AX;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
