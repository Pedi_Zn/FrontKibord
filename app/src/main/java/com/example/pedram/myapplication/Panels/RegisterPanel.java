package com.example.pedram.myapplication.Panels;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_post.Register;
import com.example.pedram.myapplication.models_get.RgUser;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterPanel extends AppCompatActivity {

    Typeface tf1;
    Button BTN_register;
    TextView errormsg, appname;
    EditText name, surname, phonenumber, passwrod, repassword, moaref;

    private Api api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_panel);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        api = retrofit.create(Api.class);


        errormsg = (TextView) findViewById(R.id.TV_errormsg);
        appname = (TextView) findViewById(R.id.TV_appname);
        BTN_register = (Button) findViewById(R.id.BTN_register);
        name = (EditText) findViewById(R.id.ET_name);
        surname = (EditText) findViewById(R.id.ET_surname);
        phonenumber = (EditText) findViewById(R.id.ET_phonenumber);
        passwrod = (EditText) findViewById(R.id.ET_password);
        repassword = (EditText) findViewById(R.id.ET_re_password);
        moaref = (EditText) findViewById(R.id.ET_moaref);

        tf1 = Typeface.createFromAsset(getAssets(), "iransans.ttf");

        errormsg.setTypeface(tf1);
        BTN_register.setTypeface(tf1);
        name.setTypeface(tf1);
        surname.setTypeface(tf1);
        phonenumber.setTypeface(tf1);
        passwrod.setTypeface(tf1);
        repassword.setTypeface(tf1);
        moaref.setTypeface(tf1);
        appname.setTypeface(tf1);



        BTN_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Register_Name, Register_Surname, Register_PhoneNumber, Register_Password, Register_RPassword, Register_Creditcard, Register_Moaref;

                Register_Name = name.getText().toString();
                Register_Surname = surname.getText().toString();
                Register_PhoneNumber = phonenumber.getText().toString();
                Register_Password = passwrod.getText().toString();
                Register_RPassword = repassword.getText().toString();
                Register_Moaref = moaref.getText().toString();

                if (Register_Name.equals("")) {
                    name.setBackgroundResource(R.drawable.edittext_shape);
                    Toast.makeText(getApplicationContext(), "لطفا نام خود را وارد کنید", Toast.LENGTH_LONG).show();
                }
                else {
                    name.setBackgroundResource(R.drawable.btn_radius2);
                    if (Register_Surname.equals("")) {
                        surname.setBackgroundResource(R.drawable.edittext_shape);
                        Toast.makeText(getApplicationContext(), "لطفا نام خانوادگی خود را وارد کنید", Toast.LENGTH_LONG).show();
                    } else {
                        surname.setBackgroundResource(R.drawable.btn_radius2);
                        if (Register_PhoneNumber.equals("")) {
                            phonenumber.setBackgroundResource(R.drawable.edittext_shape);
                            Toast.makeText(getApplicationContext(), "لطفا شماره موبایل خود را وارد کنید", Toast.LENGTH_LONG).show();
                        } else {
                            phonenumber.setBackgroundResource(R.drawable.btn_radius2);
                            if (!(trueNumber(Register_PhoneNumber, 11))) {
                                phonenumber.setBackgroundResource(R.drawable.edittext_shape);
                                errormsg.setText("ارقام شماره موبایل صحیح نیست");
                            } else {
                                phonenumber.setBackgroundResource(R.drawable.btn_radius2);
                                if (Register_Password.equals("")) {
                                    passwrod.setBackgroundResource(R.drawable.edittext_shape);
                                    errormsg.setVisibility(View.INVISIBLE);
                                    Toast.makeText(getApplicationContext(), "لطفا کلمه عبور خود را وارد کنید", Toast.LENGTH_LONG).show();
                                } else {
                                    passwrod.setBackgroundResource(R.drawable.btn_radius2);
                                    if (!(truePass(passwrod.getText().toString()))) {
                                        passwrod.setBackgroundResource(R.drawable.edittext_shape);
                                        errormsg.setText("رمز عبور باید شامل حداقل 8 کاراکتر، یک رقم و یک حرف بزرگ باشد");
                                        errormsg.setVisibility(View.VISIBLE);
                                    } else {
                                        passwrod.setBackgroundResource(R.drawable.btn_radius2);
                                        errormsg.setVisibility(View.INVISIBLE);
                                        if (Register_RPassword.equals("")) {
                                            repassword.setBackgroundResource(R.drawable.edittext_shape);
                                            Toast.makeText(getApplicationContext(), "لطفا تکرار کلمه عبور خود را وارد کنید", Toast.LENGTH_LONG).show();
                                        } else {
                                            repassword.setBackgroundResource(R.drawable.btn_radius2);
                                            if (!Register_Password.equals(Register_RPassword)) {
                                                repassword.setBackgroundResource(R.drawable.edittext_shape);
                                                errormsg.setText("کلمه عبور با تکرار آن همخوانی ندارد");
                                                errormsg.setVisibility(View.VISIBLE);
                                            }
                                            else {

                                                errormsg.setVisibility(View.INVISIBLE);

                                                Register register = new Register();
                                                register.setName(Register_Name);
                                                register.setSurname(Register_Surname);
                                                register.setMobilenumber(Register_PhoneNumber);
                                                register.setPassword(Register_Password);
                                                register.setMoaref(Register_Moaref);

                                                Call<RgUser> call = api.registerUsers(register);

                                                call.enqueue(new Callback<RgUser>() {
                                                    @Override
                                                    public void onResponse(Call<RgUser> call, Response<RgUser> response) {
                                                        if (response.code() == 400) {
                                                            Toast.makeText(RegisterPanel.this, "شماره معرف اشتباه می باشد", Toast.LENGTH_LONG).show();
                                                        }
                                                        else {
                                                            if (response.isSuccessful()) {
                                                                Toast.makeText(getApplicationContext(), " ثبت نام با موفقیت انجام شد  " + response.body().getName(), Toast.LENGTH_LONG).show();
                                                                finish();
                                                                Intent intent = new Intent(RegisterPanel.this, LoginPanel.class);
                                                                startActivity(intent);
                                                            } else {
                                                                Toast.makeText(RegisterPanel.this, "این شماره قبلا استفاده شده", Toast.LENGTH_LONG).show();
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<RgUser> call, Throwable t) {
                                                        t.printStackTrace();
                                                        Log.d("tag", "Ped, message is : " + t.getMessage());
                                                        Log.d("tag", "Ped, call is : " + call.toString());
                                                        Toast.makeText(RegisterPanel.this, "خطا در اتصال به سرور", Toast.LENGTH_LONG).show();
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    public Integer lettersCounter (String s) {
        int counter = 0;

        for (int i = 0; i < s.length(); i++) {
            if (Character.isLetter(s.charAt(i)))
                counter++;
        }
        return counter;
    }

    public Integer digitsCounter (String s) {
        int counter =0;

        for (int i=0 ; i<s.length() ; i++)
        {
            if (Character.isDigit(s.charAt(i))) {
                counter++;
            }
        }
        return counter;
    }

    public Integer uppercaseCounter (String s) {
        int counter =0;

        for (int i=0 ; i<s.length() ; i++)
        {
            if (Character.isUpperCase(s.charAt(i))) {
                counter++;
            }
        }
        return counter;
    }

    public boolean trueNumber (String phonenumber, int definenumber) {
        if (digitsCounter(phonenumber) == definenumber)
            return true;
        else
            return false;
    }

    public boolean truePass (String s) {
        if (digitsCounter(s) >= 1 && uppercaseCounter(s) >= 1 && (lettersCounter(s) + digitsCounter(s)) >= 7)
            return true;
        else
            return false;
    }

}
