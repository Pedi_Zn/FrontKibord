package com.example.pedram.myapplication.Panels;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pedram.myapplication.Adapter.DataAdapter;
import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.JSONResponse;
import com.example.pedram.myapplication.models_get.Matches;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CupPanel extends Fragment {
    private RecyclerView recyclerView;
    private ArrayList<Matches> data;
    private DataAdapter adapter;
    private StaggeredGridLayoutManager mGridLayoutManager;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cup_panel, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);

        initViews();

        return view;
    }
    private void initViews(){
        recyclerView.setHasFixedSize(true);
        mGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        loadJSON();
    }
    private void loadJSON(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL) //https://api.myjson.com/bins/
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api request = retrofit.create(Api.class);
        LoginPanel loginPanel = new LoginPanel();

        Call<JSONResponse> call = request.getJSON(loginPanel.getAccess());
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();
                data = new ArrayList<>(Arrays.asList(jsonResponse.getAndroid()));
                /*Toast.makeText(getActivity().getApplicationContext(),data.get(10).getWinner() + "s", Toast.LENGTH_SHORT).show();
                Toast.makeText(getActivity().getApplicationContext(),data.get(11).getWinner() + "s", Toast.LENGTH_SHORT).show();
                Toast.makeText(getActivity().getApplicationContext(),data.get(12).getWinner() + "s", Toast.LENGTH_SHORT).show();
                */

                /*Toast.makeText(getActivity().getApplicationContext(), String.valueOf(data.size()), Toast.LENGTH_SHORT).show();
                Toast.makeText(getActivity().getApplicationContext(),String.valueOf(14) + " active :  " + data.get(14).getActive(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getActivity().getApplicationContext(),String.valueOf(14) + " winner :  " + data.get(14).getWinner(), Toast.LENGTH_SHORT).show();
                String s = data.get(14).getWinner();
                    Toast.makeText(getActivity().getApplicationContext(), s, Toast.LENGTH_SHORT).show();*/
                for (int i=data.size()-1 ; i >= 0 ; i--) {

                    if (data.get(i).getActive().equals("false") || !(data.get(i).getWinner().equals("NULL"))) {
                        /*Toast.makeText(getActivity().getApplicationContext(),String.valueOf(i) + " : " + data.get(i).getGroup() + " active :  " + data.get(i).getActive(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(getActivity().getApplicationContext(),String.valueOf(i) + " winner :  " + data.get(i).getWinner(), Toast.LENGTH_SHORT).show();
*/
                        data.remove(i);
                    }
                }

                adapter = new DataAdapter(getActivity().getApplicationContext(), data);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error",t.getMessage());
            }
        });
    }
}
