package com.example.pedram.myapplication.models_post;

/**
 * Created by pedram on 4/11/2018.
 */

public class NewPassword {

    private String password;
    private String sheba;
    private String private_question;
    private String private_answer;
    private String first_login;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSheba() {
        return sheba;
    }

    public void setSheba(String sheba) {
        this.sheba = sheba;
    }

    public String getPrivateQuestion() {
        return private_question;
    }

    public void setPrivateQuestion(String private_question) {
        this.private_question = private_question;
    }

    public String getPrivateAnswer() {
        return private_answer;
    }

    public void setPrivateAnswer(String private_answer) {
        this.private_answer = private_answer;
    }

    public String getFirstlogin() {
        return first_login;
    }

    public void setFirstlogin(String  first_login) {
        this.first_login = first_login;
    }
}
