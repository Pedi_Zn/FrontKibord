package com.example.pedram.myapplication.models_get;

public class Matches {

    private Integer id;
    private Integer team_id;
    private Integer team2_id;
    private Integer cup_id;
    private String name_M1;
    private String name_M2;
    private String name_M3;
    private String name_M4;
    private String name_M5;
    private String name_M6;
    private String name_M7;
    private String name_M8;
    private String name_M9;
    private String name_M10;
    private String group;
    private String model_match;
    private String number_option;
    private Double factor_M1;
    private Double factor_M2;
    private Double factor_M3;
    private Double factor_M4;
    private Double factor_M5;
    private Double factor_M6;
    private Double factor_M7;
    private Double factor_M8;
    private Double factor_M9;
    private Double factor_M10;
    private String special_option;
    private String end_time;
    private String winner;
    private String active;
    private String title;

    public String getModel_match() {
        return model_match;
    }

    public String getNumber_option() {
        return number_option;
    }

    public String getName_M7() {
        return name_M7;
    }

    public String getName_M8() {
        return name_M8;
    }

    public String getName_M9() {
        return name_M9;
    }

    public String getName_M10() {
        return name_M10;
    }

    public Double getFactor_M7() {
        return factor_M7;
    }

    public Double getFactor_M8() {
        return factor_M8;
    }

    public Double getFactor_M9() {
        return factor_M9;
    }

    public Double getFactor_M10() {
        return factor_M10;
    }


    public Integer getCup_id() {
        return cup_id;
    }

    public Integer getId() {
        return id;
    }

    public Integer getTeam_id() {
        return team_id;
    }

    public Integer getTeam2_id() {
        return team2_id;
    }

    public Double getFactor_M1() {
        return factor_M1;
    }

    public Double getFactor_M2() {
        return factor_M2;
    }

    public Double getFactor_M3() {
        return factor_M3;
    }

    public Double getFactor_M4() {
        return factor_M4;
    }

    public Double getFactor_M5() {
        return factor_M5;
    }

    public Double getFactor_M6() {
        return factor_M6;
    }

    public String getSpecial_option() {return special_option;}

    public String getTitle() {return title;}

    public String getWinner() {return winner;}

    public String getEnd_time() {return end_time;}

    public String getName_M1() {
        return name_M1;
    }

    public String getName_M2() {
        return name_M2;
    }

    public String getName_M3() {
        return name_M3;
    }

    public String getName_M4() {
        return name_M4;
    }

    public String getName_M5() {
        return name_M5;
    }

    public String getName_M6() {
        return name_M6;
    }

    public String getGroup() {
        return group;
    }

    public String getActive() {
        return active;
    }
}
