package com.example.pedram.myapplication.models_get;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pedram on 5/14/2018.
 */

public class UserMatch {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("match_id")
    @Expose
    private Integer matchId;
    @SerializedName("selected_answer")
    @Expose
    private String selectedAnswer;
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("WOrL")
    @Expose
    private String wOrL;
    @SerializedName("win_value")
    @Expose
    private Integer winValue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }

    public String getSelectedAnswer() {
        return selectedAnswer;
    }

    public void setSelectedAnswer(String selectedAnswer) {
        this.selectedAnswer = selectedAnswer;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getWOrL() {
        return wOrL;
    }

    public void setWOrL(String wOrL) {
        this.wOrL = wOrL;
    }

    public Integer getWinValue() {
        return winValue;
    }

    public void setWinValue(Integer winValue) {
        this.winValue = winValue;
    }

}
