package com.example.pedram.myapplication.models_post;

/**
 * Created by pedram on 4/9/2018.
 */

public class ForgotPassword {

    private String mobile_number;

    public String getMobilenumber() {
        return mobile_number;
    }

    public void setMobilenumber(String mobile_number) {
        this.mobile_number = mobile_number;
    }
}
