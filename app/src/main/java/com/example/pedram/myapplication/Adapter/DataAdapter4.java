package com.example.pedram.myapplication.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pedram.myapplication.Fragments.PrivatePanel;
import com.example.pedram.myapplication.Panels.CupPanel;
import com.example.pedram.myapplication.Panels.GozareshPanel;
import com.example.pedram.myapplication.Panels.LoginPanel;
import com.example.pedram.myapplication.Panels.MatchPanel;
import com.example.pedram.myapplication.Interface.Api;
import com.example.pedram.myapplication.Panels.UserPanel;
import com.example.pedram.myapplication.Panels.UserPanel2;
import com.example.pedram.myapplication.R;
import com.example.pedram.myapplication.models_get.Bills;
import com.example.pedram.myapplication.models_get.Cups;
import com.example.pedram.myapplication.models_get.DbUser;
import com.example.pedram.myapplication.models_get.FpUser;
import com.example.pedram.myapplication.models_get.Guide;
import com.example.pedram.myapplication.models_get.JSONResponse;
import com.example.pedram.myapplication.models_get.JSONResponse2;
import com.example.pedram.myapplication.models_get.JSONResponse3;
import com.example.pedram.myapplication.models_get.JSONResponse4;
import com.example.pedram.myapplication.models_get.Matches;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_get.RgUser;
import com.example.pedram.myapplication.models_get.UserMatch;
import com.example.pedram.myapplication.models_get.teamList;
import com.example.pedram.myapplication.models_post.ForgotPassword;
import com.example.pedram.myapplication.models_post.Login;
import com.example.pedram.myapplication.models_post.NewPassword;
import com.example.pedram.myapplication.models_post.OptionAnswer;
import com.example.pedram.myapplication.models_post.Options;
import com.example.pedram.myapplication.models_post.OptionsValue;
import com.example.pedram.myapplication.models_post.Register;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Path;

public class DataAdapter4 extends RecyclerView.Adapter<DataAdapter4.ViewHolder> {
    private Context context;
    private ArrayList<Bills> android;
    public static int CUPID;
    public static int team1_ID, team2_ID;
    public static String team1_name, team2_name;

    public DataAdapter4(Context context, ArrayList<Bills> android) {
        this.android = android;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_row2, viewGroup, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        final Api request = retrofit.create(Api.class);
        final LoginPanel loginPanel = new LoginPanel();

        Call<JSONResponse4> call = request.getBills(loginPanel.getId(), loginPanel.getAccess());

        call.enqueue(new Callback<JSONResponse4>() {
            @Override
            public void onResponse(Call<JSONResponse4> call, Response<JSONResponse4> response) {
                if (response.isSuccessful()) {
                    viewHolder.date.setText(android.get(i).getCreatedAt());
                    viewHolder.title.setText(android.get(i).getTitle());
                    viewHolder.debit.setText(android.get(i).getDebit().toString());
                    viewHolder.credit.setText(android.get(i).getCredit().toString());
                    viewHolder.bonus.setText(android.get(i).getBonus().toString());
                    viewHolder.total_debit.setText(android.get(i).getTotalDebit().toString());
                    viewHolder.total_credit.setText(android.get(i).getTotalCredit().toString());
                }
                else
                    Toast.makeText(context, "خطا در ارسال اطلاعات", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<JSONResponse4> call, Throwable t) {
                t.printStackTrace();
                Log.d("tag", "Ped, message is : " + t.getMessage());
                Log.d("tag", "Ped, call is : " + call.toString());
                Toast.makeText(context, "خطا در اتصال به سرور", Toast.LENGTH_SHORT).show();
            }
        });

        viewHolder.setItemClickListener(new Api() {
            @Override
            public Call<DbUser> tokenAccess(@Body Login login) {
                return null;
            }

            @Override
            public Call<RgUser> registerUsers(@Body Register register) {
                return null;
            }

            @Override
            public Call<FpUser> forgotUsers(@Body ForgotPassword forgotPassword) {
                return null;
            }

            @Override
            public Call<NwPass> newPassword(@Body NewPassword newPassword, @Path("id") Integer id) {
                return null;
            }

            @Override
            public Call<RgUser> getUser(@Path("id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<JSONResponse> getJSON(@Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<JSONResponse3> getCups(@Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<JSONResponse4> getBills(@Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<Guide> getGuides() {
                return null;
            }

            @Override
            public Call<teamList> getTeam(@Path("id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<Matches> getMatch(@Path("id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> setOption(@Body Options options, @Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> sendCash(@Body Options options, @Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> addValue1(@Body Options options, @Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> addValue(@Body Options options, @Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> setValue(@Body OptionsValue optionsValue, @Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> setAnswer(@Body OptionAnswer optionAnswer, @Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<UserMatch> getOption(@Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<ResponseBody> getCupImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<ResponseBody> getTeamImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<ResponseBody> getLeftTeamImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<ResponseBody> getRightTeamImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<JSONResponse2> getPreMatches(@Path("user_id") Integer id, @Header("authorization") String accessToken) {
                return null;
            }

            @Override
            public Call<teamList> getSuccess() {
                return null;
            }

            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                /*if (isLongClick) {
//                    Toast.makeText(context, "#" + android.get(i).getId() + " - "  + " (Long click)", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(context, UserPanel2.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {

                    Intent intent = new Intent(context, UserPanel2.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
//                    Toast.makeText(context, "#" + android.get(i).getId() + " - " , Toast.LENGTH_SHORT).show();
                }*/
            }

        });
    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView title, credit, debit, bonus, total_debit, total_credit, date;
        private Api requestInterface;

        public ViewHolder(View view) {
            super(view);

            date = (TextView)view.findViewById(R.id.date);
            title = (TextView)view.findViewById(R.id.title);
            debit = (TextView) view.findViewById(R.id.debit);
            credit = (TextView) view.findViewById(R.id.credit);
            bonus = (TextView) view.findViewById(R.id.bonus);
            total_credit = (TextView)view.findViewById(R.id.total_credit);
            total_debit = (TextView)view.findViewById(R.id.total_debit);

            view.setTag(view);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);

        }

        public void setItemClickListener (Api requestInterface) {
            this.requestInterface = requestInterface;
        }

        @Override
        public void onClick(View view) {
            requestInterface.onClick(view, getPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            requestInterface.onClick(view, getPosition(), true);

            return true;
        }
    }

}