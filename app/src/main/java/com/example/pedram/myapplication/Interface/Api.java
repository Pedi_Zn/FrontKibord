package com.example.pedram.myapplication.Interface;

import android.view.View;

import com.example.pedram.myapplication.Panels.StartupPanel;
import com.example.pedram.myapplication.models_get.ChPass;
import com.example.pedram.myapplication.models_get.ChSheba;
import com.example.pedram.myapplication.models_get.Guide;
import com.example.pedram.myapplication.models_get.JSONResponse;
import com.example.pedram.myapplication.models_get.JSONResponse2;
import com.example.pedram.myapplication.models_get.JSONResponse3;
import com.example.pedram.myapplication.models_get.JSONResponse4;
import com.example.pedram.myapplication.models_get.Matches;
import com.example.pedram.myapplication.models_get.NwPass;
import com.example.pedram.myapplication.models_get.Product;
import com.example.pedram.myapplication.models_get.UserMatch;
import com.example.pedram.myapplication.models_get.teamList;
import com.example.pedram.myapplication.models_get.DbUser;
import com.example.pedram.myapplication.models_post.ForgotPassword;
import com.example.pedram.myapplication.models_get.FpUser;
import com.example.pedram.myapplication.models_post.Login;
import com.example.pedram.myapplication.models_post.NewPassword;
import com.example.pedram.myapplication.models_post.OptionAnswer;
import com.example.pedram.myapplication.models_post.Options;
import com.example.pedram.myapplication.models_post.OptionsValue;
import com.example.pedram.myapplication.models_post.Register;
import com.example.pedram.myapplication.models_get.RgUser;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by pedram on 3/22/2018.
 */

public interface Api {

    String BASE_URL = StartupPanel.IP;

//    String BASE_URL = "http://10.0.2.2:8000";
//    String BASE_URL = "http://169.254.240.105:8000";

    @POST("/login")
    Call<DbUser> tokenAccess(@Body Login login);

    @POST("/users")
    Call<RgUser> registerUsers(@Body Register register);

    @POST("/forgot_password")
    Call<FpUser> forgotUsers(@Body ForgotPassword forgotPassword);

    @POST("/users/{id}")
    Call<NwPass> newPassword(@Body NewPassword newPassword, @Path("id") Integer id);

    @GET("/users/{id}")
    Call<RgUser> getUser(@Path("id") Integer id, @Header("authorization") String accessToken);

    @GET("/matches") // 1bu9jb
    Call<JSONResponse> getJSON(@Header("authorization") String accessToken);

    @GET("/cups")
    Call<JSONResponse3> getCups(@Header("authorization") String accessToken);

    @GET("/bills/{user_id}")
    Call<JSONResponse4> getBills(@Path("user_id") Integer id, @Header("authorization") String accessToken);

    @GET("/guides/1")
    Call<Guide> getGuides();

    @GET("/teams/{id}")
    Call<teamList> getTeam(@Path("id") Integer id, @Header("authorization") String accessToken);

    @GET("/matches/{id}")
    Call<Matches> getMatch(@Path("id") Integer id, @Header("authorization") String accessToken);

    @POST("/user_update_fund/{user_id}/{match_id}")
    Call<teamList> setOption(@Body Options options, @Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken);

    @POST("/remove_value_user/{user_id}")
    Call<teamList> sendCash(@Body Options options, @Path("user_id") Integer id, @Header("authorization") String accessToken);

    @POST("/request_dargah/{user_id}")
    Call<teamList> addValue1(@Body Options options, @Path("user_id") Integer id, @Header("authorization") String accessToken);

    @POST("/add_value_user/{user_id}")
    Call<teamList> addValue(@Body Options options, @Path("user_id") Integer id, @Header("authorization") String accessToken);

    @POST("/user_update_match_value/{user_id}/{match_id}")
    Call<teamList> setValue(@Body OptionsValue optionsValue, @Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken);

    @POST("/user_update_match_answer/{user_id}/{match_id}")
    Call<teamList> setAnswer(@Body OptionAnswer optionAnswer, @Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken);

    @GET("/show_user_match/{user_id}/{match_id}")
    Call<UserMatch> getOption(@Path("user_id") Integer id, @Path("match_id") Integer matchId, @Header("authorization") String accessToken);

    @GET("/show_all_user_matches/{user_id}")
    Call<JSONResponse2> getPreMatches(@Path("user_id") Integer id, @Header("authorization") String accessToken);

    @GET("/is_successful")
    Call<teamList> getSuccess();

    @GET("/download_cup_image/{match_id}")
    Call<ResponseBody> getCupImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken);

    @GET("/download_team_image/{match_id}")
    Call<ResponseBody> getTeamImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken);

    @GET("/download_left_match/{match_id}")
    Call<ResponseBody> getLeftTeamImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken);

    @GET("/download_right_match/{match_id}")
    Call<ResponseBody> getRightTeamImage(@Path("match_id") Integer matchId, @Header("authorization") String accessToken);


    void onClick(View view, int position, boolean isLongClick);
}
