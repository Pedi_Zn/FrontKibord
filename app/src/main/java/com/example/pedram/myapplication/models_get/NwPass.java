package com.example.pedram.myapplication.models_get;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pedram on 4/11/2018.
 */

public class NwPass {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("surname")
    @Expose
    private String surname;
    @SerializedName("virtual_account")
    @Expose
    private Integer virtualAccount;
    @SerializedName("removable_account")
    @Expose
    private Integer removableAccount;
    @SerializedName("private_answer")
    @Expose
    private String privateAnswer;
    @SerializedName("private_question")
    @Expose
    private String privateQuestion;
    @SerializedName("first_login")
    @Expose
    private String firstLogin;
    @SerializedName("disable_user")
    @Expose
    private String disableUser;
    @SerializedName("sheba")
    @Expose
    private String sheba;
    @SerializedName("bonus")
    @Expose
    private Integer bonus;
    @SerializedName("remove_value")
    @Expose
    private Integer removeValue;
    @SerializedName("value_special_option")
    @Expose
    private Integer valueSpecialOption;
    @SerializedName("value_exc_special_option")
    @Expose
    private Integer valueExcSpecialOption;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getVirtualAccount() {
        return virtualAccount;
    }

    public void setVirtualAccount(Integer virtualAccount) {
        this.virtualAccount = virtualAccount;
    }

    public Integer getRemovableAccount() {
        return removableAccount;
    }

    public void setRemovableAccount(Integer removableAccount) {
        this.removableAccount = removableAccount;
    }

    public String getPrivateAnswer() {
        return privateAnswer;
    }

    public void setPrivateAnswer(String privateAnswer) {
        this.privateAnswer = privateAnswer;
    }

    public String getPrivateQuestion() {
        return privateQuestion;
    }

    public void setPrivateQuestion(String privateQuestion) {
        this.privateQuestion = privateQuestion;
    }

    public String getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        this.firstLogin = firstLogin;
    }

    public String getDisableUser() {
        return disableUser;
    }

    public void setDisableUser(String disableUser) {
        this.disableUser = disableUser;
    }

    public String getSheba() {
        return sheba;
    }

    public void setSheba(String sheba) {
        this.sheba = sheba;
    }

    public Integer getBonus() {
        return bonus;
    }

    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    public Integer getRemoveValue() {
        return removeValue;
    }

    public void setRemoveValue(Integer removeValue) {
        this.removeValue = removeValue;
    }

    public Integer getValueSpecialOption() {
        return valueSpecialOption;
    }

    public void setValueSpecialOption(Integer valueSpecialOption) {
        this.valueSpecialOption = valueSpecialOption;
    }

    public Integer getValueExcSpecialOption() {
        return valueExcSpecialOption;
    }

    public void setValueExcSpecialOption(Integer valueExcSpecialOption) {
        this.valueExcSpecialOption = valueExcSpecialOption;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

}