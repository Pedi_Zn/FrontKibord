package com.example.pedram.myapplication.models_post;

/**
 * Created by pedram on 3/30/2018.
 */

public class Register {

    private String name;
    private String surname;
    private String mobile_number;
    private String password;
    private String moaref;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMobilenumber() {
        return mobile_number;
    }

    public void setMobilenumber(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMoaref() {
        return moaref;
    }

    public void setMoaref(String moaref) {
        this.moaref = moaref;
    }
}
